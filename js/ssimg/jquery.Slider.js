var _URLSlider=$('#URLSlider').val();
	var _vistaSlidesActivos="voSlidesActivosPorOrigen";
	var _slidesMostrados={};


	refrescarSliders = function(){
		$('.slider').each(function(index,obj){
			var origen=$(obj).attr("data-origen");
			if (origen){
				params={};
				params.id=$(obj).attr("id");
				params.origen=origen;
				params.index=index;
				params.showIndicators=$(obj).attr("data-indicators")=="true"?true:false;
				params.showNavigator=$(obj).attr("data-navigator")=="true"?true:false;
				//loadViewContentByCategory(_URLSlider,_vistaSlidesActivos, origen, mostrarSlider, true, params);
				$.ajax({
					url: _URLSlider + "/API_Slide_ListarSlidesOrigen?OpenAgent&origen="+origen,
					type: "POST",
					cache:false,
					data : params,
					dataType: "text",
					success: function (data) {
						stopLoad();
						var response = JSON.parse(data);
						if (response.length>0){
							mostrarSlider(response,params);
						}
						else {
							//showNotification(translate("QueryError"), "Error");
						}
					},
					error: function (xhr, status) {
						stopLoad();
						//showNotification(translate("ConnectionError"), "Error");
					}
				});
			}//End if

		});
	}


	function mostrarSlider(data, params){
		var sliderID="slider-"+params.index;
		if(data){
			var slides="";
			var indicators="";
			for (var i=0; i<data.length;i++){
				var slide=_Slide;
				var indicator=_SlideIndicator;

				var primaryText=data[i].primaryText;
				var secondaryText=data[i].secondaryText;
				slide=slide.replace(/#SLIDE_ACTIVE#/g,i==0?"active":"");
				slide=slide.replace(/#SLIDE_IMg_SRC#/g,data[i].src);
				slide=slide.replace(/#SLIDE_LINK#/g,data[i].link);
				slide=slide.replace(/#SLIDE_IMG_ALT#/g,primaryText);
				slide=slide.replace(/#SLIDE_TEXT_PRIMARY#/g,primaryText);
				slide=slide.replace(/#SLIDE_PRIMARY_COLOR#/g,data[i].primaryTextColor);
				slide=slide.replace(/#SLIDE_TEXT_SECONDARY#/g,secondaryText);
				slide=slide.replace(/#SLIDE_SECONDARY_COLOR#/g,data[i].secondaryTextColor);
				slides=slides+slide;

				indicator=indicator.replace(/#SLIDER_ID#/g,sliderID);
				indicator=indicator.replace(/#SLIDE_NUMBER#/g,i);
				indicator=indicator.replace(/#SLIDE_ACTIVE#/g,i==0?"active":"");
				indicators=indicators+indicator;

			}
			var nav=_SliderNavigator;
			nav=nav.replace(/#SLIDER_ID#/g,sliderID);

			var bloqueSlider=_BloqueSlider;
			bloqueSlider=bloqueSlider.replace(/#SLIDER_ID#/g,sliderID);
			bloqueSlider=bloqueSlider.replace(/#SLIDES#/g,slides);
			bloqueSlider=bloqueSlider.replace(/#SLIDER_INDICATORS#/g,params.showIndicators&&data.length>1?indicators:"");
			bloqueSlider=bloqueSlider.replace(/#SLIDER_NAVIGATOR#/g,params.showNavigator&&data.length>1?nav:"");

			$('#'+params.id).append(bloqueSlider);

			$('#'+params.id+' .carousel').carousel({
  			interval: 5000
			})

			$('.slide-link').unbind("click").click(function(e){
				if($(this).attr("href")==""){
					e.preventDefault();
				}
			});

		}
	}



document.addEventListener("LanguageReady", function(){
  	refrescarSliders();
});
