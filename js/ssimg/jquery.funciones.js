var LanguageReady = new Event ("LanguageReady");
var TranslationsReady = new Event ("TranslationsReady");


(function($){
  //document.dispatchEvent(LanguageReady);


  $.global_TagRetornoAgente = "<RETORNO_AGENTE>";
  $.global_TagErrorAgente = "<ERROR_AGENTE>";
  $.global_TagWarningAgente = "~WARNING_AGENTE~";
  $.global_SufijoDBL = "~.~";
  $.global_SeparadorUniversal = "~.~";
  $.global_SustitutoDosPuntos = "*..*";
  $.global_SepURL = "&";
  $.global_FuncionConfirmS = "";
  $.global_FuncionConfirmN = "";
  $.global_DecimalSeparator = ",";
  $.global_ThousandSeparator = ".";
  $.global_ReturnHTML = "<br />";
  $.global_optionsDialogPopUp;
  $.global_FinSeparadorComentariosST = "-----------------------------";
  $.global_SepEntreDatosComentariosST = " --- ";



  //Flag para carga/descarga del loader
  var _loading=0;

/**
 * Aumenta el indicador de carga. Muestra el loader si es necesario
 * @return
 */
startLoad = function (){
  if (_loading==0){
    $('.loading').fadeIn('fast');
  }
  _loading++;
};

/**
 * Decrementa el indicador de carga. Oculta el loader si es necesario
 * @return
 */
stopLoad = function (){

if (_loading>0)
    _loading--;

  if (_loading==0){
    setTimeout(function(){
      if (_loading==0){
        $('.loading').fadeOut('slow');
      }
    },600);

  }
}

/**
 *
 * Carga todo el contenido de una vista y devuelve objeto JSON.
 * Llama a una función de callback
 *
 * @param db : Base de datos
 * @param view : vista
 * @param callBack : Función que se ejecutará al acabar la llamada
 *
 */
loadAllViewContent = function (db, view, callBck, showLoader, extraParam, asynchronous) {
	if (showLoader) {
		startLoad();
	}

	if (asynchronous == undefined || asynchronous == null) {
		asynchronous = true;
	}

	$.ajax({
		url: db + "/" + view +"?readviewentries&count=-1&outputformat=JSON",
		async: asynchronous,
		type: "GET",
		cache:false,
		dataType: "json",
		success: function (data) {
			if (showLoader) {
				stopLoad();
			}
			callBck(data, extraParam);
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + view);
			callBck("", extraParam);
		}
	});
}

/**
 *
 * Carga todo el contenido de una vista y devuelve objeto JSON.
 * Llama a una función de callback
 *
 * @param db : Base de datos
 * @param view : vista
 * @param callBack : Función que se ejecutará al acabar la llamada
 *
 */
var unlimittedViewData={};
loadAllViewContentNoLimit = function (db, view, callBck, showLoader, extraParam, asynchronous, append) {
	if (showLoader) {
		startLoad();
	}

	if (asynchronous == undefined || asynchronous == null) {
		asynchronous = true;
	}

  if (!append){
    unlimittedViewData[extraParam.targetField]={};
    unlimittedViewData[extraParam.targetField].viewentry=[]; 
  }
  
  var start = 1;
  if (unlimittedViewData[extraParam.targetField]){
    start = unlimittedViewData[extraParam.targetField].viewentry.length + 1
  }

	$.ajax({
		url: db + "/" + view +"?readviewentries&start="+ start + "&count=-1&outputformat=JSON",
		async: asynchronous,
		type: "GET",
		cache:false,
		dataType: "json",
		success: function (data) {
			if (showLoader) {
				stopLoad();
			}
 
      if (data.viewentry){
        for (var i = 0; i< data.viewentry.length; i++){
          unlimittedViewData[extraParam.targetField].viewentry.push(data.viewentry[i]);
        }

        if (data.viewentry.length == 1000) {
          loadAllViewContentNoLimit(db,view,callBck,showLoader,extraParam,asynchronous,true);
        }else {
          callBck(unlimittedViewData[extraParam.targetField], extraParam);
        }
      }else {
        callBck(unlimittedViewData[extraParam.targetField], extraParam);
      }
			
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + view);
			callBck("", extraParam);
		}
	});
}

/**
 *
 * Carga el contenido de una vista categorizada según la categoría y devuelve objeto JSON.
 * Llama a una función de callback
 *
 * @param db : Base de datos
 * @param view : vista
 * @param catId : categoría a buscar
 * @param callBack : Función que se ejecutará al acabar la llamada
 * @param extraParam : Parámetro adicional para pasarle al callback
 *
 */
loadViewContentByCategory = function(db, view, catId, callBack, showLoader, extraParam, asynchronous) {
  if (showLoader){
    startLoad();
  }

  if (asynchronous == undefined || asynchronous == null) {
    asynchronous = true;
  }
  
  $.ajax({
    url: db + "/"+view+"?readviewentries&outputformat=JSON&count=-1&restricttocategory="+ catId,
    async: asynchronous,
    type: "GET",
    cache:false,
    dataType: "json",
    success: function (data) {
      if (showLoader) {
        stopLoad();
      }
      callBack(data, extraParam);
    },
    error: function (xhr, status) {
      if (showLoader) {
        stopLoad();
      }
      console.log("error en : " + view);
      callBack("", extraParam);
    }
  });
}

/**
 *
 * Carga N registros en formato JSON de la vista desde el ID pasado por parámetro.
 * Llama a una función de callback
 *
 * @param String db : Base de datos
 * @param String view : vista
 * @param String callBack : Función que se ejecutará al acabar la llamada
 * @param String id: Id de documento a buscar
 * @param Int n : número de registros a obtener
 */
loadNRegsFromView = function (db, view, id, n, callBck, showLoader, extraParam ){
  if (showLoader) {
    startLoad();
  }
  $.ajax({
       url: db + "/" + view +"?readviewentries&startkey="+id+"&count="+n+"&outputformat=JSON",
       type: "GET",
       cache:false,
       dataType: "json",

       success: function (data) {
            if (showLoader) stopLoad();
            callBck(data, extraParam);
       },
      error: function (xhr, status) {
         if (showLoader) stopLoad();
        result="error";
        console.log(status);
       }
  });
}

decodeViewData = function (data){
  if (typeof data == "string"){
    data=data.replace(/(\r\n|\n|\r)/gm, "");
    data=data.replace(/~/g,"'");
    data=data.replace(/¬/g,'"');
  }

  return data;
}
/**
 *
 * Carga todo el contenido de una vista y devuelve objeto JSON.
 * Llama a una función de callback
 *
 * @param dbServer : Nombre de servidor de la BD (ej: SRVDOM01). Si se deja en blanco se usará el campo jsServerName
 * @param dbPath : ruta relativa de la BD (ej: ssimg/file.nsf). Si se deja en blanco se usará el campo jsPathBDWeb
 * @param view : vista
 * @param category: categoría a buscar. En blanco no filtrará ninguna y devolverá todos los registros
 * @param callBack : Función que se ejecutará al acabar la llamada
 * @param showLoader : Booleano, indica si se mostrará el loader mientras se ejecuta la llamada ajax
 * @param extraParam : Parámetro adicional para devolver con el callback
 * @param asynchronous : Booleano, indica si la llamada ajax es síncrona o asíncrona
 *
 */
loadAllViewData = function (dbServer, dbPath, view, category, callBck, showLoader, extraParam, asynchronous) {
  if (!dbServer) dbServer=jsServerName;
  if (!dbPath) dbPath=jsPathBDWeb;
  if (dbPath.substring(0, 1)=="/") dbPath= dbPath.substring(1, dbPath.length);

	if (showLoader) {
		startLoad();
	}

	if (asynchronous == undefined || asynchronous == null) {
		asynchronous = true;
	}

  dbPath=dbPath.replace(/\//g,"~")

	$.ajax({
		url: jsDbURL  + "/API_LoadViewContent"+"?OpenAgent&dbserver="+dbServer+"&dbpath="+dbPath+"&view="+view+"&cat="+category,
		async: asynchronous,
		type: "GET",
		cache:false,
		dataType: "Text",
		success: function (data) {
			if (showLoader) {
				stopLoad();
			}
      var parsedData=JSON.parse(decodeViewData(data));
      callBck(parsedData, extraParam);
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + view);
			callBck("", extraParam);
		}
	});
}


runGETAgent=function (db, agent, callBck, showLoader, params, extraParam, asynchronous) {
	if (showLoader) {
		startLoad();
	}

	if (asynchronous == undefined || asynchronous == null) {
		asynchronous = true;
	}

	$.ajax({
		url: db + "/" + agent +"?OpenAgent&"+params,
		async: asynchronous,
		type: "GET",
		cache:false,
		dataType: "json",
		success: function (data) {
			if (showLoader) {
				stopLoad();
			}
			callBck(data, extraParam);
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + agent);
      console.log(xhr);
      console.log(status);
			callBck("", extraParam);
		}
	});
}

runGETJsonParamsAgent = function (db, agent, callBck, showLoader, params, extraParam, asynchronous) {
	if (showLoader) {
		startLoad();
	}

	if (asynchronous == undefined || asynchronous == null) {
		asynchronous = true;
	}

	$.ajax({
		url: db + "/" + agent +"?OpenAgent",
		async: asynchronous,
		type: "GET",
		cache:false,
		dataType: "json",
    data: params,
		success: function (data) {
			if (showLoader) {
				stopLoad();
			}
			callBck(data, extraParam);
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + agent);
			callBck("", extraParam);
		}
	});
}

// Muestra notificacion tipo toast.
// Opciones para Type: Information, Success, Error, Warning, Confirm
showNotification = function (msg, type) {
  position = "topRight";
  if (!msg) {
    msg = getMessage();
  }
  if (!type) {
    type = 'error';
  }
  noty({
    theme: 'app-noty',
    text: msg,
    type: type,
    timeout: 3000,
    layout: position,
    closeWith: ['button', 'click'],
    animation: {
      open: 'noty-animation fadeIn',
      close: 'noty-animation fadeOut'
    }
  });
}

  resizeIframe = function(obj) {
  obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  obj.style.width= obj.contentWindow.document.body.scrollWidth + 'px';
  }


  	// Muestra un objeto modal
  	showModal = function (title, msg, acceptCallback, hideFooter){
  		modalTemplate='<div class="modal fade" id="dynamicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
  			'<div class="modal-dialog" role="document">'+
  			  '<div class="modal-content">'+
  				'<div class="modal-header">'+
  				  '<h5 class="modal-title" id="ModalLabel">#MODAL_TITLE#</h5>'+
  				  '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
  					'<span aria-hidden="true">&times;</span>'+
  				  '</button>'+
  				'</div>'+
  				'<div class="modal-body">#MODAL_MSG#</div>'+
  				'<div class="modal-footer '+(hideFooter?"hiddenElement":"")+'" >'+
  				  '<button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>'+
  				  '<button type="button" id="modalAccept" class="btn btn-default" data-dismiss="modal">Aceptar</button>'+
  				'</div>'+
  			  '</div>'+
  			'</div>'+
  		  '</div>';

  		var newModal=modalTemplate;
  		newModal=newModal.replace(/#MODAL_TITLE#/g,title);
  		newModal=newModal.replace(/#MODAL_MSG#/g,msg);

  		$("body").append(newModal);
		  translateHTMLBlock("#dynamicModal");

  		$('#dynamicModal #modalAccept').unbind("click").click(acceptCallback);
  		$('#dynamicModal #modalClose').click(function(){
  		});

  		$('#dynamicModal').modal();
  		$('#dynamicModal').on('hidden.bs.modal', function (e) {
  		  destroyModal();
  		})
  	}

  	showModalParams = function (title, msg, acceptCallback, params){
  		modalTemplate='<div class="modal fade" id="dynamicModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
  			'<div class="modal-dialog" role="document">'+
  			  '<div class="modal-content">'+
  				'<div class="modal-header">'+
  				  '<h5 class="modal-title" id="ModalLabel">#MODAL_TITLE#</h5>'+
  				  '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
  					'<span aria-hidden="true">&times;</span>'+
  				  '</button>'+
  				'</div>'+
  				'<div class="modal-body">#MODAL_MSG#</div>'+
  				'<div class="modal-footer" >'+
  				  '<button type="button"  class="btn btn-secondary" data-dismiss="modal">Cancelar</button>'+
  				  '<button type="button" id="modalAccept" class="btn btn-info" data-dismiss="modal">Aceptar</button>'+
  				'</div>'+
  			  '</div>'+
  			'</div>'+
  		  '</div>';

  		var newModal = modalTemplate;
  		newModal = newModal.replace(/#MODAL_TITLE#/g,title);
  		newModal = newModal.replace(/#MODAL_MSG#/g,msg);

  		$("body").append(newModal);
  		translateHTMLBlock("#dynamicModal");

  		$('#dynamicModal #modalAccept').unbind("click").click( function() {
  			acceptCallback(params);
  		});
  		$('#dynamicModal #modalClose').click(function(){
  		});

  		$('#dynamicModal').modal();
  		$('#dynamicModal').on('hidden.bs.modal', function (e) {
  		  destroyModal();
  		})
  	}

  	destroyModal = function(){
  	  $('#dynamicModal').remove();
  	}

  	// Objeto modal con iframe donde se pintará la ruta pasada por parámetro iframeUrl
  	showModalIframe = function (iframeUrl, height, bigmodal = false){
  		var md = new MobileDetect(window.navigator.userAgent);
  		if (md.phone() != null) { //Si estamos en un móvil, mostramos la altura de la pantalla aprox.
  			height = window.screen.availHeight - 30;
  		}
      var sizemodal = "modal-lg";
      if (bigmodal) {
        sizemodal = "modal-xl";
        height = window.innerHeight - 70;
      }

  		var newModal ='<div class="modal fade" id="iframeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
  				'<div class="modal-dialog modal-dialog-centered ' + sizemodal + '" role="document">'+
  				  '<div class="modal-content">'+
  					'<div class="modal-header" style="display:none;">'+
  					  '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
  						'<span aria-hidden="true">&times;</span>'+
  					  '</button>'+
  					'</div>'+
  					'<div class="modal-body p-0"><iframe class="rounded-lg" height="'+height+'" src="'+ iframeUrl +'"></iframe></div>'+
  				  '</div>'+
  				'</div>'+
  			  '</div>';

  		$("body").append(newModal);
  		$('#iframeModal #modalClose').click(function(){

  		});
  		$('#iframeModal').modal({backdrop: 'static', keyboard: false});
  		$('#iframeModal').on('hidden.bs.modal', function (e) {
  			 //destroyModalIframe();
  		});
  	}

  	destroyModalIframe = function(){
      window.parent.reloadDatatable(); // Si existen recargar las datatables...
      window.parent.reloadScrollContainer();
  		var padre = window.parent.document;
  		$(padre).find("[data-dismiss=modal]").trigger({ type: "click" });
  		$(padre).find('#iframeModal').remove();
  	}

  existeClave = function(dbURL, view, clave, numColumna)
  {
	if (fullTrim(clave).length == 0)
		return true;

	var existe = false
	var valor = "";
	var idDiv = "divDBCDBL";
	dbLookup(dbURL, view, clave, numColumna, idDiv);

	if ($("#"+idDiv).html().length > 0)
	{
	  var arrValores = eval($("#"+idDiv).html());
	  valor = arrValores[0].value; // Sólo devolvemos el 1er. valor encontrado
	}

	if (valor.length > 0)
		existe = true;

	return existe;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Devuelve TRUE o FALSE segun si la vista es oculta o no.
  // Será oculta si el nombre de la vista que le pasamos por parámetro empieza por "("
  // Parámetro:
  //		- viewname: nombre de la vista
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  isHiddenView = function(viewname)
  {
    return (viewname.indexOf("(") == 0);
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Crea un documento con el formulario indicado en la popia ventana.
  // Parámetro:
  //		- dbURL: ruta de la BD donde crear el documento
  //		- formName: nombre del formulario con el que crear el documento
  //		- isNewWindow: TRUE para crear el documento en una ventana nueva
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  crearDocumento = function(dbURL, formName, isNewWindow, sParamIn, newWindowBig = false)
  {
    var params = "";

    if (sParamIn != undefined)
      params += sParamIn + "&Login&cache=" + String(Math.random());
    else
      params += "&Login&cache=" + String(Math.random());

    var newURL = dbURL + "/" + formName + "?CreateDocument&Form=" + formName + params;
    if (isNewWindow)
    {
		var ancho = 600;
		var alto = 600;
		openURLNewWindow(newURL, alto, newWindowBig);
    }
    else
		window.location = newURL;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Crear/abrir un documento con el formulario indicado en la popia ventana.
  // Parámetro:
  //		- dbURL: ruta de la BD donde crear el documento
  //		- formName: nombre del formulario con el que crear el documento
  //		- isNewWindow: TRUE para crear el documento en una ventana nueva
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  openForm = function(dbURL, formName, isNewWindow, sParamIn, newWindowBig)
  {
	var params = "";

	if (sParamIn != undefined) {
		params += sParamIn + "&Login&cache=" + String(Math.random());
	} else {
		params = sParamIn + "&Login&cache=" + String(Math.random());
	}

	var newURL = dbURL + "/" + formName + "?OpenForm&Form=" + formName + params;

	if (isNewWindow) {
		var ancho = 600;
		var alto = 600;
		openURLNewWindow(newURL, alto, newWindowBig);
	} else
		window.location = newURL;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Abrir un formulario en modo.
  // Parámetro:
  //		- dbURL: ruta de la BD donde crear el documento
  //		- formName: nombre del formulario con el que crear el documento
  //		- isNewWindow: TRUE para crear el documento en una ventana nueva
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  readForm = function(dbURL, formName, isNewWindow, sParamIn)
  {
	var params = "";

	if (sParamIn != undefined) {
		params += sParamIn + "&Login&cache=" + String(Math.random());
	} else {
		params += "&Login&cache=" + String(Math.random());
	}

	var newURL = dbURL + "/" + formName + "?ReadForm&Form=" + formName + params;

	if (isNewWindow) {
		var ancho = 600;
		var alto = 600;
		openURLNewWindow(newURL, alto);
	} else
		window.location = newURL;
  };




  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Crea un documento Configuracion
  // Parámetro:
  //		- dbURL: ruta de la BD donde crear el documento
  //		- formName: nombre del formulario con el que crear el documento
  //		- isNewWindow: TRUE para crear el documento en una ventana nueva
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  crearDocumentoConfiguracion = function(dbURL, formName, isNewWindow, sParamIn)
  {
    var params = "";

    if (sParamIn != undefined)
      params += sParamIn + "&Login&cache=" + String(Math.random());
    else
      params += "&Login&cache=" + String(Math.random());

    var newURL = dbURL + "/" + formName + "?OpenForm&Form=" + formName + params;
    if (isNewWindow)
    {
        var ancho = 600;
          var alto = 600;
            openURLNewWindow(newURL, alto);
    }
    else
      window.location = newURL;

  };

  /* Recuperar la url para abrir un documento */
	getOpenFormUrl = function(formName, params) {
		var newURL = jsDbURL + "/" + formName + "?OpenForm&Form=" + formName + params;
		return newURL;
	}

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Abre en modo edición el documento cuyo ID le pasamos por parámetro.
  // Si el ID está vacío, no hará nada.
  //				- dbURL: ruta de la BD donde crear el documento
  //				- idDoc: ID del documento a abrir
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  editarDocumento = function(dbURL, idDoc)
  {
    if (fullTrim(idDoc) != "")
    {
      var newURL = dbURL + "/0/" + idDoc + "?EditDocument&Login&cache=" + String(Math.random());
      window.location = newURL;
    }
  };

  editarDocumentoModalParams = function(dbURL, idDoc, params="")
  {
    if (fullTrim(idDoc) != "")
    {
      var newURL = dbURL + "/0/" + idDoc + "?EditDocument&Login&cache=" + String(Math.random())+params;
      openURLNewWindow(newURL, 600);
    }
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Pasar el documento actual a modo edición
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  editarDocActual = function(isEditMode)
  {
    if(!isEditMode)
    {
      var loc = location.href;
      var newQS = createArrayQS(getQueryString());

      if (newQS["opendocument"] != undefined) {
        newQS["opendocument"] = "EditDocument"
      }

      if (newQS["vm"] != undefined)
		deleteQSParam(newQS, "vm");

      if (newQS["vt"] != undefined)
		deleteQSParam(newQS, "vt");

      reloadURLQueryString(createQS(newQS)+ "&cache=" + String(Math.random()));

    }
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Cambia la URL para abrir el documento
  // Parámetros:
  //	- dbURL: url de la BD
  //	- idDoc: ID del documento a abrir
  //	- formName: formulario con el que crear el documento (si el parámetro "idDoc" está vacío)
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  abrirDoc = function(dbURL, idDoc, formName)
  {
    if (fullTrim(idDoc) == "")
    {
      crearDocumento(dbURL, formName);
    }
    else
    {
      var newURL = dbURL + "/0/" + idDoc + "?OpenDocument&Login&cache=" + String(Math.random());
      window.location = newURL;
    }
  };

 /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Abre el documento en ventana modal
  // Parámetros:
  //	- dbURL: url de la BD
  //	- idDoc: ID del documento a abrir
  //	- formName: formulario con el que crear el documento (si el parámetro "idDoc" está vacío)
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  abrirDocModal = function(dbURL, idDoc, formName)
  {
    if (fullTrim(idDoc) == "")
    {
		crearDocumento(dbURL, formName, true);
    }
    else
    {
		var newURL = dbURL + "/0/" + idDoc + "?OpenDocument&Login&cache=" + String(Math.random());
		openURLNewWindow(newURL, 600);
    }
  };

  /** Abrir un documento en ventana nueva con parámetros específicos */
  abrirDocModalParams = function(dbURL, idDoc, formName, params)
  {
    if (fullTrim(idDoc) == "")
    {
      crearDocumento(dbURL, formName, true, params)

    }
    else
    {
		  var newURL = dbURL + "/0/" + idDoc + "?OpenDocument&Login&cache=" + String(Math.random()) + params;
		  openURLNewWindow(newURL, 600);
    }
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Cambia la URL para abrir el documento de tipo "Ãºnico" en la ventana actual
  // Parámetros:
  //	- dbURL: url de la BD
  //	- viewName: vista donde buscar el documento "Único".
  //	- formName: formulario con el que crear el documento de "Ãºnico" (si no existe)
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  abrirDocUnico = function(dbURL, viewName, formName)
  {
    var idDocUnico  = getIDDocConfiguracion(dbURL, viewName);

    if (fullTrim(idDocUnico) == "")
		crearDocumento(dbURL, formName)
    else
		abrirDoc(dbURL, idDocUnico, formName);
  };


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Validamos que el valor del campo sea único.
  // Parámetros:
  //			- dbURL: ruta de la BD donde crear el documento
  //			- viewUnique: vista contra la que validar el valor
  //			- clave: valor a validar que sea único.
  //			- numColumna_ids: nº de la columna de la vista que tiene los ID's
  //			- idDoc: ID del documento a validar
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  validateUnique = function(dbURL, viewUnique, clave, numColumna_ids, idDoc)
  {
    var esUnico = true;

    if (fullTrim(clave).length == 0)
    return true;

    var valor = "";
    var idDiv = "divDBCDBL";

    var sRegExp = new RegExp("\\\\", "g");
    clave = clave.replace(sRegExp, "/");

    dbLookup(dbURL, viewUnique, clave, numColumna_ids, idDiv);

    if ($("#"+idDiv).html().length > 0)
    {
      var arrValores = eval($("#"+idDiv).html());
      valor = arrValores[0].value; // Sólo devolvemos el 1er. valor encontrado
    }

    if (valor.length == 0 || valor.toLowerCase()==idDoc.toLowerCase())
		esUnico = true;
    else
		esUnico = false;
    return esUnico;
  };

  validateEqual = function(valor, idAComparar) {
    if (valor == $('#' + idAComparar).val()){
      return true;
    } else {
      return false;
    }
  }
  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // validateNumMembers
  // Validamos que la lista de valores del campo texto con los valores separados por ";" tengan el  mismo numero de valores, han de ser campos con el mismo tamaño!el valor del campo sea único.
  // Parámetros:
  //			- nomField1
  //			- nomField2
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  validateNumMembers = function(nomField1, nomField2)
  {
    var mismoNumMembers = true;

    if($("#"+nomField1)[0] != undefined) {
      var Field1 = fullTrim($("#"+nomField1).val());
      var Field2 = fullTrim($("#"+nomField2).val());

      var vListadoField1 = Field1.split(";");
      var vListadoField2 = Field2.split(";");

      if ((vListadoField1.length>1)||(vListadoField2.length>1)) {
        if (vListadoField1.length != vListadoField2.length)
			mismoNumMembers = false;
      } else {
        if ((Field1!="" && Field2=="") || (Field2!="" && Field1==""))
			mismoNumMembers = false;
      }
    }

    return mismoNumMembers;
  };


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // validateJsFormValidator
  // Validamos el formulario del objeto global jsFormValidator
  // Devuelve true si jsFormValidator es correcto, devuelve false si jsFormValidator tiene errores
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  validateJsFormValidator = function() {
    if (jsFormValidator.validateForm(true)) {
      return true;
    } else {
        if (jsFormValidator.validationErrorMessages && jsFormValidator.validationErrorMessages.length > 0) {
          var listErrorFields = "<ul>";
          jsFormValidator.validationErrorMessages.forEach(element => {
            if (element != undefined) {
              listErrorFields += "<li>" + element +"</li>";
            }
          });
          listErrorFields += "</ul>";

          showNotification(translate("litFormErrors") + listErrorFields, "Error");
        } else {
          showNotification(translate("litFormErrors"), "Error");
        }
        return false;
      }
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Muestra un pop-up de "alert"
  // Parámetros:
  //	- windowTitle: título de la ventana.
  //	- messageIcon: icono a mostrar en el título del mensaje.
  //	- messageTitle: título del mensaje.
  //	- messageText: texto del mensaje.
  //	- idDiv: id del div asociado al alert.
  //	- w: ancho de la ventana (opcional --> por defecto 300).
  //	- h: alto de la ventana (opcional --> por defecto 200).
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  $.fn.showAlert = function(windowTitle, messageIcon, messageTitle, messageText, w, h)
  {
    var titleHTML = "";
    var messageHTML = "";
    var imgIcon = "";

    // Vaciamos el contenido del div
    $(this).empty().html("");
    // Redondeamos al entero superior
    w = Math.ceil(w);
    h = Math.ceil(h);

    w = w || 'auto';
    h = h || 'auto';

    if (fullTrim(messageIcon).length > 0)
		imgIcon = "<i  class='messageIcon fa "+messageIcon+"'></i>";

    if (fullTrim(messageTitle).length > 0)
		titleHTML = "<span class='dialogPopUpMessageTitle'>" + messageTitle + "</span>";
    titleHTML = imgIcon + titleHTML;

    messageHTML = "<div style='margin-top: 10px;'>" + messageText + "</div>";

    var dlg = $(this).dialog($.global_optionsDialogPopUp);
    $(this)
    .dialog("option","title", windowTitle)
    .dialog("option", "width", w)
    .dialog("option", "minWidth", w)
    .dialog("option", "position", { my: "center top+30", at: "center top", of: window })
    .html(titleHTML + messageHTML)
    .dialog("open");

    return $(this);
    translateAll();
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Cambia la URL para abrir el documento de configuración en la ventana actual
  // Parámetros:
  //	- dbURL: url de la BD
  //	- viewConfig: vista donde buscar el documento de configuración.
  //	- formName: formulario con el que crear el documento de configuración (si no existe)
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  abrirDocConfiguracion = function(dbURL, viewConfig, formName)
  {
    var idDocConfig = getIDDocConfiguracion(dbURL, viewConfig);
    if (fullTrim(idDocConfig) == "") {
      crearDocumentoConfiguracion(dbURL, formName);
    }
    else{
      editarDocumento(dbURL, idDocConfig);
    }

  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Cambia la URL para abrir una vista en la ventana actual
  // Parámetros:
  //	- dbURL: url de la BD
  //	- vista: nombre de la vista a abrir
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  abrirVista = function(dbURL, vista)
  {
    startLoad();
    var count = getCountParaVistas(dbURL); // Coge el nº de filas a mostrar por la vista

	if (count == undefined)
		count = "";
    else if (count != "")
		count = "&Count=" + count;

    var newURL = dbURL + "/" + vista + "?OpenView" + count + "&Login&cache=" + String(Math.random());
    window.location = newURL;
    stopLoad();
  };
  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Cambia la URL para abrir una vista en la ventana actual mostrando únicmaente los documentos de la categoria. DE MOMENTO ESTA FUNCION NO SE USA!!!!!!
  // Parámetros:
  //	- dbURL: url de la BD
  //	- vista: nombre de la vista a abrir
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

  abrirVistaRestrictToCategory = function(dbURL, vista, category)
  {
    var count = getCountParaVistas(dbURL); // Coge el nº de filas a mostrar por la vista

	if (count == undefined)
		count = "";
    else if (count != "")
		count = "&Count=" + count;

    var newURL = dbURL + "/" + vista + "?OpenView&RestrictToCategory=" + category + count + "&Login&cache=" + String(Math.random());
    window.location = newURL;
  };


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Desmarca todas las opciones del navegador, y marca aquella que contiene el link cuyo ID le pasamos por parámetro.
  // Parámetro:
  //				- idOpc: id del link que queremos resaltar en el navegador.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  resetNavegador = function(idOpc)
  {
    // ocultamos "-"
    $("img[id^=imgCollapse]").addClass("hiddenElement");
    // mostramos "+"
    $("img[id^=imgExpand]").removeClass("hiddenElement");
    // ocultamos "submenús"
    $(".navNivel2").addClass("hiddenElement");
    // ocultamos "menús"
    $(".navNivel1").addClass("hiddenElement");

    // Posicionamos la opción seleccionada
    // ... desmarcamos todas las opciones del menú
    $(".navLink_seleccion").addClass("navLink").removeClass("navLink_seleccion");
    // ... marcamos la opción seleccionada
    $("a#"+idOpc).parent().addClass("navLink_seleccion").removeClass("navLink");
    // ... mostramos la zona de la opción seleccionada
    $("a#"+idOpc).parents().removeClass("hiddenElement");
    // ... para las zonas de la opción seleccionada, mostramos los "-" y ocultamos los "+"
    $("a#"+idOpc).parent().parent().each(function(){
      var idAbuelo= leftString(this.id, "_");
      var idPadre= leftBackString(this.id, "_");

      $("img[id=imgExpand_"+idAbuelo+"]").addClass("hiddenElement");
      $("img[id=imgCollapse_"+idAbuelo+"]").removeClass("hiddenElement");

      if (fullTrim(idPadre).length > 0)
      {
        $("img[id^=imgExpand_"+idPadre+"]").addClass("hiddenElement");
        $("img[id^=imgCollapse_"+idPadre+"]").removeClass("hiddenElement");
      }
    });
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Devuelve el ID del documento de configuración (o "" si no lo encuentra).
  // Parámetros:
  //		. dbURL: path de la BD
  //		- viewConfig: vista donde buscar el documento de configuración
  //
  // NOTA: la vista debe tener como 1ª columna "@Text(@DocumentUniqueID)"
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  getIDDocConfiguracion = function(dbURL, viewConfig)
  {
    var valor = "";
    var idDiv = "divDBCDBL";
    dbColumn(dbURL, viewConfig, 1, idDiv);

    if ($("#"+idDiv).html().length > 0)
    {
      var arrValores = eval($("#"+idDiv).html());
      valor = arrValores[0].value; // Sólo devolvemos el 1er. valor encontrado
    }
    return valor;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Devuelve el nº de filas para las vistas que se ha especificado en el documento de configuración.
  // Parámetros:
  //		- dbURL: path de la BD
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  getCountParaVistas = function(dbURL)
  {
    var valor = "-1"; // por defecto devolverá "todos"
    var idDiv = "divDBCDBL";
    var numColumna = 3;
    dbColumn(dbURL, "voConfiguracion", numColumna, idDiv);

    if ($("#"+idDiv).html().length > 0)
    {
      var arrValores = eval($("#"+idDiv).html());
      valor = arrValores[0].value; // Sólo devolvemos el 1er. valor encontrado
    }

    if (valor == '-1') {
      valor = jsParamCountVistas;
    }
    return valor;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // @DBColumn
  // Parámetros:
  //		- dbURL: path de la BD
  //		- view: vista
  //		- numColumna: nº de la columna
  //		- idTarget: id del DIV/OBJETO donde dejar el resultado
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  dbColumn = function(dbURL, view, numColumna, idTarget)
  {
    //var async = false;
    var options = {
      async: false,
      dataType: "xml",
      type: "get", // 'get' or 'post', override for form's 'method' attribute
      url: dbURL + "/" + view + "?Readviewentries&Count=-1&cache=" + String(Math.random()) // override for form's 'action' attribute
    };
    var xhr = $.ajax(options);
    populateColumn(xhr, numColumna, idTarget)
  };

  var populateColumn = function(xhr, numColumna, idTarget)
  {
    numColumna = numColumna - 1; // La 1ª columna es el 0 y no el 1

    var nodeViewEntry = xhr.responseXML.getElementsByTagName("viewentry") ;
    var nodeEntryData;
    var nodeValues;

    var valoresColumna = new Object();
    valoresColumna.contador = 0;
    var arrayValues;
    var strTemp = "";

    // Añadir a la lista cada uno de los valores devueltos por el DbColumn
    for (var i=0; i < nodeViewEntry.length; i++)
    {
      nodeEntryData = nodeViewEntry[i].getElementsByTagName("entrydata");
      for (var k=0; k < nodeEntryData.length; k++)
      {
        if (nodeEntryData[k].getAttribute("columnnumber") == numColumna)
        {
          // Es la columna que queremos, procesamos TODOS sus valores
          nodeValues = nodeEntryData[k].getElementsByTagName("text");
          for (var z=0; z < nodeValues.length; z++)
          {
            valoresColumna.contador++;
            strTemp = fullTrim(nodeValues[z].childNodes[0].nodeValue);
            arrayValues = strTemp.split("|");
            if (arrayValues.length > 1)
            {
              valoresColumna["fila"+valoresColumna.contador] = "value:\"" + arrayValues[1] + "\", text:\"" + arrayValues[0] + "\"";;
            }
            else
            {
              valoresColumna["fila"+valoresColumna.contador] = "value:\"" + strTemp + "\", text:\"" + strTemp + "\"";
            }
          }
        }
      }
    }

    $("#"+idTarget).html("");
    if (valoresColumna.contador > 0)
    {
      for (i=1; i<=valoresColumna.contador; i++)
      {
        $("#"+idTarget).html($("#"+idTarget).html() + "{" + valoresColumna["fila"+i] + "}" + (i<valoresColumna.contador ? ", " : ""));
      }
      $("#"+idTarget).html("[" + $("#"+idTarget).html() + "]");
    }
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // @DBLookup
  // Parámetros:
  //		- dbURL: path de la BD
  //		- view: vista
  //		- dbLookupKey: clave a buscar
  /		- numColumna: nº de la columna a devolver
  //		- idTarget: id del DIV/OBJETO donde dejar el resultado
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  dbLookup = function(dbURL, view, dbLookupKey, numColumna, idTarget)
  {
    var options = {
      async: false,
      dataType: "xml",
      type: "get", // 'get' or 'post', override for form's 'method' attribute
      url: dbURL + "/" + view + "?Readviewentries&Count=-1&restricttocategory=" + dbLookupKey + "&cache=" + String(Math.random()) // override for form's 'action' attribute
    };

    var xhr = $.ajax(options);
    populateLookup(xhr, numColumna, idTarget)
  };

  var populateLookup = function(xhr, numColumna, idTarget)
  {
    numColumna = numColumna - 2; // La 1ª columna es el 0 y la columna categoría no cuenta

    var nodeViewEntry = xhr.responseXML.getElementsByTagName("viewentry") ;
    var nodeEntryData;
    var nodeValues;

    var valoresColumna = new Object();
    valoresColumna.contador = 0;
    var arrayValues;
    var strTemp = "";

    // Añadir a la lista cada uno de los valores devueltos por el DbColumn
    for (var i=0; i < nodeViewEntry.length; i++)
    {
      nodeEntryData = nodeViewEntry[i].getElementsByTagName("entrydata");
      for (var k=0; k < nodeEntryData.length; k++)
      {
        if (nodeEntryData[k].getAttribute("columnnumber") == numColumna)
        {
          // Es la columna que queremos, procesamos TODOS sus valores
          nodeValues = nodeEntryData[k].getElementsByTagName("text");
          for (var z=0; z < nodeValues.length; z++)
          {
            valoresColumna.contador++;
            strTemp = fullTrim(nodeValues[z].childNodes[0].nodeValue);
            arrayValues = strTemp.split("|");
            if (arrayValues.length > 1)
            {
              valoresColumna["fila"+valoresColumna.contador] = "value:\"" + arrayValues[1] + "\", text:\"" + arrayValues[0] + "\"";;
            }
            else
            {
              valoresColumna["fila"+valoresColumna.contador] = "value:\"" + strTemp + "\", text:\"" + strTemp + "\"";
            }
          }
        }
      }
    }

    $("#"+idTarget).html("");
    if (valoresColumna.contador > 0)
    {
      for (i=1; i<=valoresColumna.contador; i++)
      {
        $("#"+idTarget).html($("#"+idTarget).html() + "{" + valoresColumna["fila"+i] + "}" + (i<valoresColumna.contador ? ", " : ""));
      }
      $("#"+idTarget).html("[" + $("#"+idTarget).html() + "]");
    }
  };
  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Ejecuta un agente
  // Parámetros:
  //		- dbURL: path de la BD
  //		- nomAgente: nombre del agente a ejecutar
  //		- parametros: parámetros para el agente
  //		- idTarget: id del DIV/OBJETO donde dejar el resultado
  //		- tagRetornoAgente: tag que nos delimita lo que nos devuelve el agente
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  runAgent = function(dbURL, nomAgente, parametros, idTarget, tagRetornoAgente)
  {

    // Vaciamos el contenido que pueda tener el div destino
    $("#"+idTarget).html("");

    //var async = false;
    var options = {
      async: false,
      dataType: "xml",
      type: "get", // 'get' or 'post', override for form's 'method' attribute
      url: dbURL + "/" + nomAgente + "?OpenAgent&" + parametros + "&cache=" + String(Math.random()) // override for form's 'action' attribute

    };
    var xhr = $.ajax(options);

    if (fullTrim(tagRetornoAgente).length == 0)
    tagRetornoAgente = jsTagRetornoAgente;

    var arrResultado = xhr.responseText.split(tagRetornoAgente);

    if (arrResultado.length == 3)
    $("#"+idTarget).html(arrResultado[1]);

  };


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //Ejecuta un agente
  //Parámetros:
  //		- dbURL: path de la BD
  //		- nomAgente: nombre del agente a ejecutar
  //		- parametros: parámetros para el agente
  //		- idTarget: id del DIV/OBJETO donde dejar el resultado
  //		- tagRetornoAgente: tag que nos delimita lo que nos devuelve el agente
  //		- isAsync: TRUE será asíncrono y FALSE será síncrono.
  //		- beforeFunctionName: nombre de la función a llamar antes de la llamada AJAX
  //		- afterFunctionName: nombre de la función a llamar cuando finalice la llamada AJAX
  //		- extraOptions: opciones extras para la llamada AJAX
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  runAgentExtend = function(dbURL, nomAgente, parametros, idTarget, tagRetornoAgente, async, beforeFunction, afterFunction, extraOptions)
  {
    // Vaciamos el contenido que pueda tener el div destino
    $("#"+idTarget).html("");

    async = async || false;
    var defaultOptions = {
      idTarget: idTarget,
      tagRetornoAgente: tagRetornoAgente,
      beforeFunction: beforeFunction,
      afterFunction: afterFunction,
      async: async,
      dataType: "xml",
      type: "get", // 'get' or 'post', override for form's 'method' attribute
      beforeSend: function (XMLHttpRequest){ajaxBeforeFunction(XMLHttpRequest, this)},
      complete: function (XMLHttpRequest, textStatus) {ajaxCompleteFunction(XMLHttpRequest, this)},
      url: dbURL + "/" + nomAgente + "?OpenAgent&" + parametros + "&cache=" + String(Math.random()) // override for form's 'action' attribute
    };

    var options = jQuery.extend({}, defaultOptions, extraOptions);
    var xhr = $.ajax(options);

  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //Función previa a una llamada AJAX
  //Parámetros:
  //		- xhr: XMLHttpRequest
  //		- options: options de la llamada ajax
  //				   debe tener estas opciones:
  //						- beforeFunction: función a ejecutar (se le pasará por parámetro el objeto "options")
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  ajaxBeforeFunction = function(xhr, options){
    var beforeFunction = options.beforeFunction;

    if (beforeFunction)
    {
      eval(beforeFunction+"(options);");
    }
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //Función posterior a una llamada AJAX
  //Parámetros:
  //		- xhr: XMLHttpRequest
  //		- options: options de la llamada ajax
  //				   debe tener estas opciones:
  //						- tagRetornoAgente: tag que nos delimita lo que nos devuelve el agente
  //						- idTarget: id del DIV/OBJETO donde dejar el resultado
  //						- afterFunction: función a ejecutar (se le pasará por parámetro el objeto "options")
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  ajaxCompleteFunction = function(xhr, options){
    var tagRetornoAgente = options.tagRetornoAgente;
    var idTarget = options.idTarget;
    var afterFunction = options.afterFunction;

    if (fullTrim(tagRetornoAgente).length == 0)
    tagRetornoAgente = $.global_TagRetornoAgente;

    var arrResultado = xhr.responseText.split(tagRetornoAgente);
    if (arrResultado.length == 3)
    $("#"+idTarget).html(arrResultado[1]);

    if (afterFunction)
    {
      eval(afterFunction+"(options);");
    }
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // De lo devuelto por un agente (1e. parámetro), devuelve lo que se encuentra encerrado entre el tag que pasamos como 2Âº parámetro
  // Parámetros:
  //			- retornoAgente: valor devuelto por el agene.
  //			- tag: tag a procesar
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  procesarRetornoAgente = function(retornoAgente, tag)
  {
    var result = "";
    var arrTemp = retornoAgente.split(tag);

    if (arrTemp.length > 2)
    {
      result = arrTemp[arrTemp.length-2];
    }
    return result;
  }
  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Muestra la ventana para seleccionar usuarios y/o grupos del Names
  // Parámetros
  //			- dbURL: ruta de la BD donde crear el documento
  //			- idField: ID del campo donde añadir los usuarios/grupos seleccionados
  //			- esSimple: TRUE para que sólo se pueda seleccionar 1 único usuario o grupo
  //								FALSE para poder seleccionar varios usuarios y/o grupos
  //			- dialogTitle: titulo del dialogBox para indicar el motivo de rechazo
  //			- idDivDestino: id del div donde añadir el historico
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  loadAddressBox = function(dbURL, idField, esSimple, dialogTitle, idDivDestino, params)
  {
    var formAddressBox = "AddressBox";

    if (esSimple)
		formAddressBox += "Simple";

    var windowTitle = dialogTitle;

    var messageIcon = "";
    var messageTitle = "";
    var messageText = "";
    var vParams = "";

    if (params == undefined || params=="")
		vParams="vista=&columna=";
    else
		vParams=params;

    var options = {
      async: false,
      dataType: "xml",
      type: "get",
      url: dbURL + "/"+formAddressBox + "?OpenForm&idField="+idField+"&" + vParams + "&idDiv=" + idDivDestino + "&Login"
    };

    var xhr = $.ajax(options);
    messageText = xhr.responseText;

    $("#"+idDivDestino).showAlert(windowTitle, messageIcon, messageTitle, messageText, 850, 350);
    translateHTMLBlock("#"+idDivDestino);
    return false;
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Habilita / Deshabilita los botones que hacen la llamada a la función.
  // Parámetros
  // - mustDisable: TRUE para que deshabilitar el botón
  //				FALSE para habilitarlo
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  $.fn.disableButton = function(mustDisable)
  {
    /* Only apply to buttons!!! */
    if(this[0] && this[0].nodeName.toLowerCase()!="input" && this[0].type.toLowerCase()!="buton" || !this[0])  return this;

    return this.each(function() {
      if (typeof this.disabled != "undefined")
      {
        if (mustDisable)
        {
          this.disabled = true;
          $(this).css("cursor", "default");
        }
        else
        {
          this.disabled = false;
          $(this).css("cursor", "pointer");
        }
      }
    });
  }

  formatErrorMessages = function(arrMensajes)
  {
    var txt = "";
    var prefijo = "- ";

    if (arrMensajes.length > 0)
    {
      txt = prefijo + arrMensajes.join("<br />" + prefijo);
    }

    return txt;
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Valida que exista el path que le pasamos por parámetro. Si existe devuelve TRUE, y si no existe devuelve FALSE
  // Parámetros:
  //	- dbURL: url de la BD
  //	- path: path a validar
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  existePath = function(dbURL, path)
  {
    if (!$.browser.msie)
    return true;

    runAgent(dbURL, "existePath", "tagRetorno=" + jsTagRetornoAgente + "&path=" + path, "divRetornoAgente", "");

    if (fullTrim($("#divRetornoAgente").html()).length > 0) /* si devuelve algo, pasará la validación */
		return true;
    else
		return false;
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Copia el nombre del fichero adjuntado al campo expecificado.
  // Parámetros:
  //	- idFileUpload: id del FileUpload con el que se adjunta el fichero.
  //	- idCampoDestino: id del campo que guardará el nombre del fichero adjuntado.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  distribuirFicheroAdjunto = function(idFileUpload, idCampoDestino)
  {
    var docFileUpload = $("#"+idFileUpload)[0];
    var checkboxFichero = null;

    if (docFileUpload != undefined)
    {
      var valor = rightBackString(docFileUpload.value, "\\");
      if (valor.length == 0)
		valor = docFileUpload.value;
      $("#"+idCampoDestino).val(valor);
    }
    else
    {
      if ($("[class^="+idCampoDestino+"]:checked").length > 0)
      {
        $("#"+idCampoDestino).val("");
      }
    }
  };

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Solicita la clave en la que se quiere posicionarse en la vista y ejecuta el
  // agente que realiza el posicionamiento (pasándole como parámetro la clave
  // de posicionamiento y la vista)
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  doPosicionarse = function(dbURL, vista, idField)
  {
    var texto=$("#" + idField).fieldValue().toString();
    var windowTitle;
    var messageIcon;
    var messageTitle;
    var messageText;

    if (texto)
    {
      runAgent(dbURL, "GetEntry", "tagRetorno=" + jsTagRetornoAgente + "&tagError=" + jsTagErrorAgente + "&texto="+string2Unicode(texto) + "&vista=" + vista + "&"+ qsParams["count"], "divRetornoAgente", "");

      var resultado = $("#divRetornoAgente").html().toString();

      if (fullTrim(resultado).length > 0)
      {
        var arrError = resultado.split(jsTagErrorAgente);
        if (arrError.length == 3)
        {
          messageText = arrError[1];
          showNotification(messageText, "Error");
        }
        else
        {
          if (resultado == "0")
          {
            messageText = replaceSubstring(translate("jsLit_msgNoHaPodidoPosicionarse"), "jsLit_parametro", texto);
            showNotification(messageText, "Error");
          }
          else
          {
            addQSParam(qsParams, "Start=" + resultado);
            actualizarVista(qsParams);
          }
        }
      }
    }
  };



  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Elimina todos los conflictos
  // Parámetros:
  //			- dbURL: ruta de la BD
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  eliminarConflictos = function(dbURL)
  {
    runAgent(dbURL, "eliminarConflictos", "tagRetorno=" + jsTagRetornoAgente + "&tagError=" + jsTagErrorAgente, "divRetornoAgente", "");
    var resultado = $("#divRetornoAgente").html().toString();

    if (fullTrim(resultado).length > 0)
    {
      var arrError = resultado.split(jsTagErrorAgente);
      if (arrError.length == 3)
      {
        messageText = arrError[1];
        showNotification(messageText,"Error");
      }
      else
      {
        window.location.reload();
      }
    }
    else
    {
      window.location.reload();
    }
  }


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Llamada al agente para realizar la "FullTextSearch"
  // Parámetros;
  //		- dbURL: ruta de la BD.
  //		- campo: campo con el texto a buscar
  //		- vistaBase: vista donde basar la búsqueda
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  fullTextSearch = function(dbURL, campo, vistaBase)
  {
    if (fullTrim(campo.value) == "")
    {
      campo.focus();
      return;
    }

    var busqueda =  dbURL + "/" + vistaBase + "?SearchView&Query=" + campo.value + "&Start=1&Count=0&View="+vistaBase + "&";
    window.location =  busqueda;
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Añade un control File Upload
  // Parámetros;
  //		- idBaseOrigen: id donde se encuentra el FileUploadControl a copiar.
  //		- idDestino: id de la zona donde añadir del FileUploadControl
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  addFileUploadControl = function(idBaseOrigen, idDestino)
  {
    var codeFileUpload = $.trim($("#"+idBaseOrigen).html());
    $("#"+idDestino).append(codeFileUpload);
  }


  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Abre una URL en una nueva ventana
  // Parámetros;
  //		- urlToOpen: url a abrir
  //		- idWindow: id de la ventana a abrir
  //		- width: anchura de la ventana
  //		- height: altura de la ventana
  //		- top: posición superior de la ventana
  //		- left: posición izquierda de la ventana
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  openURLNewWindow = function(urlToOpen, height, bigmodal)
  {
	  showModalIframe(urlToOpen, height, bigmodal); //No hay pop ups reales, todo son ventanas modales...
  }

  /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  //
  //				JQUERY IN ACTION
  //
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
  $.fn.emptySelect = function()
  {
    return this.each(function(){
      if (this.tagName=='SELECT') this.options.length = 0;
    });
  };

  $.fn.loadSelect = function(optionsDataArray, empty)
  {
    var innerThis = this;
    if (empty)
    innerThis = this.emptySelect();
    return innerThis.each(function(){
      if (this.tagName=='SELECT') {

        var selectElement = this;
        $.each(optionsDataArray,function(index,optionData){
          var option = new Option(optionData.text,
            optionData.value);
            if ($.browser.msie) {
              selectElement.add(option);
            }
            else {
              selectElement.add(option,null);
            }
          });
        }
      });
    };

    $.fn.replaceWith = function(html)
    {
      return this.after(html).remove();
    };

    $.fn.getClassNames = function()
    {
      if (name = this.attr("className"))
      {
        return name.split(" ");
      }
      else
      {
        return [];
      }
    };

    /*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //
    // 																					JM
    //
    ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    $.fn.restrictToIntegers = function() {
      $(this).restrictToChars("0123456789");
      return this;
    };

    $.fn.restrictToDecimals = function(numDecimals) {
      $(this).replaceDotWithComma().numeric(true, true);
    };


    $.fn.restrictToTime = function() {
      $(this).restrictToChars("0123456789:").avoidRepeatCharTimes(":",1);
      return this;
    };

    $.fn.restrictToDateTime = function() {
      $(this).restrictToChars("0123456789:/ ").avoidRepeatCharTimes(":",1).avoidRepeatCharTimes("/",2).avoidRepeatCharTimes(" ",1);
      return this;
    };

    $.fn.restrictToDate = function() {
      $(this).restrictToChars("0123456789/ ").avoidRepeatCharTimes("/",2);
      return this;
    };

    $.fn.replaceDotWithComma = function() {
      if(this[0] && (this[0].nodeName.toLowerCase()!="input" && this[0].nodeName.toLowerCase()!="textarea"))  return this;
      this.keyup(function(e) {
        var keyDot = 190; // 190 es el "." del teclado normal
        var keyDotNum = 110; // 110 es el "." del teclado numérico
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

        if (key == keyDot || key == keyDotNum)
        {
          $(this).val($(this).val().replace(/\./g,','));
        }
      });
      return this;
    };


    $.fn.restrictToChars = function(chars) {
      if(this[0] && (this[0].nodeName.toLowerCase()!="input" && this[0].nodeName.toLowerCase()!="textarea"))  return this;
      $(this).keypress(function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        var chr = String.fromCharCode(key);
        var allow = false;

        // allow Ctrl+A
        if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
        // allow Ctrl+X (cut)
        if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
        // allow Ctrl+C (copy)
        if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
        // allow Ctrl+Z (undo)
        if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
        // allow or deny Ctrl+V (paste), Shift+Ins
        if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */ || (e.shiftKey && key == 45)) return true;


        if (chr < ' ' || !chars || chars.indexOf(chr) > -1)
        allow = true;
        else
        {
          // check for other keys that have special purposes
          if (
            /* backspace */ key != 8 &&
            /* tab */ key != 9 &&
            /* enter */ key != 13 &&
            /* end */ key != 35 &&
            /* home */ key != 36 &&
            /* left */ key != 37 &&
            /* right */ key != 39 &&
            /* del */ key != 46 &&
            /* F5 */ key != 116
          )
          {
            allow = false;
          }
          else
          {
            // for detecting special keys (listed above)
            // IE does not support 'charCode' and ignores them in keypress anyway
            if(typeof e.charCode != "undefined")
            {
              // special keys have 'keyCode' and 'which' the same (e.g. backspace)
              if(e.keyCode == e.which && e.which != 0)
              {
                allow = true;
              }
              // or keyCode != 0 and 'charCode'/'which' = 0
              else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0)
              {
                allow = true;
              }
            }
          }

        }

        return allow;
      });
      return this;
    };

    $.fn.avoidRepeatCharTimes = function(char,times) {
      if(this[0] && (this[0].nodeName.toLowerCase()!="input" && this[0].nodeName.toLowerCase()!="textarea"))  return this;
      $(this).keypress(function(e) {
        var chr = String.fromCharCode(e.charCode == undefined ? e.keyCode : e.charCode);
        if (chr=="," || chr=="." || chr==":" || chr=="/" || chr==" ") {
          var founded = 0;
          var value = ($(this).val()+chr).replace(/\./g,',');
          for (i=0;i<value.length;i++) {
            if (value.charAt(i)==char) founded++;
          }
          return founded<=times;
        }
        else
        return true;
      }
    );
    return this;
  };

  /* NO SE USA --> Una vez insertado en nº de decimales máximo, no deja añadir más caracteres (aunque sea antes del punto decimal) */
  $.fn.avoidMoreDecimals = function(numDecimals) {
    if(this[0] && (this[0].nodeName.toLowerCase()!="input" && this[0].nodeName.toLowerCase()!="textarea"))  return this;
    $(this).keypress(function(e) {
      var posComma = $(this).val().indexOf(",");
      var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if (posComma < 0)
      return true;
      else
      {
        if (isSpecialChar(e, key))
        return true;
        if (posComma < $(this).val().length - parseInt(numDecimals))
        return false;
        else
        return true;
      }
    });
    return this;
  }

  isSpecialChar = function(e, key)
  {
    var allow = false;
    // allow Ctrl+A
    if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
    // allow Ctrl+X (cut)
    if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
    // allow Ctrl+C (copy)
    if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
    // allow Ctrl+Z (undo)
    if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
    // allow or deny Ctrl+V (paste), Shift+Ins
    if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */ || (e.shiftKey && key == 45)) return true;

    // check for other keys that have special purposes
    if (
      /* backspace */ key != 8 &&
      /* tab */ key != 9 &&
      /* enter */ key != 13 &&
      /* end */ key != 35 &&
      /* home */ key != 36 &&
      /* left */ key != 37 &&
      /* right */ key != 39 &&
      /* del */ key != 46 &&
      /* F5 */ key != 116
    )
    {
      allow = false;
    }
    else
    {
      // for detecting special keys (listed above)
      // IE does not support 'charCode' and ignores them in keypress anyway
      if(typeof e.charCode != "undefined")
      {
        // special keys have 'keyCode' and 'which' the same (e.g. backspace)
        if(e.keyCode == e.which && e.which != 0)
        {
          allow = true;
        }
        // or keyCode != 0 and 'charCode'/'which' = 0
        else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0)
        {
          allow = true;
        }
      }
    }

    return allow;
  }


  $.fn.stripeNotesView = function(options)
  {

    /* Only apply to tables!!! */
    if(this[0] && this[0].nodeName.toLowerCase()!="table" || !this[0])  return this;

    var opts = jQuery.extend({}, jQuery.fn.stripeNotesView.defaults, options);

    var table = $(this);
    if (!table.attr("id"))
      table.attr("id", opts.id);
    var tableId = this.get(0).id;

    var icoCollapse = opts.icoCollapse;
    var icoExpand = opts.icoExpand;
    if (opts.pathImages)
    {
      icoCollapse = opts.pathImages + "/" + icoCollapse;
      icoExpand = opts.pathImages + "/" + icoExpand;
    }

    /* clase de la tabla */
    table.addClass("table");
    table.addClass("table-striped");
    table.addClass("table-hover");
    table.addClass("table-sm");

    /* cambiamos los iconos Plegar/Desplegar */
    $('img[src$="/icons/collapse.gif"]', $("#"+tableId)).parent().append('<i class="fa fa-minus-square" style="margin-right:5px;"></i>');
    $('img[src$="/icons/collapse.gif"]', $("#"+tableId)).remove();

    $('img[src$="/icons/expand.gif"]', $("#"+tableId)).parent().append('<i class="fa fa-plus-square" style="margin-right:5px;"></i>');
    $('img[src$="/icons/expand.gif"]', $("#"+tableId)).remove();

    /* quitamos los iconos ecblank.gif */
    $('img[src$="/icons/ecblank.gif"]', $("#"+tableId)).replaceWith("");

    /* quitamos los tags "font size=2" */
    $("#" + tableId + " font[size=2]").each(function(){
      var autoFont = this;
      $(autoFont).parent().html($(autoFont).html());
      $(autoFont).remove();
    });

    /* Cambiamos los iconos de ordenación */
    $('img[src$="/icons/dblsort.gif"]').parent().append('<i class="ti-exchange-vertical" style=padding:7px;color:orange !important;"></i>');
    $('img[src$="/icons/dblsort.gif"]').remove();

    $('img[src$="/icons/dblasc.gif"]').parent().append('<i class="ti-arrow-up" style="font-size:12px; padding:7px;"></i>');
    $('img[src$="/icons/dblasc.gif"]').remove();

    $('img[src$="/icons/dbldesc.gif"]').parent().append('<i class="ti-arrow-down" style="font-size:12px; padding:7px;"></i>');
    $('img[src$="/icons/dbldesc.gif"]').remove();

    /*Centramos los checkbox de fila horizontalmente en su columna*/
    $('input[type="checkbox"][name="cb_view"]').parent().css("text-align","center");
    $('input[type="checkbox"][name="cb_viewTitle"]').closest("th").css("text-align","center");

    /*Eliminamos los iconos en la primera columna en las tablas de resultado de búsquedas de textos*/
    if ($('img[src^="/icons/vwicnsr"]').length>0){
      table.find("tbody tr:first-of-type th:first-of-type").remove();
      $('img[src^="/icons/vwicnsr"]').parent().remove();
    }

    if (opts.isClickable)
    {
	  $("#" + tableId + " tbody tr td span.myLink").each(function(){
			var spanMyLink = this;
			var filaTabla = $(this).parent().parent();
			$(filaTabla).unbind("click").bind("click", function(e) {
				eval($(spanMyLink).html());
				e.preventDefault();
			});
		});
    }

	/* Eliminamos todos los links de las categorías (Plegar/Desplegar) */
  if (!opts.keepNavigator){
    var listaDocumentosNavegables=[];
  }

	$("#" + tableId + " tbody tr td a").each(function(){
		var aLink = this;
		var filaTabla = $(aLink).parent().parent();
		var ahref = $(aLink).attr("href");
		$(aLink).replaceWith($(aLink).html());


    if (ahref){
      if ( ahref.toLowerCase().indexOf("?opendocument") > 1 ) {
        listaDocumentosNavegables.push(ahref);
      }
    }
    if (!opts.keepNavigator){
      setSessionData("documentosNavegables",listaDocumentosNavegables);
    }

    var newbig = 0;
    if (typeof openInNewWindowBig !== 'undefined') {
      newbig = openInNewWindowBig;
    }

		$("td", $(filaTabla)).each(function() {
			$(this).bind("click", function(e) {
				if ( $.trim(ahref).length > 0 ) {
					if ( ahref.toLowerCase().indexOf("?opendocument") > 1 ) {
            setSessionData("urlOrigen",window.location.href);
						if ( openInNewWindow == 1 ) {
							openURLNewWindow(ahref, 600, newbig == 1); // Bigmodal si newbig == 1
						} else if ( newbig == 1 ) {
              openURLNewWindow(ahref, 600, true); // BigModal
            } else {
							location.href = ahref;
						}
					} else {
						location.href = ahref;
					}
				}
				$(this).css("cursor","wait");
			});
		})
	});

	if (opts.numCategorias > 0)
	{
	  /* Ocultamos la última columna (la pone Domino automáticamente por las categorías) */
	  $("#" + tableId + " tbody tr th:last-child").addClass("hiddenElement");
	  $("#" + tableId + " tbody tr td:last-child").addClass("hiddenElement");
	}

	/* MouseOver en la fila */
	var rows = this.children("tbody").children("tr");
	rows.each( function(rowIndex) {
	  var row = $(this);
	  row.mouseover(function() { $(this).addClass(opts.overClass) });
	  row.mouseout(function() { $(this).removeClass(opts.overClass) });
	});


  /* Añadimos multiidioma por javascript a la cabecera */
	table.find("tr:first th").each(function() {
		var texto = $(this).text();
		var html = $(this).html();
		html = html.replace(texto,"<span data-translate='"+texto+"'>"+texto+"</span>");
		$(this).html(html);
		//$(this).attr("data-translate", $(this).text());
	});


	/* Procesamos los CHECKBOXES de la vista */
	var cols_cbView = $("input[name=cb_view]", this);

	$(cols_cbView).each(function(){
		$(this).parent()
		.unbind("click")
		.width("5")
		.css("margin", 0)
		.css("padding", 0);
    $(this).prop("checked",false);
		$(this).bind("click", function(){
			$("input[name=cb_viewTitle]").prop("checked", false);
		});
	});


	var col_cbViewTitle = $("input[name=cb_viewTitle]", this);

	$(col_cbViewTitle).each(function(index, obj){
		$(this).parent().parent()
		.width("5")
		.css("margin", 0)
		.css("padding", 0);
    $(this).prop("checked",false);
    $(this).bind("click", function(){
      var self=$(this);
      $("input[name=cb_view]").prop("checked", $(this)[0].checked);
    });
	});


	table.parent().removeClass("hiddenElement");

	return this;
}


$.fn.stripeNotesView.defaults = {
  id: 'vistaActiva',
  tableClass: '',
  alternateRowClass: '',
  pathImages: '',
  icoExpand: 'Expand.gif',
  icoCollapse: 'Collapse.gif',
  overClass: 'over',
  numCategorias: 0,
  isClickable:true
};

$.fn.stripeTable = function(options)
{
  /* Only apply to tables!!! */
  if(this[0] && this[0].nodeName.toLowerCase()!="table" || !this[0])  return this;

  var opts = jQuery.extend({}, jQuery.fn.stripeNotesView.defaults, options);

  var table = $(this);
  if (!table.attr("id"))
  table.attr("id", opts.id);
  var tableId = this.get(0).id;

  /* clase de la tabla */
  table.addClass(opts.tableClass);

  /* filas alternas */
  var rowsOdd = this.children("tbody").children("tr:odd");
  rowsOdd.addClass(opts.alternateRowClass);

  /* MouseOver en la fila */
  var rows = this.children("tbody").children("tr");
  rows.each( function(rowIndex) {
    var row = $(this);
    row.mouseover(function() { $(this).addClass(opts.overClass) });
    row.mouseout(function() { $(this).removeClass(opts.overClass) });
  });

  return this;
}

$.fn.stripeTable.defaults = {
  id: '',
  tableClass: '',
  alternateRowClass: '',
  overClass: ''
};

$.fn.numeric = function(allowDecimalPoint, allowNegativeSign, callback)
{
  var decimalComma = ",";
  var decimalDot = ".";
  var negativeSign = "-";
  var isNotDot;
  callback = typeof callback == "function" ? callback : function(){};

  this.keypress(function(e){
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;

    if (e.charCode == 0 && (key == decimalComma.charCodeAt(0) || key == decimalDot.charCodeAt(0)) && !$.browser.msie)
    isNotDot = true;
    else
    isNotDot = false;

    // allow enter/return key (only when in an input box)
    if(key == 13 && this.nodeName.toLowerCase() == "input")
    {
      return true;
    }
    else if(key == 13)
    {
      return false;
    }
    var allow = false;
    // allow Ctrl+A
    if((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
    // allow Ctrl+X (cut)
    if((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
    // allow Ctrl+C (copy)
    if((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
    // allow Ctrl+Z (undo)
    if((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
    // allow or deny Ctrl+V (paste), Shift+Ins
    if((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */
    || (e.shiftKey && key == 45)) return true;
    // if a number was not pressed
    if(key < 48 || key > 57)
    {
      // check for other keys that have special purposes
      if(
        key != 8 /* backspace */ &&
        key != 9 /* tab */ &&
        key != 13 /* enter */ &&
        key != 35 /* end */ &&
        key != 36 /* home */ &&
        key != 37 /* left */ &&
        key != 39 /* right */ &&
        key != 46 /* del */ &&
        key != 116 /* F5 */
      )
      {
        allow = false;
      }
      else
      {
        // for detecting special keys (listed above)
        // IE does not support 'charCode' and ignores them in keypress anyway
        if(typeof e.charCode != "undefined")
        {
          // special keys have 'keyCode' and 'which' the same (e.g. backspace)
          if(e.keyCode == e.which && e.which != 0)
          {
            allow = true;
          }
          // or keyCode != 0 and 'charCode'/'which' = 0
          else if(e.keyCode != 0 && e.charCode == 0 && e.which == 0)
          {
            allow = true;
          }
        }
      }

      /* '-' only one allowed if negative sign is allowed */
      if(key == negativeSign.charCodeAt(0) && allowNegativeSign && this.value.indexOf(negativeSign) == -1) return true;
      /* only one decimal separator allowed (comma or dot) if decimal point is allowed */
      if((key == decimalComma.charCodeAt(0) || key == decimalDot.charCodeAt(0)) && !isNotDot)
      {
        if ( this.value.indexOf(decimalComma) != -1 || this.value.indexOf(decimalDot) != -1)
        {
          allow = false;
        }
        else
        {
          if (allowDecimalPoint)
          allow = true;
          else
          allow = false;
        }
      }
    }
    else
    {
      allow = true;
    }

    return allow;
  });
}

//Para poder ordenar los desplegables que se retroallimentan de forma dinámica:
jQuery.fn.sort = function()
{
  return this.pushStack([].sort.apply(this, arguments), []);
};

jQuery.fn.sortOptions = function(sortCallback)
{
  jQuery('option', this)
  .sort(sortCallback)
  .appendTo(this);
  return this;
};

jQuery.fn.sortOptionsByText = function()
{
  var byTextSortCallback = function(x, y)
  {
    var xText = jQuery(x).text().toUpperCase();
    var yText = jQuery(y).text().toUpperCase();
    return (xText < yText) ? -1 : (xText > yText) ? 1 : 0;
  };
  return this.sortOptions(byTextSortCallback);
};

jQuery.fn.sortOptionsByValue = function()
{
  var byValueSortCallback = function(x, y)
  {
    var xVal = jQuery(x).val();
    var yVal = jQuery(y).val();
    return (xVal < yVal) ? -1 : (xVal > yVal) ? 1 : 0;
  };
  return this.sortOptions(byValueSortCallback);
};


/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Guardar el documento
// Parámetro:
//			- nomAgente: nombre del agente a ejecutar al hacer el submit.
//			- seDebeValidar: opcional. TRUE valida y FALSE no valida.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
guardarDoc = function(nomAgente, seDebeValidar)
{

  $(".imgError").css("display", "none");

  var salirElement = $("#salir");
  var salir = "";

  if (salirElement != null && salirElement != undefined) {
    salir = salirElement.fieldValue()[0];
  }

  if (seDebeValidar == undefined) {
	  seDebeValidar = true;
  }

  if (seDebeValidar) {
    if (!jsFormValidator.validateForm(true)) {
      if (jsFormValidator.validationErrorMessages && jsFormValidator.validationErrorMessages.length > 0) {
        var listErrorFields = "<ul>";
        jsFormValidator.validationErrorMessages.forEach(element => {
          if (element != undefined) {
            listErrorFields += "<li>" + element +"</li>";
          }
        });
        listErrorFields += "</ul>";
        showNotification(translate("litFormErrors") + listErrorFields, "Error");
      } else {
        showNotification(translate("litFormErrors"), "Error");
      }
      return false;
    }
  }

  startLoad();

  if ($("#urlVolver").val()==""){
    $("#urlVolver").val(window.location);
  }

  $("#nomAgenteGuardar").val(nomAgente);

  $("form")[0].submit();
}

/*
 * La función procesarDocsSeleccionados ejecutará un agente que marcará todos los documentos seleccionados como basura para que se cargue la papelera.
 * Esta función se utilizará para enviar docs seleccionados a la papelera y tb para restaurarlos desde la papelera.
 *
 */
procesarDocsSeleccionados = function(dbURL, idAEliminar, sNomAgente, callback){
	var usuCN = string2Unicode(jsUsuarioActualCN);

	var params = "tagRetorno=" + $.global_TagRetornoAgente +
	"&tagError=" + $.global_TagErrorAgente +
	"&id=" + idAEliminar +
	"&usu=" + usuCN;
+

	runAgent(dbURL, sNomAgente, params, "divRetornoAgente", "");
	var resultado = $("#divRetornoAgente").html().toString();

	if (fullTrim(resultado).length > 0)
	{
		var arrError = resultado.split($.global_TagErrorAgente.toLowerCase());
		if (arrError.length == 3)
		{
			messageText = arrError[1];
			showNotification(messageText,"Error");
		}
		else
		{
      if (callback!=undefined){
        //window[callback]();
      }
		}
	}
	else
	{
    if (callback!=undefined){
      //window[callback]();
    }
	}
}



/*
 * indexForm: Recupera la posición del formulario sNom de entre el listado de forms de $("form"), Esta función se utilizará desde los formularios dialog para poder trabajar con los diferentes formualrios que se instancian.....
 */
indexForm = function(forms$, sNom){
	for (var i=1; i<forms$.length; i++){
		if (forms$[i].name == sNom)
			return i;
	}
}

/** Funciones para cerrar/abrir la ayuda de los formularios que la contengan **/
toggleAdvancedSearch = function()
{
	$("#advSearchContent").toggle();
}

toggleHelp = function()
{
	$("#helpzoneContent").toggle();

	$("#datos").toggleClass("col-md-8");
	$("#datos").toggleClass("col-md-11");

	$("#helpzone").toggleClass("col-md-4");
	$("#helpzone").toggleClass("col-md-1");

}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función para preparar la búsqueda avanzada de pagos
// Parámetro:
//			- parentObject: nombre del formulario
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
	searchWeb = function (formName){
		var flagFieldsFilled=false;
		var query='{"form" : "eq_text' +$.global_SeparadorUniversal+ formName+'", "user" : "' + $('#usuarioActualCN').val() +'",';

		$('[data-query]').each(function(){
			if ($(this).val()!="") {
				flagFieldsFilled=true
				var field=$(this).attr("data-query-field");
				var type= $(this).attr("data-query") +"_"+$(this).attr("data-query-type")
				var value= $(this).val();
				var q= '"'+($(this).attr("id")).replace(/search/g,"")+'" : "'+ type+$.global_SeparadorUniversal+value+'",';
				query+=q;

				sessionStorage.setItem($(this).attr("name"), value);
			} else {
				sessionStorage.removeItem($(this).attr("name"));
			}

		});
		query=query.replace(/,$/,"");
		query+="}";

		if (flagFieldsFilled) {
			startLoad();

			var db=$('#pathBDWeb').val();
			$.ajax({
				url: db + "/API_Busqueda?openAgent",
				type: "POST",
				cache:false,
				data : JSON.parse(query),
				dataType: "text",
				success: function (data) {
					stopLoad();
					var response = JSON.parse(data);
					if (response.result=="ok"){
						if (parseInt(response.count)>0){
							abrirVista(jsDbURL, response.view);
						}
						else {
							saveSearchWebFilters(false); // Cargar los datos de la búsqueda infructuosa en los filros y en el título
							$('#embedView').html('<h1 data-translate="NoResultsInQuery"></h1>');
							translateHTMLBlock("#embedView");
						}
					}
					else {
						showNotification(translate("QueryError"), "Error");
					}
				},
				error: function (xhr, status) {
					stopLoad();
					console.log("error en : " + view);
					showNotification(translate("ConnectionError"), "Error");
				}
			});
		}
		else {
			showNotification(translate("QueryFieldsRequired"), "Warning");
		}

	}

	/**
	** Función para mantener los filtros de la búsqueda avanzada (funciona solo en combinación con al función anterior searchWeb
	** permite mantener los filtros seleccionados en el buscador
	** utilizando para ello el sessionStorage del navegador
	** El campo removeSession se utiliza para saber si hace falta borrar el storage, en función de la vista en la que se está...
	** Saber la vista lo hace Lotus desde el formulario donde se llama esta función.
	**/
	saveSearchWebFilters = function (removeSession) {
		// Borrado de datos del storage
		if (removeSession) {
			$('[data-query]').each(function(){
				sessionStorage.removeItem($(this).attr('name'));
			});
		}

		var searchquery = '';

		// Recuperar los datos del sessionStorage
		$('[data-query]').each(function(){
			var id = $(this).attr('id');
			var itemname = $(this).attr('name');
			var item = sessionStorage.getItem($(this).attr('name'));

			if ( item != null && item != undefined ) {
				$(this).val(item);
				if (itemname == "searchEstado") {
					$('#sel-' + itemname ).val(item).trigger('change');
					$('#' + itemname + " option:selected").removeAttr("selected");
					$('#' + itemname + " option[value='" + item + "']").attr("selected","selected");

					searchquery += $('#sel-' + id + " option[value='" + item + "']").html() + " / ";
				} else {
					searchquery += item + " / ";
				}
			}
		});

		// Además se añade en el título del formulario, la búsqueda realizada...
		if (searchquery != '') {
      searchquery = searchquery.replace(new RegExp(" / " + '$'), '');
			searchquery = '(' + searchquery + ')';
			$('#subtitle').html(searchquery);
		}
	}

  /**
  * Función auxiliar para la lectura de sessionStorage por nombre
  *
  **/
  getSessionData = function (name) {
    if (sessionStorage.getItem(name)){
      return JSON.parse(sessionStorage.getItem(name));
    }
    else return null;
  }

  /**
  * Función auxiliar para la escritura de sessionStorage por nombre
  *
  **/
  setSessionData = function(object, data) {
    sessionStorage.setItem(object, JSON.stringify(data));
  }

  /**
  * Función auxiliar para la escritura de sessionStorage por nombre
  *
  **/
  removeSessionData = function(object) {
    sessionStorage.removeItem(object);
  }

  procesarErroresDeRetornoDeAgente = function (){
		var urlParams = new URLSearchParams(window.location.search);
		var hayErrorAgente = urlParams.has('errorRetorno');
		if (hayErrorAgente){
			var errorRetorno=urlParams.get('errorRetorno');
      setTimeout(function(){
        showNotification(translate(errorRetorno,true), "error");
      },500)

		}
	}

  updateDocumentNavigator = function(){
		var thisDoc=jsIdDoc.toLowerCase();
    var posicion = getDocumentPosition();
		var docList = getSessionData("documentosNavegables");
		if (docList && posicion>-1) {
			$('.current-position').html(posicion+1);
			$('.total-documents').html(docList.length);

			if (posicion>0){
				$('#button-prev-document').removeClass("navigator-disabled");
			}
			else {
				$('#button-prev-document').addClass("navigator-disabled");
			}

			if (posicion<docList.length-1){
					$('#button-next-document').removeClass("navigator-disabled");
			}
			else {
				$('#button-next-document').addClass("navigator-disabled");
			}
		}
		else {
			$('.document-navigator').hide();
		}

	}

	goToDocumentPosition = function(position){
		var docList = getSessionData("documentosNavegables");
		if (docList[position]!=undefined){
			location.href = docList[position];
		}
	}

	getDocumentPosition = function() {
		var thisDoc=jsIdDoc.toLowerCase();
		var docList = getSessionData("documentosNavegables");
		var posicion=-1;
    if (docList){
      for (var i=0; i<docList.length;i++){
  			if (docList[i].indexOf(thisDoc.toUpperCase())>-1){
  				posicion=i;
  				break;
  			}
  		}
    }

		return posicion
	}



  var _DTHeadersInfo = {};
  var _FiltersCount = 0;
  var _ResetFilters = false;
  propagarCambiosDeFiltrosCategorizados = function(viewName, tableID, filterChanged, newValue){
    var filtroGenerico = "&search[value]=" + $('#'+tableID+'_filter').find("input").val();
    var filtros ="";
    var valores = "";
     _FiltersCount = 0;
    $.each($('#'+tableID+' .filter'), function(index,obj){
      filtros +="&columns["+index+"][data]="+$(obj).attr("data-filter");
        var tempObj = $(obj).find("option:selected");
        var tempVal = $(tempObj).attr("data-category");
        if (tempVal=="undefined" || tempVal == undefined){
          tempVal="";
        } else {
          _FiltersCount++;
        }
        valores +="&columns["+index+"][search][value]="+tempVal;

    });

    headerApiURL= "API_Datatables_GetHeaderCategorized?OpenAgent&view=";
    $.ajax({
      url: headerApiURL + viewName + filtros + valores + filtroGenerico,
      dataType: "json",
      success: function (data) {
        $.each(data, function (index, obj) {
          //Ordenar filtros categorizados
          if (obj.catFilters){
            $.each(obj.catFilters, function(key){
              data[index].catFilters[key].childrenValues=data[index].catFilters[key].childrenValues.sort();
              data[index].catFilters[key].parentValues=data[index].catFilters[key].parentValues.sort();
            });

              _DTHeadersInfo[obj.data]={};
              _DTHeadersInfo[obj.data].cats = obj.catFilters;
              _DTHeadersInfo[obj.data].parent = data[index].parent;
              _DTHeadersInfo[obj.data].child = data[index].child;
              _DTHeadersInfo[obj.data].position = data[index].position;
              _DTHeadersInfo[obj.data].allFilters = data[index].filters;
          }
        });

        //Adaptar valores de filtro Padre (si lo hay)
        if (newValue == "" && _FiltersCount == 1) {
          _ResetFilters = true;
        }else {
          _ResetFilters = false;
        }

        if (_DTHeadersInfo[filterChanged].parent !="" && _DTHeadersInfo[filterChanged].parent !=undefined){
          var actualizar = true;
          /*if (_FiltersCount == 1){
            actualizar = false;
          }*/
          procesarParentSelect(filterChanged, newValue, Object.keys(_DTHeadersInfo[filterChanged].cats), actualizar);
        } else {
          //Si no tiene parent, actualizar el propio selector.
          procesarSelect(filterChanged);
        }

        //Adaptar valores de filtro Hijo (si lo hay)
        if (_DTHeadersInfo[filterChanged].child !="" && _DTHeadersInfo[filterChanged].child !=undefined){
           procesarChildSelect(filterChanged, newValue, Object.keys(_DTHeadersInfo[filterChanged].cats));
        }

      },
      error : function(data){
        console.log("Error ", data);
      }
    });

  }









  updateDynamicDatatables = function (){

    $.each($('.dynamic-datatable'),function(index, obj){
      var viewName = $(obj).data("view");
      var selectRows= $(obj).data("select-rows")=="1";
      var openInNewWindowParam = $(obj).data("open-new-window");
      var newWindowBig = $(obj).data("new-window-big");
      var editMode = $(obj).data("open-edit-mode");

      var selectRowsMultiple= ($(obj).data("select-multiple")==undefined || $(obj).data("select-multiple") == "1")? true : false;
      var tableID = $(obj).prop("id");
      var tableTitle = $(obj).data("title") != undefined ? $(obj).data("title") : "" ;
      var heavyData= ($(obj).data("heavy-data")==undefined || $(obj).data("heavy-data") == "0")? false : true;
      var categorizedFilters = ($(obj).data("categorized-filters")==undefined || $(obj).data("categorized-filters") == "0")? false : true;
      var showUnread = ($(obj).data("show-unread")==undefined || $(obj).data("show-unread") == "0")? "0" : "1";
      var onlyUnread = ($(obj).data("show-only-unread")==undefined || $(obj).data("show-only-unread") == "0")? "0" : "1";
      var customAgent = $(obj).data("custom-agent")? $(obj).data("custom-agent") : "";
      var customHeader = $(obj).data("custom-header")? $(obj).data("custom-header") : "";
      var customForm =  $(obj).data("custom-form")? $(obj).data("custom-form") : "";
      
      var params={"viewName": viewName ,
                  "tableID" : tableID, 
                  "selectRows" : selectRows, 
                  "tableTitle" : tableTitle, 
                  "selectRowsMultiple": selectRowsMultiple, 
                  "openInNewWindow": openInNewWindowParam, 
                  "newWindowBig": newWindowBig, 
                  "heavyData": heavyData, 
                  "editMode": editMode, 
                  "categorizedFilters" : categorizedFilters, 
                  "showUnread": showUnread, 
                  "onlyUnread": onlyUnread,
                  "customAgent": customAgent,
                  "customHeader" : customHeader,
                "customForm" : customForm };
        
      if ( $.fn.dataTable.isDataTable( "#"+params.tableID ) ) {
          $("#"+params.tableID).DataTable().ajax.reload();
          translateHTMLBlock("#"+params.tableID);
          return;
      }

      var headerApiURL = "";
      if (params.customHeader) {
        headerApiURL = params.customHeader;
      }else {
        headerApiURL = "API_Datatables_GetHeader?OpenAgent&view=" + viewName;
      }
      
      $.ajax({
        url: headerApiURL,
        dataType: "json",
        success: function (data) {
          var headers = "";
          var collapsable=false;
          params.collapsedCols=[];
          $.each(data, function (index, obj) {

            // Ordenar
            if (obj.ordered == "False"){
              data[index].orderable = false;
            } else {
              data[index].orderable = true;
            }
            //Ordenar filtros categorizados
            if (obj.catFilters){
              $.each(obj.catFilters, function(key){
                data[index].catFilters[key].childrenValues=data[index].catFilters[key].childrenValues.sort();
                data[index].catFilters[key].parentValues=data[index].catFilters[key].parentValues.sort();
              });
                _DTHeadersInfo[obj.data]={};
                _DTHeadersInfo[obj.data].cats = obj.catFilters;
                _DTHeadersInfo[obj.data].parent = data[index].parent;
                _DTHeadersInfo[obj.data].child = data[index].child;
                _DTHeadersInfo[obj.data].position = data[index].position;
                _DTHeadersInfo[obj.data].allFilters = data[index].filters;
            }

            // Colapasar
            if (obj.collapsed=="True"){
              collapsable=true;
              data[index].collapsable=true;
              params.collapsedCols.push({"id":index, "data":obj.data})
            } else {
                data[index].collapsable=false;
            }
            headers = headers + '<th><span data-translate="' + obj.data + '"></span></th>';
          });

          if (collapsable){
            params.collapsable=true;
          }else {
            params.collapsable=false;
          }
          if (selectRows){
            if(selectRowsMultiple){
                headers = headers + '<th tabindex="0" aria-controls="'+params.tableID+'" style="width: 0px;" rowspan="1" colspan="1"><input class="checkbox" type="checkbox" name="cb_viewTitle" value="ALL"/></th>'
            }else {
                headers = headers + '<th tabindex="0" aria-controls="'+params.tableID+'" style="width: 0px;" rowspan="1" colspan="1"></th>'
            }
          }
          if (params.collapsable){
            headers = headers + '<th style="width: 0px;"></th>'
          }

          $("#"+tableID).html("<thead><tr>" + headers + "</tr></thead>");
          translateHTMLBlock($("#"+params.tableID));
          initializeTable(data, params);
        },
        error : function(data, error, status){
          console.log(error);
          console.log(status);
          console.log(data);
        }
      });
    });
  }

  table_datatable = [];
  reloadDatatable = function () {
    if (table_datatable != null && table_datatable != undefined && table_datatable.length > 0) {
      for(var i = 0; i < table_datatable.length ; i++) {
        table_datatable[i].ajax.reload(null, false);
      }
    }
  }

function initializeTable(columnsDef,params) {
  $("#"+params.tableID+" thead tr").clone(true).prependTo("#"+params.tableID+" thead");

  $("#"+params.tableID+" thead tr:eq(0) th").each(function (i) {
    var max= $("#"+params.tableID+" thead tr:eq(1) th").length;
    if (params.selectRows){max--;}
    if (params.collapsable){max--;}
    if (parseInt(i) < max){
      var title = $(this).text();
      if (columnsDef[i].collapsed=="False"){
        if (columnsDef[i].static=="False") {
          if (columnsDef[i].filtered=="True") {
            columnsDef[i]["filters"].sort();
            var filterOptions='<option data-keep="true" selected value="">'+ translate("Buscar") +' '+ title +' ...</option>';
            for (var n=0;n<columnsDef[i]["filters"].length;n++){
              var el = $(columnsDef[i].filters[n]);
              var valueToDisplay=columnsDef[i].filters[n];
              var valueToSearch=columnsDef[i].filters[n]
              var categoryToSearch=columnsDef[i].filters[n]
              $.each(el, function(index,obj){
                if ($(obj).data("translate")!=undefined){
                  valueToDisplay=translate($(obj).data("translate"));
                }
                if ($(obj).data("search-key")!=undefined){
                  if (params.categorizedFilters) {
                    categoryToSearch=$(obj).data("search-key");
                  }else {
                    valueToSearch=$(obj).data("search-key");
                  }

                }
              });
              var cat = categoryToSearch != "" ? 'data-category="'+ categoryToSearch+'"':"";
              filterOptions = filterOptions + '<option '+cat+' data-keep="false" value="'+valueToSearch+'">'+valueToDisplay+'</option>';
            }
            $(this).addClass("p-0 text-nowrap").html('<select data-filter="'+columnsDef[i].data+'" class="filter form-control d-inline">'+filterOptions+'</select>');
          }
          else {
              $(this).addClass("p-0 text-nowrap").html('<input data-priority="1" type="text" class="filter form-control d-inline" placeholder="'+ translate("Buscar") +' '+ title + '..." /><i class="fa fa-filter filterIcon"></i>');
          }
        }
        else {
          $(this).html("");
        }
      }else {
          $(this).html("");
      }

      $(".filter", this).on("change", function () {
        var filterChanged = $(this).attr("data-filter")
        var thisValue = this.value;

        if (params.categorizedFilters){
          propagarCambiosDeFiltrosCategorizados(params.viewName, params.tableID, filterChanged, this.value);
        }

        if (table.column(i).search() !== this.value) {
          table.column(i).search(this.value).draw();
        }
      });
    }else {
      $(this).html("");
    }
  });

  var counts=[10, 25, 50, -1];
  var userPageLenth=counts.reduce(function(prev, curr) {
    return (Math.abs(curr - jsParamCountVistas) < Math.abs(prev - jsParamCountVistas) ? curr : prev);
  });
  var restrictToCategory=jsRestrictToCategory!=""?("&RestrictToCategory="+jsRestrictToCategory):"";

  var columnas = [];
  if (params.collapsable){
    columnas.push({
      targets:[params.selectRows? (columnsDef.length+1):(columnsDef.length)],
      data: null,
      defaultcontent: '',
      className: 'dt-control-cell',
      visible: true,
      orderable:false,
      render: function (data, type, row) {
        return ('<i class="fa fa-chevron-circle-down dt-control"></i>');
      }
    });
  }

  if (params.selectRows){
    columnas.push({
      targets:[columnsDef.length],
      responsivePriority:100,
      data: null,
      defaultcontent: '',
      visible: true,
      orderable:false,
      render: function(data, type, row){
          return ('<input type="checkbox" '+(params.selectRowsMultiple?"":"data-unique ")+'class="form-check" name="cb_view" value="'+row["unid"]+'"/>');
      },
    });
  }

  for (c=0;c < params.collapsedCols.length;c++){
    columnas.push({
      targets:[params.collapsedCols[c].id],
      visible:false
    });
  }




  columnas.push({
    render: function (data, type, row) {
      if (params.customForm) {
        return (
          '<a class="documentLink" href="' + jsPathBDWeb + "/"+params.customForm+"?OpenForm&id=" +  row["unid"] +'">' + data + '</a>'
        );
      }else {
        return (
          '<a class="documentLink" href="' + jsPathBDWeb + "/X/" + row["unid"] +'?OpenDocument">' + data + '</a>'
        );
      }
      
      },
      targets: "_all",
    });

    var apiUrl = "";
 
    if (params.customAgent) {
      apiUrl = params.customAgent;
    }else {
      apiUrl = (params.heavyData?"API_Datatables_GetHeavyData":"API_Datatables_GetData");
    }

    var table = $("#"+params.tableID).DataTable({
    columns: columnsDef,
    order: [],
    orderCellsTop: false,
    processing: true,
    serverSide: true,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, translate("DATATABLES_All")]],
    pageLength: userPageLenth,
    language: {
      lengthMenu: translate("DATATABLES_lengthMenu"),
      zeroRecords: translate("DATATABLES_zeroRecords"),
      info: translate("DATATABLES_info"),
      infoEmpty: translate("DATATABLES_infoEmpty"),
      infoFiltered: translate("DATATABLES_infoFiltered"),
      search: translate("DATATABLES_search"),
      loadingRecords: translate("DATATABLES_loadingRecords"),
      processing: translate("DATATABLES_processing"),
      paginate: {
        first: translate("DATATABLES_paginate_first"),
        last: translate("DATATABLES_paginate_last"),
        next: translate("DATATABLES_paginate_next"),
        previous: translate("DATATABLES_paginate_previous"),
      },
    },
    ajax: {
      url: apiUrl +"?OpenAgent&view=" + jsViewName+restrictToCategory+"&showUnread="+params.showUnread+"&onlyUnread="+params.onlyUnread,
      type: "GET",
      contentType: "application/json;charset=utf-8",
      data: function (data) {
        //stopLoad();
        data.customSearch = [];
        $('.customSearch').each(function(index,obj){
          if ($(obj).val()){
            data.customSearch.push({data: $(obj).data("field"), value: $(obj).val()})
          }
        });
        return decodeViewData(data);
      },
    },
    columnDefs: columnas,
    dom:'lBftip',
    buttons :[
      {extend:'collection',
      text:'<i class="fa fa-bars export-menu"></i>',
      buttons: [{extend: 'excel', messageTop : params.tableTitle, "text":'<i class="fa fa-regular fa-file-excel mr-3"></i>Excel' },
                {extend: 'pdf', messageTop : params.tableTitle, "text":'<i class="fa fa-regular fa-file-pdf mr-3"></i>PDF' },
                {extend: 'print', messageTop : params.tableTitle, "text": '<i class="fa fa fa-print mr-3"></i>' + translate("DATATABLES_print") }
               /* {"text":'<i class="fa fa-regular fa-download mr-3"></i>Download', action: function( e, dt, node, config){
                  console.log("ACTION!");
                  console.log(dt.table().columns().column(1).data());
                  console.log(dt.table().columns().column(1).search());
                }}*/
            ]
      }],

    createdRow: function( row, data ) {
                    if ( data.unread == "1" ) {        
                      $(row).addClass('unread');
                    }
    },
    drawCallback: function () {
        var dtReady = new CustomEvent("dtReady", {detail:{"tableID" : params.tableID}});
        document.dispatchEvent(dtReady);
        $('input[name="cb_viewTitle"]').click(function(){
          if ($(this).prop('checked')){
            $('input[name="cb_view"]').prop('checked', 'checked');
          }else {
            $('input[name="cb_view"]').prop('checked', '');
          }
        });

        $('input[name="cb_view"]').click(function(){
          if ($(this).attr('data-unique') != undefined){
            if ($(this).prop('checked')==""){
                $(this).prop('checked', '');
            }else{
              $('input[name="cb_view"]').prop('checked', '');
              $(this).prop('checked', 'checked');
            }
          }
          $('input[name="cb_viewTitle"]').prop('checked', '');
        });

        $('i.dt-control').on('click', function () {
          var tr = $(this).closest('tr');
          var row = table.row( tr );

          if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
              $(this).removeClass("fa-chevron-circle-up");
              $(this).addClass("fa-chevron-circle-down");
          }
          else {
              row.child( showCollapsedData(row.data(),params.collapsedCols)).show();
              tr.addClass('shown');
              $(this).addClass("fa-chevron-circle-up");
              $(this).removeClass("fa-chevron-circle-down");
          }
      } );

      translateHTMLBlock($('#'+params.tableID));

      var listaDocumentosNavegables=[];
      $.each($(".documentLink"), function (index, obj) {
        $(obj)
          .unbind("click")
          .click(function (event) {
            event.preventDefault();
          });

        var ahref=$(obj).parent().find("a.documentLink").prop("href");
        if (ahref){
          if ( ahref.toLowerCase().indexOf("?opendocument") > 1 ) {
            if (!listaDocumentosNavegables.includes(ahref)){
              listaDocumentosNavegables.push(ahref);
            }
          }
        }
        setSessionData("documentosNavegables",listaDocumentosNavegables);

        var openNewWindowLocal = openInNewWindow;
        if (params.openInNewWindow != null && params.openInNewWindow !== undefined) {
          openNewWindowLocal = params.openInNewWindow;
        }
        var newWindowBigLocal = false;
        if (params.newWindowBig != null && params.newWindowBig !== undefined) {
          newWindowBigLocal = params.newWindowBig == 1;
        }
        var editMode = false;
        if (params.editMode != null && params.editMode !== undefined) {
          editMode = params.editMode == 1 | params.editMode == true;
        }


        $(obj)
          .parent()
          .click(function () {
            var ahref = $(this).find("a.documentLink").prop("href");
            if (ahref.toLowerCase().indexOf("?opendocument") > 1) {
              if (editMode) { //Cambiamos a modo edición
                ahref = ahref.replace(/opendocument/gi, "EditDocument");
              }
              setSessionData("urlOrigen", window.location.href);
              if (openNewWindowLocal == 1) {
                openURLNewWindow(ahref, 600, newWindowBigLocal);
              } else {
                location.href = ahref;
              }
            } else if (params.customForm){
              if (openNewWindowLocal == 1) {
                openURLNewWindow(ahref, 600, newWindowBigLocal);
              } else {
                location.href = ahref;
              }
            }else {
              location.href = ahref;
            }
            
          
          });
      });
    },
  });

  table_datatable.push(table);
}

showCollapsedData = function (data, collapsedCols){
  var child="";
  for (var i =0; i < collapsedCols.length; i++){
    var translateChild = translate(collapsedCols[i].data);
    translateChild = translateChild.trim();
    if ("" == translateChild) {
      child += "<span class=\"content-collapsed\">"+data[collapsedCols[i].data]+"</span><br/>"
    } else {
      child += "<strong>"+translate(collapsedCols[i].data)+": </strong><span class=\"content-collapsed\">"+data[collapsedCols[i].data]+"</span><br/>"
    }
  }
  return child;
}


/**
*
*
**/

procesarSelect = function(filtro){
  var newValues=_DTHeadersInfo[filtro].allFilters;

  newValues = newValues.sort();
  var current = $('.filter[data-filter='+filtro+']');
  var thisvalue=$(current).val();
  var optionSelected = $(current).find("option:selected").attr("value");
  if (optionSelected = ""){
    optionSelected = null;
  }

  $(current).find("option[data-keep='false']").remove();
  for (var nc = 0; nc < newValues.length; nc++){
    var sel = thisvalue == newValues[nc] || newValues[nc]==optionSelected ?"selected":"";
    var tempObj = $(newValues[nc]);
    var categoryToSearch = $(tempObj).attr("data-search-key");
    var cat = 'data-category="'+ categoryToSearch+'"';
    var option = '<option '+cat+' data-keep="false" '+sel+' value="' + newValues[nc] +'">'+newValues[nc]+'</option>';
    $(current).append(option);
  }


}


procesarParentSelect = function (filtro, valorHijoSeleccionado, valoresHijo , actualizarHijo) {
  var parentID = _DTHeadersInfo[filtro].parent;
  var position = _DTHeadersInfo[parentID].position;
  var parent = $('.filter[data-filter='+_DTHeadersInfo[filtro].parent+']');
  var parentValue =  $(parent).val();

  var optionSelected = $(parent).find("option:selected").attr("value");

  if (optionSelected == ""){
    optionSelected = null;
  }

  $(parent).find("option[data-keep='false']").remove();

  var parentValues=[];
  if ((valorHijoSeleccionado =="" && valoresHijo == null) || (_ResetFilters && optionSelected !=null)){
    parentValues=_DTHeadersInfo[parentID].allFilters;
  }else {
    if (valorHijoSeleccionado != ""){
      parentValues=_DTHeadersInfo[filtro].cats[valorHijoSeleccionado].parentValues;
    }else {
      for (var i = 0; i<valoresHijo.length;i++){
        if (_DTHeadersInfo[filtro].cats[valoresHijo[i]]){
          var temp=_DTHeadersInfo[filtro].cats[valoresHijo[i]].parentValues;
          for (j = 0; j< temp.length;j++){
            if (parentValues.indexOf(temp[j]) == -1){
              parentValues.push(temp[j]);
            }
          }
        }
      }
    }
  }
  parentValues = parentValues.sort();
  var newChildValues = [];
  for (var p = 0; p < parentValues.length; p++){
    var sel = parentValue == parentValues[p] || parentValues[p] == optionSelected ? "selected" : "";
    var tempObj = $(parentValues[p]);
    var categoryToSearch = $(tempObj).attr("data-search-key");
    var cat = 'data-category="'+ categoryToSearch+'"';
    var option = '<option '+cat+' '+sel+' data-keep="false" value="' + parentValues[p] +'">'+parentValues[p]+'</option>';
    $(parent).append(option);
    if (actualizarHijo){
      //Preparar lista de valores posibles para el hijo según los valors posibles en el filtro padre
      if (_DTHeadersInfo[parentID].cats[parentValues[p]]){
        var parentChidValues = _DTHeadersInfo[parentID].cats[parentValues[p]].childrenValues;
        if (parentChidValues != undefined){
          for (cv = 0 ; cv < parentChidValues.length; cv++){
            if (newChildValues.indexOf(parentChidValues[cv]) == -1){
              newChildValues.push(parentChidValues[cv]);
            }
          }
        }
      }

    }
  }

  if (actualizarHijo){
    newChildValues = newChildValues.sort();
    var hijo = $('.filter[data-filter='+filtro+']');
    var thisvalue=$(hijo).val();
    var optionSelected = $(hijo).find("option:selected").attr("value");
    if (optionSelected = ""){
      optionSelected = null;
    }

    $(hijo).find("option[data-keep='false']").remove();
    newChildValues = newChildValues.sort();
    for (var nc = 0; nc < newChildValues.length; nc++){
      var sel = thisvalue == newChildValues[nc] || newChildValues[nc]==optionSelected ?"selected":"";
      var tempObj = $(newChildValues[nc]);
      var categoryToSearch = $(tempObj).attr("data-search-key");
      var cat = 'data-category="'+ categoryToSearch+'"';
      var option = '<option '+cat+' data-keep="false" '+sel+' value="' + newChildValues[nc] +'">'+newChildValues[nc]+'</option>';
      $(hijo).append(option);
    }
  }

  if(_DTHeadersInfo[parentID].parent != "") {
    procesarParentSelect(parentID, "", parentValues, false);
  }
}

procesarChildSelect = function (filtro,valorPadreSeleccionado, valoresPadre){
  var childID=_DTHeadersInfo[filtro].child;
   var child = $('.filter[data-filter='+_DTHeadersInfo[filtro].child+']');
   var childValue =  $(child).val();
   var optionSelected = $(child).find("option:selected").attr("value");
   if (optionSelected == ""){
     optionSelected = null;
   }

   $(child).find("option[data-keep='false']").remove();

   var childValues=[];
   if ((valorPadreSeleccionado == "" && valoresPadre == null) || (_ResetFilters && optionSelected !=null)){
     childValues=_DTHeadersInfo[childID].allFilters;
   }else {
     if (valorPadreSeleccionado != ""){
       childValues=_DTHeadersInfo[filtro].cats[valorPadreSeleccionado].childrenValues;
     }else {
       for (var i = 0; i<valoresPadre.length;i++){
         if (_DTHeadersInfo[filtro].cats[valoresPadre[i]]) {
           var temp=_DTHeadersInfo[filtro].cats[valoresPadre[i]].childrenValues;
           for (j = 0; j< temp.length;j++){
             if (childValues.indexOf(temp[j]) == -1){
               childValues.push(temp[j]);
             }
           }
         }
       }
     }
   }
   childValues=childValues.sort();
   for (var p = 0; p < childValues.length; p++){
     var sel = childValue == childValues[p] || childValues[p] == optionSelected ? "selected" : "";
     var tempObj = $(childValues[p] );
     var categoryToSearch = $(tempObj).attr("data-search-key");
     var cat = 'data-category="'+ categoryToSearch+'"';
     var option = '<option '+cat+' '+sel+' data-keep="false" value="' + childValues[p]  +'">'+childValues[p] +'</option>';
     $(child).append(option);
   }
   if(_DTHeadersInfo[childID].child != "") {
     procesarChildSelect(childID, "", childValues);
   }
}



iniValidationsGlobal = function(frmValidator, formName)
	{
		frmValidator = new Validator(formName);
		// Eliminamos las validaciones que pueda haber
		frmValidator.clearAllValidations();

    var fieldsToValidate=$('[data-validation]');

    if (fieldsToValidate){
      for (var i=0;i< fieldsToValidate.length;i++){
        var validations=$(fieldsToValidate[i]).data("validation").split(",");
        for (var n=0;n<validations.length;n++){
          var id=fieldsToValidate[i].id;
          var validation=validations[n].toString().trim();
          var translated_item = undefined;
          if ($(fieldsToValidate[i]).data("translate-title") != undefined) {
            translated_item = translate($(fieldsToValidate[i]).data("translate-title"), true);
          }
          switch (validation){
            case "unique":
              var pKey=$('#'+id).val().toUpperCase()+"_._";
              frmValidator.addValidation(id, "function=validateUnique(jsDbURL, '(Unique."+id+")', '"+pKey+"', 2 , jsIdDoc)~"+'litValorUnico', translated_item);
              break;
            case "eq":
              var equalTo = $(fieldsToValidate[i]).data("equal");
              frmValidator.addValidation(id, "function=validateEqual('"+ $(fieldsToValidate[i]).val()+"', '"+equalTo+"')~"+'litValorDiferente', translate("litValorDiferente"))
              break;
            default:if (validation!=""){frmValidator.addValidation(id,validation,translated_item);}
          }

        }
      }
    }

		return frmValidator;
}


/************* CÓDIGO DE VISTAS CON SCROLL INFINITO Y VISUALIZACIÓN DE ELEMENTOS PERSONALIZADA *************/

// Configuración paginación
var currentPageControl = 0;
var lengthScroll = 6;
var searchText = "";
scrollListHeaders = undefined;

reloadScrollContainer = function() {
  if ($("#scroll-container") != undefined && $("#scroll-container").length > 0) {
    callScrollSearch();
    $(window).scrollTop(0);
  }
}

callScrollSearch = function() {
  currentPageControl = 0;
  $("#scroll-container").html("");
  searchText = $("#searchInput").val();
  var template = $("#scroll-container").data("template");
  callAjaxElements(template);
}

updateInfiniteScrollTables = function() {
  // Si hay id de scroll
  if ($("#scroll-container") != undefined && $("#scroll-container").length > 0) {
    // Buscador
    var inputSearchCode =   '<div class="form-group float-left">'+
                            '<div class="dt-buttons btn-group flex-wrap"><div class="btn-group"><button class="btn btn-default" type="button" onclick="callScrollSearch()"><span><i class="fa fa-bars export-menu"></i></span></button></div></div>' +
                            '<div class="input-group"><div class="input-group-addon"><i class="fa fa-search search-icon"></i> &nbsp</div>'+
                            '<input id="searchInput" type="search" class="form-control" placeholder="">'+
                            '<div class="input-group-button"></div>' +
                            '</div></div>';
    $("#scroll-container").before(inputSearchCode);

    // Pull para recargar listado
    var loadMore = '<div id="scroll-load-more">'+translate("MoreResults")+'</div>';
    $("#scroll-container").after(loadMore);
    $("#scroll-load-more").off("click").on("click", function() {
      var template = $("#scroll-container").data("template");
      callAjaxElements(template);
    });

    $('#scroll-container').mkPullFresh(function() { location.reload(); }, {
      indicatorHtml:'<div class="mkpf-envelop"><div class="mkpf-indicator-wrapper"><div class="mkpf-indicator"><div class="mkpf-icon-wrapper"><i class="mkpf-arrow-down"></i></div><i class="mkpf-spinner"></i></div></div></div>'
    });

    // Carga de los primeros elementos
    scrollListHeaders = callAjaxHeaders();
    var template = $("#scroll-container").data("template");
    callAjaxElements(template);

    // Carga dinámica al hacer scroll
    $(window).on("scroll", function() {
      var scrollHeight = $(document).height();
      var scrollPos = $(window).height() + $(window).scrollTop();

      if(((scrollHeight - 200) >= scrollPos) / scrollHeight == 0){
        callAjaxElements(template);
      }
    });
  }
}

var callAjaxHeaders = function() {
  var showLoader = true;
  var results;

	$.ajax({
		url: "API_Datatables_GetHeader" + "?OpenAgent&view=" + jsViewName,
		async: false,
		type: "GET",
		dataType: "json",
		success: function (data) {
			results = decodeViewData(data);
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + jsViewName);
		}
	});

  return results;
}

var callAjaxElements = function(template) {
  var dataParams = {};
  dataParams.draw = 1;

  dataParams.length = lengthScroll;
  dataParams.start = currentPageControl;

  dataParams.search = {};
  dataParams.search.value = "";
  dataParams.search.regex = false;

  if (searchText) {
    dataParams.search.value = searchText;
  }
  dataParams.columns = [];

  for(var i=0; i<scrollListHeaders.length; i++) {
    dataParams.columns[i] = {};
    dataParams.columns[i].data = scrollListHeaders[i].data;
    dataParams.columns[i].name = "";
    dataParams.columns[i].searchable = false;
    dataParams.columns[i].orderable = false;

    dataParams.columns[i].search = {}
    dataParams.columns[i].search.value = "";
    dataParams.columns[i].search.regex = false;
  }

  var defaultOrder = "asc";

  if (scrollListHeaders[0] != undefined && scrollListHeaders[0].defaultOrder) {
    defaultOrder = scrollListHeaders[0].defaultOrder
  }

  // Orden fijado en la primera columna...
  dataParams.order = [];
  dataParams.order[0] = {}
  dataParams.order[0].column = 0;
  dataParams.order[0].dir = defaultOrder;

  var restrictToCategory = "";
  var showLoader = true;

	$.ajax({
		url: "API_Datatables_GetData" + "?OpenAgent&view=" + jsViewName + restrictToCategory,
		async: false,
		type: "GET",
		dataType: "json",
    data: dataParams,
		success: function (data) {
      if (data.data.length > 0) {
        currentPageControl = currentPageControl + lengthScroll ; // Actualizamos... la página en la que estamos
        $("#scroll-load-more").html(translate("MoreResults"));
      } else {
        $("#scroll-load-more").html("")
      }
			if (showLoader) {
				stopLoad();
			}
			dataResult = decodeViewData(data);
			drawElements(dataResult, template);

      // Lanzamos evento de fin de carga (el mismo que las datatables)...
      document.dispatchEvent(new CustomEvent("dtReady", {detail:{"id" : "#scroll-container"}}));
		},
		error: function (xhr, status) {
			if (showLoader) {
				stopLoad();
			}
			console.log("error en : " + jsViewName);
		}
	});
}

var drawElements = function(result, template) {
  if (result != undefined & result.data != undefined) {
    for (var i=0; i<result.data.length; i++) {
      var htmlElement = "";
      if (template != undefined && _ScrollTemplateList != undefined && _ScrollTemplateList[template]) {
        htmlElement = _ScrollTemplateList[template];
        let entries = Object.entries(result.data[i]);
        for (var j=0; j < entries.length; j++) {
          var key = entries[j][0];
          var label = translate(key, true).trim();
          var value = entries[j][1];
          htmlElement = htmlElement.replace("##"+key+"_key##", label);
          htmlElement = htmlElement.replace("##"+key+"_value##", value);
          htmlElement = htmlElement.replace("##Database##", jsPathBDWeb);
        }
      } else {
        // Sin plantilla se mostrará un recuadro con todos los valores clave: valor de la tabla
        htmlElement = '<div class="scroll-list-element">';
        let entries = Object.entries(result.data[i]);
        for (var j=0; j < entries.length - 1; j++) {
          var label = translate(entries[j][0], true).trim();
          if (label != "") {
            htmlElement += '<div class="mb-3"><strong>'+label+'</strong>: <span>'+entries[j][1]+'</span></div>';
          } else {
            htmlElement += '<div class="mb-3">'+entries[j][1]+'</div>';
          }
        }
        htmlElement += '</div>';
      }

      $("#scroll-container").append(htmlElement);
    }

    // Links en popup...
    $.each($("a.documentLink"), function (index, obj) {
      $(obj).unbind("click").click(function (event) {
          event.preventDefault();
          event.stopPropagation();
      });
      $(obj).click(function () {
        var ahref = $(this).prop("href");
        openURLNewWindow(ahref, 600);
      });
    });

    // Navegac. de documentos
    var listaDocumentosNavegables = [];
    $.each($(".documentLinkNav"), function (index, obj) {
      var ahref=$(obj).parent().find("a.documentLinkNav").prop("href");
      if (ahref){
        if ( ahref.toLowerCase().indexOf("?opendocument") > 1 ) {
          if (!listaDocumentosNavegables.includes(ahref)){
            listaDocumentosNavegables.push(ahref);
          }
        }
      }
      setSessionData("documentosNavegables", listaDocumentosNavegables);
    });

    translateHTMLBlock("#scroll-container");
  }
}
/************* CÓDIGO DE VISTAS CON SCROLL INFINITO Y VISUALIZACIÓN DE ELEMENTOS PERSONALIZADA *************/

/************* FUNCIONES DEL MÓDULO DE AVATARES DE USUARIO  *************/

userAvatars = undefined;
initializeAvatarModule = function() {
  if (userAvatars == undefined) {
    // Se recuperan las imágenes de usuario
    runGETAgent(jsDbURL, "API_USUARIOS_GetPics", initializeAvatarModuleCallback, false, "", "", true);
  } else {
    // se actualizan los módulos de avatar existentes
    loadAvatarModule();
  }
}

initializeAvatarModuleCallback = function(data) {
  userAvatars = {};
  if (data != undefined ) {
    for (var i=0; i < data.length; i++) {
      userAvatars[data[i].usuario] = data[i].enlace;
    }
    loadAvatarModule();
  }
}

loadAvatarModule = function(){
  $(".avatar-module").each(function(index) {
    var content = "";
    var users = $(this).data("users");
    var arrUsers = users.split(',');
    for(var i=0; i < arrUsers.length; i++) {
      if (userAvatars != undefined) {
        var url = userAvatars[arrUsers[i].trim()];
        if (url == undefined) {
          url = userAvatars["defaultpic"];
        }
        content = content + '<img class="user-avatar-module" title="'+arrUsers[i]+'" src="'+url+'">';
      }
    }
    $(this).html(content);
  });
}

/*************  FIN FUNCIONES DEL MÓDULO DE AVATARES DE USUARIO  *************/

/************************* FUNCIONES FUNCIONALIDAD DROPZONE PARA ANEXOS ************************/
/*updateDropZone = function() {
  Dropzone.autoDiscover = false;

  $.each($('.bloque-anexos-nuevo'),function(index, obj){
      var parentFolder = $(obj).data("parent-folder");
      var author = $(obj).data("author");
      console.log("parentFolder: " + parentFolder);
      //var url = jsDbURL  + "/API_Anexo_Add_Dropzone"+"?OpenAgent"
      var url = jsDbURL  + "/app/v1/anexo/add";
      $(obj).dropzone({
          url: url,
          autoProcessQueue: false, // Para que no se procesen los ficheros automáticamente, y que se haga mediante llamada ajax
          ignoreHiddenFiles: true,
          //acceptedFiles: null, // Para los tipos de fichero aceptados, por si queremos filtrar... Ej.: `image/*,application/pdf,.psd`
          accept: function(file, done) {
            var myDropzone = this;
            console.log(file);
            var reader = new FileReader();
            reader.onload = handleReaderLoad;
            reader.readAsDataURL(file);

            function handleReaderLoad(evt) {
              // Preparación datos
              var dataForm = {};
              //dataForm["base64"] = evt.target.result;
              dataForm["filename"] = file.name;
              dataForm["filetype"] = file.type;
              dataForm["fullpath"] = file.fullPath
              dataForm["parent"] = parentFolder;
              dataForm["author"] = author;

              // Llamada a Agente en el servidor
              $.ajax( {
                method: "POST",
                timeout: 2000,
                url: url,
                data: dataForm
              }).done(function(result) {
                console.log(result);
                console.log( "success" );

                if (result.result == "ok") {
                  var element = $(file.previewElement).find(".dz-success-mark");
                  var elementSVG = $(file.previewElement).find(".dz-success-mark svg");
                  element.css("opacity","1");
                  element.css("background-color","green");
                  element.css("border-radius","50%");
                  elementSVG.width("25px");
                  elementSVG.height("25px");
                } else {
                  var element = $(file.previewElement).find(".dz-error-mark");
                  var elementSVG = $(file.previewElement).find(".dz-error-mark svg");
                  element.css("opacity","1");
                  element.css("background-color","red");
                  element.css("border-radius","50%");
                  elementSVG.width("25px");
                  elementSVG.height("25px");
                }
              })
              .fail(function(result) {
                //console.log(result);
                console.log( "fail" );
                var element = $(file.previewElement).find(".dz-error-mark");
                var elementSVG = $(file.previewElement).find(".dz-error-mark svg");
                element.css("opacity","1");
                element.css("background-color","red");
                element.css("border-radius","50%");
                elementSVG.width("25px");
                elementSVG.height("25px");
              })
              .always(function() {
                console.log( "complete" );
                function removeFile(myDropzone, file) {
                  console.log(file.name);
                  myDropzone.removeFile(file);
                }
                setTimeout(removeFile, 15000, myDropzone, file);
              });
            }
            done();
          }
      });
      if (!$(obj).hasClass()) {
          $(obj).addClass("dropzone");
      }
  });
}*/
/*********************** FIN FUNCIONES FUNCIONALIDAD DROPZONE PARA ANEXOS **********************/

function comprobarCantidadAnexos(data, parentID){
    //console.log(data);
}

abrirVistaExterna = function(dbURL, vista, dbURLDest, newWindow)
{

	var count = "50" //getCountParaVistas(dbURL, "(Configuracion)", 2); // Coge el nÂº de filas a mostrar por la vista
	if (count == undefined)
		count = "";
	else
		if (count != "")
			count = "&Count=" + count;

	var dbAbrir = dbURL;

	switch (dbURLDest)
	{
	case "jsDbLog":
		dbAbrir = jsDbLog;
		break;

	}

	var newURL = dbAbrir + "/" + vista + "?OpenView" + count + "&Login&cache=" + String(Math.random());
	if (newWindow){
    window.open(newURL);
  }else {
    	window.location = newURL;
  }

};

destroyDatatables = function () {
  if (table_datatable != null && table_datatable != undefined && table_datatable.length > 0) {
    for(var i = 0; i < table_datatable.length ; i++) {
      table_datatable[i].destroy(false);
    }
  }
}

})(jQuery);


document.addEventListener("LanguageReady", function(){
  qsParams = createArrayQS();

  if ($("#embedView > table").length>0){
    startLoad();
    $("#embedView > table").stripeNotesView({tableClass: "notesView selectTable ", alternateRowClass:"alternateRow", overClass:"filaOn", numCategorias:jsNumCategorias});

    filtrarURL();

    if (qsParams["count"] == undefined)
      addQSParam(qsParams, "Count=-1");
    if (qsParams["start"] == undefined)
      addQSParam(qsParams, "Start=1");

    deshabilitarAccionesVista(qsParams);
    formatNoDocumentsFound();
    stopLoad();
  }

  updateDynamicDatatables();
  updateInfiniteScrollTables(); // Vistas con Scroll inifinito
  //updateDropZone(); // Zonas de dropzone para nuevos anexos

});
