

$(document).ready(function(){


	$("a.openExternalView").bind("click", function(e) {
			// la variable de jsDBURL será la que se llame igual que el nombre de this.id tras el _ que se ponga en el id del navegador!!!!

			var arrTemp = this.id.split("_");
			var newWindow = $(this).hasClass("newWindow");
			abrirVistaExterna(jsDbURL, arrTemp[0], "jsDb" + arrTemp[1], newWindow);
			e.preventDefault();
		});


		$(".SinIntros").bind("blur", function(e){

			var idAct = this.id;
			texto = $("#" + idAct).val();

			//NO se admitirán carácteres que puedan dar problemas despues con el autocomplete de la via!
			texto = replaceSubstring(texto, String.fromCharCode(10), " ");
			texto = replaceSubstring(texto, String.fromCharCode(13), " ");
			texto = replaceSubstring(texto, String.fromCharCode(9), " ");
			texto = replaceSubstring(texto, ";", " ");
			texto = replaceSubstring(texto, ",", " ");
			texto = fullTrim(texto);

			$("#" + idAct).val(texto);
		})


	// Zona de ayuda contextual
	$("#helpzoneAction").bind("click", function(e) {
		toggleHelp();
	});

	var md = new MobileDetect(window.navigator.userAgent);
	if (md.mobile() == null) { //Si no estamos en móvil, desplegamos la ayuda.
		toggleHelp();
	}

	var isIframe = window.location !== window.parent.location;
	if (isIframe) {
		$("#closebutton").toggle();
		$("input[type=button][name=salir]").toggle();
		$("#buttonSalirDiv").toggle();
	}

	// Inicialización DatePicker
	$('.datepicker-input').datepicker({
		format: 'dd/mm/yyyy',
		language: 'es'
	});

	formatShortDate = function (event, object){
		var val = $(object).val();
		var newValue = val;
		if (val.length == 6 ){
			newValue = val.substr(0,2) + "/" + val.substr(2,2)+ "/" + "20" + val.substr(4,2);
			$(object).val(newValue);
		}
		if (validarFecha(object)) {
			$(object).val(newValue);
		}else {
			$(object).addClass("error-field");
		}

	}

	$('.shortDateFormatter').on("keydown", function(e){
		$(this).removeClass("error-field");
		if (isNaN(e.key) && e.keyCode != 13 && e.keyCode != 55 && e.keyCode != 9 && e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 39 && e.keyCode != 111){
			e.preventDefault();
		}
    if(e.keyCode == 13){
			formatShortDate(e, $(this))
		}
	}).on("blur",function(e){
		formatShortDate(e,$(this));
	});

	/* links del navegador */
	$("a.openView").bind("click", function(e) {
		abrirVista(jsDbURL, this.id);
		e.preventDefault();
	});

	$("a.openViewCat").bind("click", function(e) {
	abrirVistaRestrictToCategory(jsDbURL, this.id, $(this).attr("data-category"));
	e.preventDefault();
});

	/* Para abrir la base que se haya clickado en el navegador*/
	$("a.openDB").bind("click", function(e) {
		var isBaseActual = $(this).hasClass("esBaseActual");
		if (isBaseActual)	return (false);
		var arrTemp = this.id.split("_");
		location.href = eval(arrTemp[1]);
		e.preventDefault();
	});

	$("a.openBusquedaNav").unbind("click").bind("click", function(e) {
	var tipo = this.id.split("_")[2]; // el id es el alias de la vista_Buscaipo_# de tipo;
		searchWebNav (jsForm, tipo); // <- Esta función debe declararse en jquery.FuncionesBD
		e.preventDefault();
	});

	$("a.openDocConfig").unbind("click").bind("click", function(e) {
		abrirDocConfiguracion(jsDbURL, "(Configuracion)", "Configuracion");
		e.preventDefault();
	});

	$("a.openDocConfigNotifi").unbind("click").bind("click", function(e) {
		abrirDocConfiguracion(jsDbURL, "(Destinatarios Aviso ABMAlta)", "NotificacionAltaUsuario");
		e.preventDefault();
	});

	$("a.openDocFechaLimiteAuto").unbind("click").bind("click", function(e) {
		abrirDocConfiguracion(jsDbURL, "(FechaLimiteAuto)", "FechaLimiteAuto");
		e.preventDefault();
	});

	/* Acciones "Crear..."  desde menú*/
	$(".createDocument").unbind("click").bind("click", function(e) {
		e.preventDefault();
		setSessionData("urlOrigen",window.location.href);
		var arrTemp = $(this).data("form").split("_");
		var nomForm = arrTemp[1];
		var id = this.id;
		var params="&" + $(this).data("params");
		if (params == "&"){
			params=undefined;
		}

		var isNewWindow = $(this).hasClass("newWindow");

		var newWindowBig = $(this).data("new-window-big");
		if (newWindowBig == undefined || newWindowBig == null) {
			newWindowBig = 0;
		}

		crearDocumento(jsDbURL, nomForm, isNewWindow, params, newWindowBig );
	});

	/* Acciones "Crear..."  desde menú - Pero para hacer un OpenForm en lugar de un OpenDocument o CreateDocument */
		$("a.openForm, button.openForm").unbind("click").bind("click", function(e) {
			e.preventDefault();

			var arrTemp = $(this).data("form").split("_");
			var nomForm = arrTemp[1];
			var id = this.id;
			var isNewWindow = $(this).hasClass("newWindow");
			var params="&" + $(this).data("params");
			if (params == "&"){
				params=undefined;
			}

			var newWindowBig = $(this).data("new-window-big");
			if (newWindowBig == undefined || newWindowBig == null) {
				newWindowBig = 0;
			}
			openForm(jsDbURL, nomForm, isNewWindow, params, newWindowBig );
		});

	/* Hacer un readForm */
	$(".readForm").unbind("click").bind("click", function(e) {
		e.preventDefault();

		var arrTemp = $(this).data("form").split("_");
		var nomForm = arrTemp[1];
		var id = this.id;
		var isNewWindow = $(this).hasClass("newWindow");

		readForm(jsDbURL, nomForm, isNewWindow, undefined );
	});

	/* clicks de las secciones del navegador: imágenes "+" y "-" */
	$(".navTituloSeccionClick").unbind("click").bind("click", function(e) {
		$("#imgExpand_"+this.id).toggleClass("hiddenElement");
		$("#imgCollapse_"+this.id).toggleClass("hiddenElement");
		$("#"+this.id+"_1").toggleClass("hiddenElement");
		e.preventDefault();
	});

	$(".navTituloNivel1Click").unbind("click").bind("click", function(e) {
		$("#imgExpand_"+this.id).toggleClass("hiddenElement");
		$("#imgCollapse_"+this.id).toggleClass("hiddenElement");
		$("#"+this.id+"_2").toggleClass("hiddenElement");
		e.preventDefault();
	});

	/* click del título */
	$("#formTitulo").css("cursor", "pointer").unbind("click").bind("click", function(e) {
		$(".navNivel1").addClass("hiddenElement");
		$(".navNivel2").addClass("hiddenElement");
		e.preventDefault();
	});

	/* click para abrir un documento cuando ésta va a ser único, no hace falta mostrar una vista. */
	$("a.openDocUnico").bind("click", function(e) {
		var arrTemp = this.id.split("_");
		var formName = arrTemp[1];
		arrTemp = this.name.split("_");
		var viewName = arrTemp[1];

		abrirDocUnico(jsDbURL, viewName, formName);
		e.preventDefault();
	});

	$("a.lanzarAgXForm").bind("click", function(e) {
		var nomAg = this.name;
		loadForm(jsDbURL, "w" + nomAg,"Importacion contactos", "divRetornoAgente");
		e.preventDefault();
	});

	$(".lanzarAg").bind("click", function(e) {
		var nomAg = this.name;
		ejecutarAgente(nomAg, jsUsuarioActualCN); //YAM 12/01/2022 QUiTAR COMENTARIO Y BORRAR EL ALERT
		e.preventDefault();
	});

	/* click de los botones "AddressBox" */
	$(".icoButton[id^=addressbox1_]").unbind("click").bind("click", function(e) {

	var idCampoDestino = rightString(this.id, "_");
		loadAddressBox(jsDbURL, idCampoDestino, true, translate("jsLit_DirectorioPersonasGrupos"), "divPopUpAddBox");
		e.preventDefault();
	});

	$(".icoButton[id^=addressbox_]").unbind("click").bind("click", function(e) {
		var idCampoDestino = rightString(this.id, "_");
		loadAddressBox(jsDbURL, idCampoDestino, false, translate("jsLit_DirectorioPersonasGrupos"), "divPopUpAddBox");
		e.preventDefault();
	});

	$(".icoButton[id^=addressboxParams_]").unbind("click").bind("click", function(e) {
		var vParams = this.id.split("_");
		var idCampoDestino = vParams[1];
		var params = "vista=" + vParams[2] + "&columna=" + vParams[3];
		loadAddressBox(jsDbURL, idCampoDestino, false, translate("jsLit_DirectorioPersonasGrupos"), "divPopUpAddBox", params);
		e.preventDefault();
	});

	/* formateo de los campos fecha */
	$(".fecha").restrictToDate().bind("blur", function(e) {
		validarFecha(this);
	});

	/* fullTrim de los campos */
	$(".campo").unbind("blur").bind("blur", function(e) {
		$(this).val(replaceTagSimulator(replaceComillas(fullTrim($(this).fieldValue()[0]))));
		e.preventDefault();
	});

    /* campos numéricos */
    $(".numberOnly").restrictToIntegers();

    /* campos decimales */
    $(".decimalNumber").restrictToDecimals(2);

	/* campos precio */
	$(".numberXdecimal").restrictToDecimals(5);

    /* separador numérico de los miles */
    $(".thousandSeparator").each(function(e) {
      $(this).val(addThousandSeparator($(this).fieldValue()));
    });
    $(".thousandSeparator").unbind("focus").bind("focus", function(e) {
      $(this).val(removeThousandSeparator($(this).fieldValue()));
    });
    $(".thousandSeparator").unbind("blur").bind("blur", function(e) {
      $(this).val(addThousandSeparator($(this).fieldValue()));
    });

    /* SCRIPT PARA TEMA DE SELECCIONADOS */
    /* Select's que no se pueden seleccionar */
    $(".unselect").bind("change", function(e){
      if ($(this).is("select"))
      $(this).attr("selectedIndex", -1);
      e.preventDefault();
    });


    /*Acciones sobre documentos seleccionados que se ejecutarán por cada doc seleccionado. El id de la acción deberá contener el nombre del agente a ejecutar. Se ha de quitar de los formularios que lo contengan!!!!*/
		$(".acSobreSeleccionados").bind("click", function(e) {
			e.preventDefault();

			var listaIdsSelec = "";
			var numSelec = 0;

			var _thisBtn$ = $(this);
			var idAct = _thisBtn$.attr("id");
			var sNomAgenteProc = idAct.split("_")[1]; //"eliminarFamilias", "elimiarAplicaciones"
			var callback = $(this).attr("data-callback");
			

			$('input[type="checkbox"][name="cb_view"]').each(function() {
				var _this$ = $(this);

				if (_this$.attr("name").toUpperCase() == "CB_VIEWTITLE"){
					_this$.checked=false;
				}

				if (_this$.is(':checked')){
					numSelec += 1;
					var idDoc = _this$.val();
					listaIdsSelec = listaIdsSelec + idDoc + ";";
					procesarDocsSeleccionados(jsDbURL, idDoc, sNomAgenteProc, callback);
				};
			});

			// Si no se ha seleccionado nada, se muestra un aviso!
			if(listaIdsSelec==""){
				showNotification(translate("jsLitAvisoNingunDocSelec"), "Error");
				return false;
			};
			if (callback){
				window[callback]();
			}
			else {
				window.location.reload();
			}

		});
    /* fin de script para te,a de docs seleccionados */

    /* zona pop-up's "alert" */
    try
    {
		/* Lo ponemos dentro de un "try-catch" pq si la página actual no incorpora el "ui-dialog.js", esta inicialización daría ERROR*/
		$("#divAlert").dialog({
			modal: true,
			resizable:true,
			autoOpen:false,
			overlay:{background:"#cccccc", opacity:0.5}
		});

		$("#divPopUpEstadisticas").dialog({
			modal: true,
			resizable:true,
			width: 400,
			autoOpen:false,
			overlay:{background:"#cccccc", opacity:0.5}
		});

		$.global_optionsDialogPopUp = {
			bgiframe: true,
			modal: true,
			closeOnEscape: false,
			autoOpen:false,
			width: 'auto',
			close: function(event, ui) {
			  $(this).dialog("destroy");
			},
			drag: function(event, ui) {
			  if ($.browser.msie)
			  {
				var ieWidthResizeBug = 15;
				var ieHeightResizeBug = 5;
				var newWidth = $(".ui-dialog-titlebar", $(this).parent()).width()+ieWidthResizeBug;
				var newHeight = $(this).parent().height() - $(".ui-dialog-titlebar", $(this).parent()).height() - ieHeightResizeBug;

				$(this).width(newWidth+"px");
				$(this).height(newHeight+"px");
			  }
			},
			resize: function(event, ui) {
			  if ($.browser.msie)
			  {
				var ieWidthResizeBug = 15;
				var ieHeightResizeBug = 15;
				var newWidth = $(".ui-dialog-titlebar", $(this).parent()).width()+ieWidthResizeBug;
				var newHeight = $(this).parent().height() - $(".ui-dialog-titlebar", $(this).parent()).height() - ieHeightResizeBug;

				$(this).width(newWidth+"px");
				$(this).height(newHeight+"px");
			  }
			}
		};

		/* clear out plugin default styling */
    } catch(err) {
    };

    //Campos con selects asociados
	$('.campo-select').each(function(){
		campoId=$(this).attr("id");
		$('#sel-'+campoId).val($(this).val());

		$('#sel-'+campoId).change(function(){
			id=$(this).attr("id");
			id=id.split("sel-");
			id=id[1];
			$('#'+id).val($(this).val());
		});

		options=$('#sel-'+campoId).children("option");
		if(options.length<=1){
			$('#sel-'+campoId).attr("disabled","disabled");
		}
		if(options.length==1){
			$('#sel-'+campoId).val($(options[0]).val())
			$('#'+campoId).val($(options[0]).val());
		}
	});

    /**
     *
     * F U N C I O N E S   C O M U N E S
     *
     *
     */

		//<<<<<<


	// Configuración de campos "autocomplete"
	$.each($('[data-autocomplete]'),function(index, obj){
		var fieldID = $(obj).attr("id");
		var view = $(obj).attr("data-autocomplete");

		var db = $(obj).attr("data-autocomplete-db");
		if (db == undefined) {
			db = $('#pathBDWeb').val();
		}

		var server = $(obj).attr("data-autocomplete-server");
		if (server == undefined) {
			server = jsServerName;
		}
		loadAllViewData(server, db, view, "", setAutocompleteSource, true, fieldID, true);
	});

	function setAutocompleteSource(data, fieldID) {
		if (data && data.length > 0){
			source = [];

			for (var i=0;i<data.length;i++){
				value=data[i]["0"];
				source.push(value);
			}

			$("#"+fieldID).autocomplete({
				source: source,
				change: function (event, ui) {
					if (!ui.item) {
						this.value = '';
					}
				}
			});
		}
	}


	/**
	*
	*Configuración de los campos Selectize
	*
	**/

  // Procesa el objeto html y extrae los parámetros para generar un campo selectize
  processSelectizeField = function (selectizeField){
    var field= $(selectizeField);
    var params = {};

    params.targetField=field.attr('id');
    params.value=field.val();
    params.multiple=field.attr("data-selectize-multiple")!=undefined;
    params.disabled=field.attr("data-selectize-disabled")!=undefined;
    params.placeHolder=field.attr("data-selectize-placeholder") || "";
    params.db=field.attr("data-selectize-db")==undefined ? "/names.nsf" : field.attr("data-selectize-db");
    params.view=field.attr("data-selectize-view")==undefined ? $('#defaultViewAddressBox').val() : field.attr("data-selectize-view");
    params.column=field.attr("data-selectize-column")==undefined ? $('#defaultColumnAddressBox').val() : field.attr("data-selectize-column");

	params.valueColumn=field.attr("data-selectize-column-value")==undefined ? params.column :  field.attr("data-selectize-column-value");
	params.allowCreation=field.attr("data-selectize-no-create")==undefined;
	params.translate=field.attr("data-selectize-translate")!=undefined;
	params.translatable=field.attr("data-selectize-translatable")!=undefined;
	params.translatablePrefix = field.attr("data-selectize-translatable-prefix") == undefined ? "CLI" : field.attr("data-selectize-translatable-prefix");
	params.excluded=field.attr("data-selectize-excluded");
	params.delimiter = field.attr("data-selectize-delimiter") == undefined ? ",": field.attr("data-selectize-delimiter");

	var category = field.attr("data-selectize-viewcategory")==undefined ? "" : field.attr("data-selectize-viewcategory");
	var showLoader = field.attr("data-selectize-loader") == undefined ? true : field.attr("data-selectize-loader") == 1;

	if (category == "") {
		setTimeout(function(){loadAllViewContentNoLimit(params.db, params.view, createSelectizeField, showLoader, params,false,false);},200);
	} else {
		setTimeout(function(){loadViewContentByCategory(params.db, params.view, category, createSelectizeField, showLoader, params);},200);
	}
  }

	// Generación del objeto selectize a partir de los datos extraídos de la vista y los parámetros del objeto
	function createSelectizeField(data, params) {
		var isNames = params.db == "/names.nsf";

		var viewentrylength = 0;
		var selectizeId = '#sz-'+params.targetField;

		$('#sz-'+params.targetField).next(".selectize-control").remove();
		$('#sz-'+params.targetField).remove();
		var options="";
		var select = '<select id="sz-#SELECTIZE_ID#" #DISABLED# class="text item-info" #MULTIPLE# >#OPTIONS#</select>';
		
		if (data.viewentry) {
			
			viewentrylength = data.viewentry.length;
			var excluded=[];
			if (params.excluded) {
				excluded=params.excluded.split(",");
			}

			var values;
			if (params.value) {
				values = params.value.split(params.delimiter).map(item => item.trim());
			} else {
				values=[];
			}

			if (!params.multiple) {
				options+='<option value=""></option>';
			}
			
			for (var j=0; j<values.length;j++) {
					var mustInclude=true;
					for (var i=0;i<data.viewentry.length;i++){
						var optValue= data.viewentry[i].entrydata[params.valueColumn-1].text == undefined ? data.viewentry[i].entrydata[params.valueColumn-1].textlist.text[0][0]: data.viewentry[i].entrydata[params.valueColumn-1].text[0];

						// Cambio de la coma
						if (isNames && optValue.indexOf(",")!=-1){
							var temp = optValue.split(",").map(item => item.trim());
							optValue=temp[1] + " " + temp [0];
						}
						// Fin Cambio de la coma

						if (values[j]==optValue) {
							mustInclude=false;
						}
					}
					if (mustInclude) {
						var nonExistentObj=JSON.stringify({"nonexistent":true});
						options+="<option data-data='"+nonExistentObj+"' selected value='"+values[j]+"'>"+values[j]+"</option>";
					}
			}

			for (var i=0;i<data.viewentry.length;i++){
				var optDesc= data.viewentry[i].entrydata[params.column-1].text == undefined ? data.viewentry[i].entrydata[params.column-1].textlist.text[0][0]: data.viewentry[i].entrydata[params.column-1].text[0];
				var optValue= data.viewentry[i].entrydata[params.valueColumn-1].text == undefined ? data.viewentry[i].entrydata[params.valueColumn-1].textlist.text[0][0]: data.viewentry[i].entrydata[params.valueColumn-1].text[0];

				// Cambio de la coma
				if (isNames && optValue.indexOf(",")!=-1) {
					var temp = optValue.split(",").map(item => item.trim());
					optValue=temp[1] + " " + temp [0];
				}
				if (isNames && optDesc.indexOf(",")!=-1) {
						var temp = optDesc.split(",").map(item => item.trim());
						optDesc=temp[1] + " " + temp [0];
				}
				// Fin cambio de la coma

				if (!excluded.includes(optValue)) {
					var selected = values.indexOf(optValue)!=-1;
					if (params.translatable){
						options+='<option value="'+optValue+'" '+ (selected?"selected":"") +' data-translate="' + params.translatablePrefix + '_'+optDesc+'"></option>';
					} else if (params.translate){
						options+='<option value="'+optValue+'" '+ (selected?"selected":"") +' data-translate="'+optDesc+'"></option>';
					} else {
						options+="<option  value='"+optValue+"' "+ (selected?"selected":"") +">"+optDesc+"</option>";
					}
				}
			}
		} else {
			options+='<option value=""></option>';
		}

		select=select.replace(/#SELECTIZE_ID#/g,params.targetField);
		select=select.replace(/#DISABLED#/g, params.disabled ? "disabled" : "");
		select=select.replace(/#MULTIPLE#/g, params.multiple ? "multiple" : "");
		select=select.replace(/#OPTIONS#/g,options);

		$(select).insertAfter('#'+params.targetField);
		translateHTMLBlock($('#'+params.targetField).parent());

		$('#sz-'+params.targetField).selectize({
			delimiter: params.delimiter,
			persist: false,
			onChange: function(value){
				var tempVal;
				if (value!=null){
					if (Array.isArray(value)) {
						tempVal=value.join(params.delimiter);
					} else {
						tempVal=value;
					}
				} else {
					tempVal="";
				}
				$('#'+params.targetField).val(tempVal);
				$('#'+params.targetField).change();
			},
			render: {
        		item: function(data) {
					if (data.nonexistent){
						return "<div class='disabled-option'>"+data.text+"</div>";
					} else {
						return "<div>"+data.text+"</div>";
					}
    			}
			},
			plugins: params.disabled?[]:['remove_button'],
			placeholder:params.placeHolder != ""? translate(params.placeHolder) : "",
			create: params.allowCreation?(function(input) {
				return {
					value: input,
					text: input
				}
			}):false
		});


		// Launch event finalize selectize values
		$(document).trigger("selectizeFieldEvent", ['#'+params.targetField, selectizeId, viewentrylength]);
	}



	// Procesa el objeto html y extrae los parámetros para generar un campo selectize
	processSelectizeTableField = function (selectizeTableField){
		var field= $(selectizeTableField);
		var params = {};

		params.targetField=field.attr('id');
		params.value=field.val();
		params.multiple=field.attr("data-selectize-multiple")!=undefined;
		params.disabled=field.attr("data-selectize-disabled")!=undefined;
		params.placeHolder=field.attr("data-selectize-placeholder") || "";
		params.db=field.attr("data-selectize-db")==undefined ? "/names.nsf" : field.attr("data-selectize-db");
		params.view=field.attr("data-selectize-view")==undefined ? $('#defaultViewAddressBox').val() : field.attr("data-selectize-view");
		params.column=field.attr("data-selectize-column")==undefined ? $('#defaultColumnAddressBox').val() : field.attr("data-selectize-column");
		params.valueColumn=field.attr("data-selectize-column-value")==undefined ? params.column :  field.attr("data-selectize-column-value");
		params.allowCreation=field.attr("data-selectize-no-create")==undefined;
		params.translate=field.attr("data-selectize-translate")!=undefined;
		params.translatable=field.attr("data-selectize-translatable")!=undefined;
		params.excluded=field.attr("data-selectize-excluded");

		var category=field.attr("data-selectize-viewcategory")==undefined ? "" : field.attr("data-selectize-viewcategory");
		var showLoader = field.attr("data-selectize-loader") == undefined ? true : field.attr("data-selectize-loader") == 1;

		if (category == ""){
			setTimeout(function(){loadAllViewContent(params.db, params.view, createSelectizeTableField, showLoader, params);},200);
		} else {
				setTimeout(function(){loadViewContentByCategory(params.db, params.view, category, createSelectizeTableField, showLoader, params);},200);
		}
	}



	// Generación del objeto selectize a partir de los datos extraídos de la vista y los parámetros del objeto
	function createSelectizeTableField(data, params){
		var viewentrylength = 0;
		var selectizeId = '#sz-'+params.targetField;

		//$('#sz-'+params.targetField).next(".selectize-control").remove();
		$('#sz-'+params.targetField).remove();
		var options="";
		var table = '<table id="sz-#SELECTIZE_ID#" #DISABLED# class="table table-striped table-sm table-bordered sz-table">#OPTIONS#</table>';

		if (data.viewentry) {
			viewentrylength = data.viewentry.length;

			var excluded=[];
			if (params.excluded){
				excluded=params.excluded.split(",");
			}

			var values;
			if (params.value){
				values = params.value.split(",").map(item => item.trim());
			} else {
				values=[];
			}

			var odd=true;
			for (var i=0;i<data.viewentry.length;i++){
				var optDesc= data.viewentry[i].entrydata[params.column-1].text == undefined ? data.viewentry[i].entrydata[params.column-1].textlist.text[0][0]: data.viewentry[i].entrydata[params.column-1].text[0];
				var optValue= data.viewentry[i].entrydata[params.valueColumn-1].text == undefined ? data.viewentry[i].entrydata[params.valueColumn-1].textlist.text[0][0]: data.viewentry[i].entrydata[params.valueColumn-1].text[0];
				if (optValue.indexOf(",")!=-1){
					var temp = optValue.split(",").map(item => item.trim());
					optValue=temp[1] + " " + temp [0];
				}

				if (!excluded.includes(optValue)) {
					var selected = values.indexOf(optValue)!=-1;
					if (params.translatable){
						options+='<tr class="'+(odd?"odd":"even")+'"><td class="col-md-11" data-translate="CLI_'+optDesc+'"></td><td class="szt-option-cell col-md-1"><input data-field="'+params.targetField+'" class="szt-option" type="checkbox" value="'+optValue+'" '+ (selected?"checked":"") +' '+params.disabled ? "disabled" : ""+'/></td></tr>';
					} else if (params.translate){
						options+='<tr class="'+(odd?"odd":"even")+'"><td class="col-md-11" data-translate="'+optDesc+'"></td><td class="szt-option-cell col-md-1"><input data-field="'+params.targetField+'" class="szt-option" type="checkbox" value="'+optValue+'" '+ (selected?"checked":"") +' '+params.disabled ? "disabled" : ""+'/></td></tr>';
					}
					else {
						options+='<tr class="'+(odd?"odd":"even")+'"><td class="col-md-11">'+optDesc+'</td><td class="szt-option-cell col-md-1"><input data-field="'+params.targetField+'" class="szt-option" type="checkbox" value="'+optValue+'" '+ (selected?"checked":"") +' ' + (params.disabled ? "disabled" : "") + '/></td></tr>';
					}
				}
				odd=!odd;
			}
		}
		else {
			options+='<tr><td colspan="2">No se encontraron datos</td></tr>';

		}

		table=table.replace(/#SELECTIZE_ID#/g,params.targetField);
		table=table.replace(/#OPTIONS#/g,options);

		$(table).insertAfter('#'+params.targetField);
		translateHTMLBlock($('#'+params.targetField).parent());

		$('.szt-option').unbind("click").click(function(){
			var valClicked=$(this).val();
			var addValue=$(this).prop("checked");
			var values=[];
			if ($("#"+$(this).attr("data-field")).val() != ""){
				values=$("#"+$(this).attr("data-field")).val().split(",");
			}

			if (addValue){
					if (!values.includes(valClicked)){
						values.push(valClicked);
					}
			} else {
				if (values.includes(valClicked)){
					var position=values.indexOf(valClicked);
					values.splice(position,1);
				}
			}

			$("#"+$(this).attr("data-field")).val(values.toString());
			$("#"+$(this).attr("data-field")).change();
		});
		$(document).trigger("selectizeTableFieldEvent", ['#'+params.targetField, selectizeId, viewentrylength]);
	}




	/* Control para insertar vistas embebidas */
	applyCellEvents = function(){
		$('.embededViewCell').off("click").on("click", function(){
			var docUNID = ($(this).attr("data-unid"));
			var dbPath= $(this).attr("data-db-path");

			var ahref=dbPath+"/x/"+docUNID+"?OpenDocument";
			var openInNewWindow = $(this).attr("data-open-new-window") != undefined;
			var newWindowBig = $(this).attr("data-new-window-big") != undefined;
			var openInEditMode = $(this).attr("data-open-edit-mode") != undefined;
			if (openInEditMode) {
				ahref = dbPath+"/x/"+docUNID+"?EditDocument";
			}

			if (openInNewWindow) {
				openURLNewWindow (ahref, 600,newWindowBig);
			} else {
				location.href = ahref;
			}
		});
	}


	appendRows = function (data, extraParam, callback) {
		var count = 0;
		if (data.viewentry) {
			count = data.viewentry.length;
			var docID= data.viewentry[0]["@unid"];
			var bloque = $('#'+extraParam.tableId + ' table');
			var dbPath=extraParam.db;

			var openInNewWindow=extraParam.newWindow;
			var openInEditMode = extraParam.editMode;
			var newWindowBig = extraParam.newWindowBig;
	
			if (bloque.find("th").length==0){
				var header="<thead><tr>";
				for (var i=0; i< data.viewentry[0].entrydata.length;i++){
					header+='<th>'+translate(data.viewentry[0].entrydata[i]["@name"])+'</th>';
				}
				header+="</thead>";
				bloque.append(header);
			}

			for(var i=0; i< data.viewentry.length;i++){
				var row='<tr>';
				for (var j=0; j< data.viewentry[i].entrydata.length;j++){
					var cellValue;
					var documentUNID = data.viewentry[i]["@unid"];

					if (data.viewentry[i].entrydata[j].text!=undefined){
					  cellValue = data.viewentry[i].entrydata[j].text[0];
					} else if (data.viewentry[i].entrydata[j].number!=undefined){
					  cellValue = data.viewentry[i].entrydata[j].number[0];
					} else if (data.viewentry[i].entrydata[j].datetime!=undefined){
						var tempDate=data.viewentry[i].entrydata[j].datetime[0];
					  cellValue = tempDate.substr(6,2)+"/"+tempDate.substr(4,2)+"/"+tempDate.substr(0,4);
					} else if (data.viewentry[i].entrydata[j].textlist != undefined
						&& data.viewentry[i].entrydata[j].textlist.text != undefined
						&& data.viewentry[i].entrydata[j].textlist.text[0] != undefined){
						cellValue = data.viewentry[i].entrydata[j].textlist.text[0]["0"];
					} else {
						cellValue="";
					}

					if (extraParam.stdEvent){
						row+='<td class="embededViewCell" data-unid="'+documentUNID+'" data-db-path="'+dbPath+'" '+(newWindowBig?"data-new-window-big=\"true\"":"")+' '+(openInNewWindow?"data-open-new-window=\"true\"":"")+ ' '+(openInEditMode?"data-open-edit-mode=\"true\"":"")+'>'+cellValue+'</td>';
					} else {
						row+='<td class="embededViewCellCustom" data-unid="'+documentUNID+'" data-db-path="'+dbPath+'" '+(newWindowBig?"data-new-window-big=\"true\"":"")+' '+(openInNewWindow?"data-open-new-window=\"true\"":"")+' '+(openInEditMode?"data-open-edit-mode=\"true\"":"")+'>'+cellValue+'</td>';
					}
				}
				row+='</tr>';
				bloque.append(row);
			}
			applyCellEvents();

			if (!extraParam.stdEvent && extraParam.customEvent){
					window[extraParam.customEvent]();
			}
			translateHTMLBlock(bloque);

		} else { // No hay resultados
			var bloque = $('#'+extraParam.tableId + ' table');
			bloque.html("<tr><td>" + translate("NoResultsInQuery") + " </td></tr>");
		}
		$(document).trigger("embededViewTotalElementsEvent", ['#'+ extraParam.tableId, count]);
	}

  var embededViewTable = '<table border="0" cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-hover table-sm"></table>';

  refreshEmbededViews = function () {
    var list = $('.embededView');
    $.each(list, function(index, obj){
		var view = $(obj).attr("data-view");
		if (view != undefined){
			$(obj).html(embededViewTable);
			var tableId=$(obj).attr("id");
			var stdEvent=$(obj).attr("data-std-event")=="false"?false:true;
			var newWindow=$(obj).attr("data-open-new-window")=="false"?false:true;
			var newWindowBig=$(obj).attr("data-new-window-big")=="true"?true:false;
			var editMode=$(obj).attr("data-open-edit-mode")=="true"?true:false;
			  var db=$(obj).attr("data-db-path");
			var callback=$(obj).attr("data-custom-event");
			
			if (db==undefined) {
				db=$('#pathBDWeb').val();
			}
	
			var extraParams = {"db" : db, "tableId" : tableId, "stdEvent" : stdEvent, "newWindow" : newWindow, "customEvent" : callback, "editMode" : editMode, "newWindowBig": newWindowBig};
			
			var category=$(obj).attr("data-category");
			if (category != undefined){
				loadViewContentByCategory(db, view, category, appendRows, false, extraParams, callback);
			} else {
				loadAllViewContent(db,view,appendRows,false,extraParams);
			}
		}
    	
    });
  }

	$('#button-next-document').unbind("click").click(function(){
		var posicion = getDocumentPosition();
		goToDocumentPosition(posicion+1)
	});

	$('#button-prev-document').unbind("click").click(function(){
		var posicion = getDocumentPosition();
		goToDocumentPosition(posicion-1)
	})

	updateDocumentNavigator();

});


document.addEventListener("LanguageReady", function(){
	refreshEmbededViews();
	procesarErroresDeRetornoDeAgente();

	// Crear automáticamente los campos Selectize para todos aquellos elementos con la clase ".selectize"
	$.each($('.selectize'), function(index, obj){
		processSelectizeField(obj);
	});

	// Crear automáticamente los campos Selectize para todos aquellos elementos con la clase ".selectize"
	$.each($('.selectize-table'), function(index, obj){
		processSelectizeTableField(obj);
	});
});

document.addEventListener("TranslationsReady", function(){
	// Crear automáticamente los campos Selectize para todos aquellos elementos con la clase ".selectize-simple"
	// Cuando ya se hayan hecho todas las traducciones para que se coja el texto de option
	$.each($('.selectize-simple'), function(index, obj) {
		$(obj).show();
		$(obj).removeClass("hiddenElement");
		var isDisabled = $(obj).is(':disabled');
		$(obj).selectize({
			create: false,
			plugins: isDisabled ? [] : ['remove_button']
		});
	});
});
