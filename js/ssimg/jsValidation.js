var regExp_NIF = "^[0-9]{8}[A-Z]{1}$";
var regExp_ExtensionLogo = "\.gif$";
/*
  -------------------------------------------------------------------------
	                    JavaScript Form Validator
                                Version 2.0.2
	Copyright 2003 JavaScript-coder.com. All rights reserved.
	You use this script in your Web pages, provided these opening credit lines are kept intact.
	The Form validation script is distributed free from JavaScript-Coder.com

	You may please add a link to JavaScript-Coder.com, making it easy for others to find this script.
	Checkout the Give a link and Get a link page:http://www.javascript-coder.com/links/how-to-link.php

	You may not reprint or redistribute this code without permission from JavaScript-Coder.com.

	JavaScript Coder
	It precisely codes what you imagine!
	Grab your copy here: http://www.javascript-coder.com/
    -------------------------------------------------------------------------
*/
Validator = function(frmname)
{
	this.formobj = document.forms[frmname];
	this.validationErrorMessages = new Array(0);

	if (!this.formobj)
	{
		this.validationErrorMessages[this.validationErrorMessages.length] = translate("jsLit_msgObjetoInaccesible") + " : "+frmname;
		return;
	}

	this.validateForm = validate_form;
	this.addValidation = add_validation;
	this.removeValidation = remove_validation;
	this.setAddnlValidationFunction = set_addnl_vfunction;
	this.clearAllValidations = clear_all_validations;
	this.fieldsTogether = false;
}

function validate_form(fieldsTogether)
{
	this.fieldsTogether = fieldsTogether;
	for(var itr=0; itr < this.formobj.elements.length; itr++)
	{
		if (this.formobj.elements[itr].validationset && !this.formobj.elements[itr].validationset.validate(fieldsTogether))
		{
			if (!fieldsTogether)
				return false;
		}
	}

	if (this.validationErrorMessages.length > 0)
	{
		return false;
	}
	if (this.formobj.addnlvalidation)
	{
		str =" var ret = "+this.formobj.addnlvalidation+"()";
		eval(str);
		if (!ret) return ret;
	}
	return true;
}

function set_addnl_vfunction(functionname)
{
	this.formobj.addnlvalidation = functionname;
}

function clear_all_validations()
{
	for (var itr=0; itr < this.formobj.elements.length; itr++)
	{
		this.formobj.elements[itr].validationset = null;
		$(this.formobj.elements[itr]).removeClass("error-field");
	}
	$('.error-field-msg').addClass("hiddenElement");
}

function form_submit_handler()
{
	for(var itr=0; itr < this.elements.length; itr++)
	{
		if (this.elements[itr].validationset && !this.elements[itr].validationset.validate(this.fieldsTogether))
		{
			return false;
		}
	}

	if (this.addnlvalidation)
	{
		str =" var ret = "+this.addnlvalidation+"()";
		eval(str);
		if (!ret) return ret;
	}
	return true;
}

function remove_validation(itemname)
{
	if (!this.formobj)
	{
		this.validationErrorMessages[this.validationErrorMessages.length] = translate("jsLit_msgFormNoInicializado");
		return;
	}

	if (this.formobj.elements[itemname] != undefined)
		this.formobj.elements[itemname].validationset = null;
}

function add_validation(itemname, descriptor, errstr)
{

	if (!this.formobj)
	{
		this.validationErrorMessages[this.validationErrorMessages.length] = translate("jsLit_msgFormNoInicializado");
		return;
	}

	var itemobj = this.formobj[itemname];
	if (!itemobj)
	{
		this.validationErrorMessages[this.validationErrorMessages.length] = translate("jsLit_msgCampoInaccesible") +' "'+itemname + '".';
		return;
	}

	if (!itemobj.validationset)
	{
		itemobj.validationset = new ValidationSet(itemobj);
	}

	if (errstr == undefined || errstr == null) {
		errstr = translate(itemname, true);
	}

	itemobj.validationset.add(descriptor, errstr);
}

function ValidationDesc(inputitem, desc, error)
{
	this.desc = desc;
	this.error = error;
	this.itemobj = inputitem;
	this.validate = vdesc_validate;
}

function vdesc_validate(fieldsTogether)
{
	if (!V2validateData(this.desc, this.itemobj, this.error, fieldsTogether))
 	{
		this.itemobj.focus();
		return false;
	}

	return true;
}

function ValidationSet(inputitem)
{
	this.vSet = new Array();
	this.add = add_validationdesc;
	this.validate = vset_validate;
	this.itemobj = inputitem;
}

function add_validationdesc(desc, error)
{
  this.vSet[this.vSet.length] = new ValidationDesc(this.itemobj, desc, error);
}

function vset_validate(fieldsTogether)
{
	for(var itr=0;itr<this.vSet.length;itr++)
	{
		if (!this.vSet[itr].validate(fieldsTogether))
		{
			return false;
		}
	}

	return true;
}

function validateEmailv2(email)
{
	// a very simple email validation checking.
	// you can add more complex email checking if it helps
	if(email.length <= 0)
	{
		return true;
	}

	var splitted = email.match("^(.+)@(.+)$");
	if (splitted == null) return false;
	if (splitted[1] != null )
	{
		var regexp_user = /^\"?[\w-_\.]*\"?$/;
		if (splitted[1].match(regexp_user) == null) return false;
	}

	if (splitted[2] != null)
	{
		var regexp_domain = /^[\w-\.]*\.[A-Za-z]{2,4}$/;
		if (splitted[2].match(regexp_domain) == null)
		{
			var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
			if (splitted[2].match(regexp_ip) == null) return false;
		}
		return true;
	}
	return false;
}

function validarFecha(campo){
	var f = $(campo).val();
	if ( f == "") {
		return false;
	} else {
		var fechaValida = false;
		var fechaf = f.split("/");

		if (fechaf.length != 3) {
			return false;
		}

		var day = fechaf[0];
		var month = fechaf[1];
		var year = fechaf[2];

		if (year.length < 4){
			return false;
		} else {
			var date = new Date(year,month,'0');
			if((day-0) > (date.getDate()-0)){
				fechaValida = false;
			} else {
				fechaValida = true;
			}
			return fechaValida;
		}
	}
}

function displayError(objValue, strError){
	$('#'+objValue.id).addClass("error-field");
	$('#sel-'+objValue.id).addClass("error-field"); //Campos select asociados a campos notes
	$('#sel-'+objValue.id).next(".selectize-simple").addClass("error-field"); //Campos select asociados a campos notes
	$('#ck_'+objValue.id).addClass("error-field"); //Campos CK-Editor
	if ($('#'+objValue.id).hasClass("selectize")){//campos selectize
			$('#sz-'+objValue.id).next(".selectize-control").addClass("error-field");
			$('#sz-'+objValue.id).change(function(){
				$('#sz-'+objValue.id).next(".selectize-control").removeClass("error-field");
				$('#msg_'+objValue.id).addClass("hiddenElement").html("");
			})
	}
	$('#msg_'+objValue.id).removeClass("hiddenElement");

	var translation=translate(strError);
	if (!translation.includes("TRANSLATION_ERROR")){
		strError=translate(strError);
	}
	$('#msg_'+objValue.id).html(strError);

	$('#'+objValue.id).change(function(){
		$(this).removeClass("error-field");
		$('#sel-'+$(this).attr("id")).removeClass("error-field");
		$('#msg_'+$(this).attr("id")).addClass("hiddenElement").html("");
	});

	$('div #sel-'+objValue.id).change(function(){
		$(this).removeClass("error-field");
		id = $(this).attr("id").replace("sel-","");
		$('#msg_'+id).html("").addClass("hiddenElement");
	});

}

function V2validateData(strValidateStr, objValue, strError, fieldsTogether)
{

	var errorMessages = jsFormValidator.validationErrorMessages;
	var epos = strValidateStr.search("=");
	var  command  = "";
	var  cmdvalue = "";
	var icoWarning = null;
	if (epos >= 0)
	{
		command  = strValidateStr.substring(0,epos);
		cmdvalue = strValidateStr.substr(epos+1);
	}
	else
	{
		command = strValidateStr;
	}

	switch (command.toLowerCase())
	{
		case "req":
		case "required":
         	{
						var error="";
			if (objValue.value.length == 0)
			{
				if (!strError || strError.length ==0)
              			{
                			error = objValue.title + ": " + jsLit_msgCampoObligatorio;
				}
				else
				{
					error = objValue.title + ": " + strError;
				}
				error=translate("jsLit_msgCampoObligatorio");
				displayError(objValue, error);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
           		}
           		break;
         	}//case required

		case "reqifnoempty":
		case "requiredifnoempty":
         	{
			var fieldNoEmpty = document.getElementById(cmdvalue);
			if (eval(fieldNoEmpty.value.length) > 0)
			{
				if (eval(objValue.value.length) == 0)
				{
					if (!strError || strError.length ==0)
	              			{
						strError = objValue.title + ": " + jsLit_msgCampoObligatorio;
					}
					else
					{
						strError = objValue.title + ": " + strError;
					}


					strError=translate("jsLit_msgCampoObligatorio");
					displayError(objValue, strError);

					errorMessages[errorMessages.length] = strError;
					if (!fieldsTogether)
					{
						return false;
					}
				}
           		}
           		break;
         	}//case requiredifnoempty

		case "reqifempty":
		case "requiredifempty":
         	{
			var fieldEmpty = document.getElementById(cmdvalue);
			if (eval(fieldEmpty.value.length) == 0)
			{
				if (eval(objValue.value.length) == 0)
				{
					if (!strError || strError.length ==0)
	              			{
						strError = objValue.title + ": " + jsLit_msgCampoObligatorio;
					}
					else
					{
						strError = objValue.title + ": " + strError;
					}
					strError=translate("jsLit_msgCampoObligatorio");
					displayError(objValue, strError);

					errorMessages[errorMessages.length] = strError;
					if (!fieldsTogether)
					{
        						return false;
					}
				}
           		}
           		break;
         	}//case requiredifempty

		case "email":
		{
			if (!validateEmailv2(objValue.value))
			{
				if (!strError || strError.length ==0)
				{
                			strError = objValue.title + ": " + jsLit_msgEmailNoValido;
				}
				else
				{
					strError = objValue.title + ": " + strError;
				}

				strError=translate("jsLit_msgEmailNoValido");
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case email

		case "radiobutton":
		{
			if ($(":radio[name="+objValue.name+"]:checked")[0]==undefined)
			{
				if (!strError || strError.length ==0)
              			{
                			strError = objValue.title + ": " + jsLit_msgCampoObligatorio;
				}
				else
				{
					strError = objValue.title + ": " + strError;
				}

				strError=translate("jsLit_msgCampoObligatorio");
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case radiobutton

		case "date":
		{
			if (!validarFecha(objValue))
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.title + ": " + jsLit_msgFechaIncorrecta;
				}
				else
				{
					strError = objValue.title + ": " + strError;
				}

				strError=translate("jsLit_msgFechaIncorrecta");
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}// case date

		case "num":
		case "numeric":
		{
			var valor = removeThousandSeparator(objValue.value);
			var charpos = valor.search("[^0-9]");
			if (objValue.value.length > 0 &&  charpos >= 0)
			{
				if (!strError || strError.length ==0)
				{
                			strError = objValue.title + ": " + jsLit_msgNumeroNoValido;
				}
				else
				{
					strError = objValue.title + ": " + strError;
				}

				strError=translate("jsLit_msgNumeroNoValido");
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case numeric

		case "dec":
		case "decimal":
		{
			var valor = objValue.value;
			valor = removeThousandSeparator(valor);
			if (objValue.value.length > 0)
			{
				if (isNaN(cmdvalue) || cmdvalue==0)
					cmdvalue = 2; // nº de decimales por defecto
				var num = parseFloat(valor.replace(/,/g, "."));
				num = num.toFixed(cmdvalue);
				if (isNaN(num))
				{
					if (!strError || strError.length ==0)
					{
	                			strError = objValue.title + ": " + jsLit_msgNumeroNoValido;
					}
					else
					{
						strError = objValue.title + ": " + strError;
					}

					strError=translate("jsLit_msgNumeroNoValido");
					displayError(objValue, strError);

					errorMessages[errorMessages.length] = strError;
					if (!fieldsTogether)
					{
						return false;
					}
				}
			}
			break;
		}//case decimal

		// function=nomFuncion~mensajeError
		case "function":
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				cmdvalue = arr[0];
				if (strError == null || strError == undefined) {
					strError = arr[1];
				}
			}
			var result = eval(cmdvalue);
			if (!result)
			{
				displayError(objValue, strError);
				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case function

		case "unique":
		{
			var result = validateUnique(objValue, cmdvalue);
			if (!result)
			{
				displayError(objValue, strError);
				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		} //case unique

		// lessthan=valor~separadorDecimales
		// se debe utilizar primero la validacion "num" o "decimal" pq aquí NO se muestra mensaje de error si es un nº incorrecto
		case "lt":
		case "lessthan":
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				var sepDecimal = arr[1];
				cmdvalue = removeThousandSeparator(arr[0]).replace(/,/g, '.')
			}

			if (isNaN(objValue.value))
			{
				return false;
			}

			if (eval(objValue.value) >=  eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + eval("jsLit_msgCampoMenor.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
											strError = eval("jsLit_msgCampoMenor.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
				}
				else
				{
					//strError = objValue.title + ": " + strError;
					strError =  strError;
				}

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */

				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case lessthan

		// lessequalthan=valor~separadorDecimales
		// se debe utilizar primero la validacion "num" o "decimal" pq aquí NO se muestra mensaje de error si es un nº incorrecto
		case "let":
		case "lessequalthan":
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				var sepDecimal = arr[1];
				cmdvalue = removeThousandSeparator(arr[0]).replace(/,/g, '.')
			}

			if (isNaN(objValue.value))
			{
				return false;
			}

			if (eval(objValue.value) > eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + eval("jsLit_msgCampoMenorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
											strError = eval("jsLit_msgCampoMenorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
				}
				else
				{
					//strError = objValue.title + ": " + strError;
					strError = strError;
				}

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case lessequalthan

		// greaterthan=valor~separadorDecimales
		// se debe utilizar primero la validacion "num" o "decimal" pq aquí NO se muestra mensaje de error si es un nº incorrecto
		case "gt":
		case "greaterthan":
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				var sepDecimal = arr[1];
				cmdvalue = removeThousandSeparator(arr[0]).replace(/,/g, '.')
			}

			if (isNaN(objValue.value))
			{
				return false;
			}

			if (eval(objValue.value) <=  eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + eval("jsLit_msgCampoMayor.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
											strError = eval("jsLit_msgCampoMayor.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
				}
				else
				{
					//strError = objValue.title + ": " + strError;
					strError =  strError;
				}

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case greaterthan

		// greaterequalthan=valor~separadorDecimales
		// se debe utilizar primero la validacion "num" o "decimal" pq aquí NO se muestra mensaje de error si es un nº incorrecto
		case "get":
		case "greaterequalthan":
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				var sepDecimal = arr[1];
				cmdvalue = removeThousandSeparator(arr[0]).replace(/,/g, '.')
			}

			if (isNaN(objValue.value))
			{
				return false;
			}

			if (eval(objValue.value) < eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + eval("jsLit_msgCampoMayorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
											strError = eval("jsLit_msgCampoMayorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
				}
				else
				{
					//strError = objValue.title + ": " + strError;
					strError = strError;
				}

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case greaterequalthan

		// greaterequalthandate=fechaMenor
		// se debe utilizar primero la validacion "date" pq aquí NO se muestra mensaje de error si es una fecha incorrecta
		case "getd":
		case "greaterequalthandate":
		{
			var dia;
			var mes;
			var anyo;
			var fecha = objValue.value;
			var fechaMenor = cmdvalue;
			var arr = fecha.split("/");
			fecha = null;
			if (arr.length > 2)
			{
				dia = "0"+arr[0];
				mes = "0"+arr[1];
				anyo = arr[2];
				fecha = new Date(anyo, mes, dia);
			}
			arr = fechaMenor.split("/");
			fechaMenor = null;
			if (arr.length > 2)
			{
				dia = "0"+arr[0];
				dia = dia.substring(dia.length-2);
				mes = "0"+arr[1];
				mes = mes.substring(mes.length-2);
				anyo = arr[2];
				fechaMenor = new Date(anyo, mes, dia);
				cmdvalue = dia + "/" + mes + "/" + anyo;
			}
			if (fecha == null || fechaMenor == null)
			{
				return true;
			}

			if (fecha < fechaMenor)
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + eval("jsLit_msgCampoFechaMayorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
											strError = eval("jsLit_msgCampoFechaMayorIgual.replace(/"+jsLit_parametro+"/gi, cmdvalue);");
				}
				else
				{
					//strError = objValue.title + ": " + strError;
					strError = strError;
				}

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case greaterequalthandate

		case "regexp":
		{
			if (objValue.value.length > 0)
			{
				if (!objValue.value.match(cmdvalue))
				{
					if (!strError || strError.length ==0)
					{
	                			//strError = objValue.title + ": " + jsLit_msgCampoIncorrecto;
												strError =  jsLit_msgCampoIncorrecto;
					}
					else
					{
						//strError = objValue.title + ": " + strError;
						strError = strError;
					}

					//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
					//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
					displayError(objValue, strError);

					errorMessages[errorMessages.length] = strError;
					if (!fieldsTogether)
					{
						return false;
					}
				}
			}
			break;
		}//case regexp

		case "startdatelessequalthanenddate":
		case "startdateletenddate":
		/* requiere "jsFormateoFechas.js" */
		{
			var arr = cmdvalue.split("~");
			if (arr.length > 0)
			{
				var startFieldName = arr[0];
				var endFieldName = arr[1];
				var startDate = document.getElementById(startFieldName).value;
				var endDate = document.getElementById(endFieldName).value;
				strError = arr[2];
			}
			var result = validarFechaFin(startDate, endDate);

			if (result < 0)
			{
				//strError = objValue.title + ": " + strError;

				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
			}
			break;
		}//case "startdateenddate"

		case "mustselect":
		{
			if(objValue.selectedIndex == null || objValue.selectedIndex == -1)
			{
				if (!strError || strError.length ==0)
              			{
                			//strError = objValue.title + ": " + jsLit_msgCampoObligatorio;
											strError = jsLit_msgCampoObligatorio;
				}
				else
				{
					//strError = objValue.title + ": " + strError;
				}
				//$("#error_" + objValue.id).css("display", "inline"); /* icoWarning */
				//$("[name=error_" + objValue.id+"]").css("display", "inline"); /* icoWarning */
				displayError(objValue, strError);

				errorMessages[errorMessages.length] = strError;
				if (!fieldsTogether)
				{
					return false;
				}
           		}
           		break;
		}//case mustselect

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case "maxlength":
		case "maxlen":
		{
strError += "FJRP # ";
			if (eval(objValue.value.length) >  eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.name + " : "+cmdvalue+" characters maximum ";
				}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError + "\n[Current length = " + objValue.value.length + " ]");
					return false;
				}
			}
			break;
		}//case maxlen

		case "minlength":
		case "minlen":
		{
strError += "FJRP # ";
			if (eval(objValue.value.length) <  eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.name + " : " + cmdvalue + " characters minimum ";
				}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError + "\n[Current length = " + objValue.value.length + " ]");
					return false;
                 		}
			}
			break;
		}//case minlen

		case "alnum":
		case "alphanumeric":
		{
strError += "FJRP # ";
			var charpos = objValue.value.search("[^A-Za-z0-9]");
			if (objValue.value.length > 0 &&  charpos >= 0)
			{
				if(!strError || strError.length ==0)
				{
					strError = objValue.name+": Only alpha-numeric characters allowed ";
				}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
					return false;
				}
			}
			break;
		}//case alphanumeric

		case "alpha":
		case "alphabetic":
		{
strError += "FJRP # ";
			var charpos = objValue.value.search("[^A-Za-z]");
			if (objValue.value.length > 0 &&  charpos >= 0)
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.name+": Only alphabetic characters allowed ";
                		}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
					return false;
				}
			}
			break;
		}//case alphabetic

		case "alnumhyphen":
		{
strError += "FJRP # ";
			var charpos = objValue.value.search("[^A-Za-z0-9\-_]");
			if (objValue.value.length > 0 &&  charpos >= 0)
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.name+": characters allowed are A-Z,a-z,0-9,- and _";
				}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
					return false;
				}
			}
			break;
		}//case alnumhyphen

		case "dontselect":
		{
strError += "FJRP # ";
			if(objValue.selectedIndex == null)
			{
				alert("BUG: dontselect command for non-select Item");
				return false;
			}
			if (objValue.selectedIndex == eval(cmdvalue))
			{
				if (!strError || strError.length ==0)
				{
					strError = objValue.name+": Please Select one option ";
              			}
				if (fieldsTogether)
				{
					errorMessages[errorMessages.length] = strError;
				}
				else
				{
					alert(strError);
					return false;
				}
			}
			break;
		}//case dontselect

	}//switch
	return true;
}
/*
	Copyright 2003 JavaScript-coder.com. All rights reserved.
*/
