var language;
var userLanguage;
var availableLanguages;
var creatingTranslations=0;
var dbkey = '';

	/**
	 * Carga fichero de idioma y lo asigna a la variable language.
	 * Lanza traducción de todos los items de la web
	 *
	 * @param lang : id del idioma a cargar: es, en, ...
	 */
	loadLanguage = function(lang, forceUpdate){
		var db=$('#pathBDWeb').val();
		if (forceUpdate){
			loadViewContentByCategory(db,"voTraduccionesPorIdioma",lang,processLanguageData,true,lang);
		}
		else {
			language = getSessionData(dbkey + "_language-"+lang.toLowerCase());
			if (language){
				document.dispatchEvent(LanguageReady);
				translateAll();
			}else{
				loadViewContentByCategory(db,"voTraduccionesPorIdioma",lang,processLanguageData,true,lang);
			}
		}
		setSessionData(dbkey + "_userLanguage",lang);
		
	}


	/**
	*Procesa los datos de traducciones y genera el objeto necesario para guardar en la sessionStorage
	*
	*/
	processLanguageData = function(data, langCode){
		if (data.viewentry){
			setSessionData(dbkey + "_userLanguage", langCode);
			language={};
			for (var i=0;i<data.viewentry.length;i++){
				language[(data.viewentry[i].entrydata[0].text[0]).toString()]=data.viewentry[i].entrydata[1].text[0];
			}
			setSessionData(dbkey + "_language-"+langCode.toLowerCase(), language);
			document.dispatchEvent(LanguageReady);
			translateAll();
			if (jsFormNameVisible != "Traduccion" && jsFormNameVisible != "GestorIdiomas"){
				window.location.reload();
			}
			
		}
	}

	/**
	*Elimina un idioma de la session Storage
	*
	*/
	removeLanguage = function(lang){
		removeSessionData(dbkey + "_language-"+lang.toLowerCase());
	}

	getUserLanguange = function(){
		var userLanguage = getSessionData(dbkey + "_userLanguage");
		if (!userLanguage) {
			userLanguage = $('#userLanguage').val(); 
		};
		return userLanguage;
	}

	/**
	 * Devuelve la traducción de un término concreto desde la variable language
	 *
	 * @params term : término a traducir
	 *
	 * @return string con la traducción
	 */
	translate = function(term, originalTermIfTranslationNoExist = false){
		if (term){
			if (language){
				if (originalTermIfTranslationNoExist) {
					return language[term] || term;
				} else {
					return language[term] || ("TRANSLATION_ERROR : " + term);
				}
			}
			else return "TRANSLATION_ERROR";
		}
		else return "";

	}


	/**
	 * Busca todos los items a traducir (marcados con data-translate="xxxx" o data-translate-title) y sustituye su
	 * código html o su title por el término traducido
	 *
	 * @param block : bloque html en el que buscar: body, news-wrapper,...
	 *
	 */
	translateHTMLBlock = function(block){
		if (block==null) block="body";
			var items = $(block).find('[data-translate]');

			for (var i=0; i< items.length; i++){
				var translation= translate($(items[i]).attr("data-translate"));
				$(items[i]).html(translation);
			}
		items=[];
		items = $(block).find('[data-translate-title]');
		for (var i=0; i< items.length; i++){
				var translation= translate($(items[i]).attr("data-translate-title"));
				$(items[i]).attr("title", translation);
		}
		items=[];
		items = $(block).find('[data-translate-value]');
		for (var i=0; i< items.length; i++){
				var translation= translate($(items[i]).attr("data-translate-value"));
				$(items[i]).attr("value", translation);
		}
		items=[];
		items = $(block).find('[data-translate-placeholder]');
		for (var i=0; i< items.length; i++){
				var translation= translate($(items[i]).attr("data-translate-placeholder"));
				$(items[i]).attr("placeholder", translation);
		}
	}

  /**
   * Busca todos los items a traducir (marcados con data-translate="xxxx" o data-translate-title) y sustituye su
   * código html o su title por el término traducido
   *
   * Busca los items dentro de los bloques header, body y footer
	 */
	translateAll = function(){
			var items = $('[data-translate]');
			for (var i=0; i< items.length; i++){
				var translation= translate($(items[i]).attr("data-translate"));
				$(items[i]).html(translation);
			}
			items=[];
			items = $('[data-translate-title]');
			for (var i=0; i< items.length; i++){
					var translation= translate($(items[i]).attr("data-translate-title"));
					$(items[i]).attr("title", translation);
				}
			items=[];
			items = $('[data-translate-value]');
			for (var i=0; i< items.length; i++){
					var translation= translate($(items[i]).attr("data-translate-value"));
					$(items[i]).attr("value", translation);
			}
			items=[];
			items = $('[data-translate-placeholder]');
			for (var i=0; i< items.length; i++){
					var translation= translate($(items[i]).attr("data-translate-placeholder"));
					$(items[i]).attr("placeholder", translation);
			}
			document.dispatchEvent(TranslationsReady);
	}

	iniValidationsTraduccion = function(frmValidator, formName)	{
		frmValidator = new Validator(formName);
		frmValidator.clearAllValidations();
		frmValidator.addValidation("TraduccionIdioma", "req");

		frmValidator.addValidation("TraduccionLiteral", "req");
		var pKey=$('#TraduccionIdioma').val()+"_._"+$('#TraduccionLiteral').val()+"_._";
		frmValidator.addValidation("TraduccionLiteral", "function=validateUnique(jsDbURL, '(Unique.Traduccion)', '"+pKey+"' , 2, jsIdDoc)", translate("litTraduccionUnica"));

		frmValidator.addValidation("TraduccionTraduccion", "req");

		return frmValidator;
	}

	iniValidationsIdioma = function(frmValidator, formName)	{
		frmValidator = new Validator(formName);
		frmValidator.clearAllValidations();
		frmValidator.addValidation("GestIdiomaISOCode", "req");
		frmValidator.addValidation("docFileUpload", "req");
		return frmValidator;
	}

	exportIdioma = function (idioma){
		var db=$('#pathBDWeb').val();
		loadViewContentByCategory(db, "voTraduccionesPorIdioma", idioma, exportFile, false, null);
	}

	exportFile = function (data) {
		if (data.viewentry) {
			var fileContent="";
			for (var i = 0; i< data.viewentry.length;i++){
				fileContent+=(data.viewentry[i].entrydata[0].text[0]).trim() + ";" + (data.viewentry[i].entrydata[1].text[0]).trim()+ "\r\n";

			}
			var encodedUri = encodeURI(fileContent);
			var link = document.createElement("a");

			blob = new Blob([fileContent], {type: "text/plain;charset=UTF-8"}),
			url = window.URL.createObjectURL(blob);
			link.setAttribute("href", url);
			link.setAttribute("taget","_blank");
			link.setAttribute("download", "my_data.csv");
			document.body.appendChild(link); // Required for FF

			link.click(); // This will download the data file named "my_data.csv".

		}
	}

	prepareLangSelector=function(data){
		if (data.viewentry){
			availableLanguages=[];
			var loaded = "es";

			var userLanguage = getSessionData(dbkey + "_userLanguage");
			if (!userLanguage) {
				userLanguage = $('#userLanguage').val(); 
			}

			if (userLanguage != undefined && userLanguage != null) {
				loaded = userLanguage;
			}

			for (var i=0;i<data.viewentry.length;i++){
				var idioma = data.viewentry[i].entrydata[0].text[0];
				availableLanguages.push(idioma);
				if (data.viewentry.length > 1){
					var langSelectorLine = '';
					if (idioma.toLowerCase() === loaded.toLowerCase()) {
						langSelectorLine = '<li><div class="navLink"><a href="" data-language="'+idioma+'" class="loadLanguage lang-selected" data-translate="Idioma-'+idioma+'">'+idioma+'</a></div></li>';
					} else {
						langSelectorLine = '<li><div class="navLink"><a href="" data-language="'+idioma+'" class="loadLanguage" data-translate="Idioma-'+idioma+'">'+idioma+'</a></div></li>';
					}
					
					$('#lang-selector').append(langSelectorLine);
					$('#lang-selector-sm').append(langSelectorLine);
				}
			}

			// Carga del idioma
			if (userLanguage != undefined) {
				if (availableLanguages.indexOf(userLanguage.toUpperCase()) > -1 || availableLanguages.indexOf(userLanguage.toLowerCase()) > -1){
					loaded = userLanguage;
				} else {
					loaded = "es";
				}
			} else {
				loaded = "es";
			}
			loadLanguage(loaded);

			// Listener para el selector de idioma. Guarda localstorage con nuevo idioma y recarga la página
			$('.loadLanguage').unbind("click").click(function(e){
				e.preventDefault();
				var languageToLoad = $(this).attr("data-language");
				loadLanguage(languageToLoad,true);
				$('.loadLanguage').removeClass("lang-selected");
				$(this).addClass("lang-selected");
			});

			if (data.viewentry.length > 1){ // Si solo hay un idioma no mostrar el menú de selección
				$('#lang-menu').removeClass("hiddenElement");
				$('#lang-menu-mov').removeClass("hiddenElement");
			}
			return true;
		}

		$('#lang-menu').addClass("hiddenElement");
		$('#lang-menu-mov').addClass("hiddenElement");
		loadLanguage('es');
		return false;
	}


	displayTranslatableFields = function (data, translatableData){
		if (data.viewentry){
			var bloqueTranslatable = $("#translatables-"+translatableData.id);
			if (bloqueTranslatable.length < 1){
				bloqueTranslatable=$('#'+translatableData.id).parent().parent();
			}
			
			$('.translatable-literal').parent().parent().remove();

			var literal=translatableData.prefix+"_"+$('#'+translatableData.id).val();
			for (var i=0;i<data.viewentry.length;i++){
				var field=_TranslatableTemplate;
				var nombreIdioma=translate("Idioma-" + data.viewentry[i].entrydata[0].text[0]) + " (" +data.viewentry[i].entrydata[0].text[0] +")";
				var idioma=data.viewentry[i].entrydata[0].text[0];
				var traduccion=data.viewentry[i].entrydata[1].text[0];
				field=field.replace(/#TRANSLATABLE_LANG_TITLE#/g, nombreIdioma);
				field=field.replace(/#TRANSLATABLE_LANG#/g, idioma);
				field=field.replace(/#TRANSLATABLE_LITERAL#/g, literal);
				field=field.replace(/#TRANSLATABLE_TRADUCCION#/g, traduccion);
				field=field.replace(/#READ_ONLY#/g,jsIsEditMode?"":"readonly" );
				bloqueTranslatable.append(field);

				if(!jsIsEditMode){
						$(".submit-traduccion").remove();
						$(".undo-traduccion").remove();
				}

			}
			applyTranslatableLiteralsListeners();
		}
		else {
			$.each(availableLanguages, function(index,obj){
				var literal=$('#'+translatableData.id).val();
				creatingTranslations++;
				addTraduccion(translatableData.prefix+"_"+literal,obj,literal);
			});
		}
	}

	applyTranslatableLiteralsListeners = function () {
		$('.translatable-literal').unbind("keyup").keyup(function(){
			var currentVal=$(this).val();
			var originalVal=$(this).attr("data-original-value");

			if (currentVal!=originalVal){
				$(this).parent().find(".undo-traduccion").removeClass("action-disabled").addClass("action-enabled");
				if (currentVal!=""){
					$(this).next(".submit-traduccion").removeClass("action-disabled").addClass("action-enabled");
				}
				else {
					$(this).next(".submit-traduccion").addClass("action-disabled").removeClass("action-enabled");
				}
			}
			else {
				$(this).next(".submit-traduccion").addClass("action-disabled").removeClass("action-enabled");
				$(this).parent().find(".undo-traduccion").addClass("action-disabled").removeClass("action-enabled");
			}
		});

		$('.submit-traduccion').unbind("click").click(function(){
			if ($(this).hasClass("action-enabled")){
				var traduccion=$(this).prev('.translatable-literal').val();
				var idioma=$(this).prev('.translatable-literal').attr("data-language");
				var prefix = $(this).prev('.translatable-literal').attr("data-language");
				var literal=$(this).prev('.translatable-literal').attr("id");
				updateTraduccion (literal,idioma,traduccion);
			}
		});

		$('.undo-traduccion').unbind("click").click(function(){
			if ($(this).hasClass("action-enabled")){
				$(this).parent().find('.translatable-literal').val($(this).parent().find('.translatable-literal').attr("data-original-value"));
				$(this).parent().find('.translatable-literal').keyup();
			}
		});
	}

	addTraduccion= function (literal, idioma, traduccion){
		$.ajax({
		       url: jsDbURL + "/API_Traduccion_Add?OpenAgent&literal="+literal+"&idioma="+idioma+"&traduccion="+traduccion,
		       type: "GET",
		       cache:false,
		       dataType: "JSON",

		       success: function (data) {
						if (data){
							if (data.result=="ok"){
								loadLanguage(getSessionData(dbkey + "_userLanguage"), true); 
								//refrescar  bloques de translatables
								creatingTranslations--;
								if (creatingTranslations==0){
									updateTranslatableFields();
								}
							}
							else {
								//Ocurrió algún error al generar la línea de historial
								console.log("Error al generar traduccion:" + data.errorCode);
							}
						} else {
							//Ocurrió algún error al generar la línea de historial
							console.log("Error al generartraduccion (2): " + data.errorCode);
						}
		       },
		        error: function (xhr, status) {
		    	   console.log("error en la API de crear traducciones");
		    	   console.log(xhr);
		    	   console.log(status);
		    }
		});
	}

	updateTraduccion= function (literal, idioma, traduccion){
		$.ajax({
		       url: jsDbURL + "/API_Traduccion_Update?OpenAgent&literal="+literal+"&idioma="+idioma+"&traduccion="+traduccion,
		       type: "GET",
		       cache:false,
		       dataType: "JSON",

		       success: function (data) {
						if (data){
							if (data.result=="ok"){
								showNotification(translate("TraduccionActualizada"), "Success");
								updateTranslatableFields();
							}
							else {
								//Ocurrió algún error al actualizar la traducciónote
								showNotification(translate("TraduccionNoActualizada"), "Error");
								updateTranslatableFields();
								console.log("Error al cambiar traduccion:" + data.errorCode);
							}
						}else {
							//Ocurrió algún error al generar la línea de historial
							showNotification(translate("TraduccionNoActualizada"), "Error");
							updateTranslatableFields();
							console.log("Error al cambiar la traduccion (2): " + data.errorCode);
						}
		       },
		        error: function (xhr, status) {
					showNotification(translate("TraduccionNoActualizada"), "Error");
					updateTranslatableFields();
					console.log("error en la API de crear traducciones");
		    }
		});
	}

	updateTranslatableFields = function(){
		//Gestión de campos "Traducibles/Translatable"
		var translatables=$('[data-translatable]');
		$.each(translatables, function(index,obj){
			if (jsIsNewDocument==0){
				var literal=$(obj).val();
				var prefix= $(obj).data("translatable-prefix");
				var db=$('#pathBDWeb').val();
				var translatableData = {"id":$(obj).attr("id"), "prefix":prefix };
				loadViewContentByCategory(db, "voTraduccionesPorLiteral", prefix+"_"+literal, displayTranslatableFields, false, translatableData);
			}
			else {
				//isNewDoc
			}
		});
	}



$(document).ready(function(){
	language={};
	availableLanguages=[];

	var db = $('#pathBDWeb').val();
	dbkey = db; // Clave para distinguir entre BBDD

	if(!getSessionData(dbkey + "_userLanguage")){
		if ($('#PaisUsuario').val()){
			userLanguage = $('#PaisUsuario').val().substring(0,2).toLowerCase(); 
		}else {
			var navigatorLanguage = navigator.language;
			userLanguage = navigatorLanguage.substring(0,2).toLowerCase(); 
		}
		
		setSessionData(dbkey + "_userLanguage", userLanguage);
	}

	//Cargar listado de idiomas
	loadAllViewContent(db,"voIdiomas", prepareLangSelector, false, null);
	

	document.addEventListener("LanguageReady", function(){
		updateTranslatableFields();
	})

}) ;
