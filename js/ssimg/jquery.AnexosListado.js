

		_db=$('#pathBDWeb').val();
		var _loadedAnexos={};

		refrescarBloqueDeAnexos = function(bloque){
			var parentID=$(bloque).attr("data-parent-id");
			var template=$(bloque).attr("data-template");

			var bloqueAnexos=_BloqueAnexosTemplate;
			if (template && _AnexoBDTemplates){
				if (_AnexoBDTemplates[template]){
					bloqueAnexos=_AnexoBDTemplates[template]["bloqueAnexosTemplate"]
				}
			}

			var date = new Date();
			var tempIndex=date.getTime()+Math.floor(Math.random() * 100);
			bloqueAnexos=bloqueAnexos.replace(/#INDEX#/g,tempIndex.toString());
			$(bloque).html(bloqueAnexos);
			_loadedAnexos[parentID]=[];

			loadViewContentByCategory(_db, "voAnexos", parentID, appendAnexos, false, parentID)

			translateHTMLBlock('.bloque-anexos');
		}

		refrescarBloquesDeAnexos = function(){
			applyUploaderListeners();
			var listas=$('.bloque-anexos');
			$.each(listas, function(index,obj){
				var parentID=$(obj).attr("data-parent-id");
				var template=$(obj).attr("data-template");
				if($(obj).find('.accordion').length==0){
					var bloqueAnexos=_BloqueAnexosTemplate;
					if (template && _AnexoBDTemplates){
						if (_AnexoBDTemplates[template]){
							bloqueAnexos=_AnexoBDTemplates[template]["bloqueAnexosTemplate"]
						}
					}

					var date = new Date();
					var tempIndex=date.getTime()+Math.floor(Math.random() * 100);
					bloqueAnexos=bloqueAnexos.replace(/#INDEX#/g,tempIndex.toString());
					$(obj).html(bloqueAnexos);
					_loadedAnexos[parentID]=[];
				}

				loadViewContentByCategory(_db, "voAnexos", parentID, appendAnexos, false, parentID)
			});
			translateHTMLBlock('.bloque-anexos');
		}

		function resetearBloquesDeAnexos(){
			var listas=$('.bloque-anexos');
			$.each(listas, function(index,obj){
				var parentID=$(obj).attr("data-parent-id");
				_loadedAnexos[parentID]=[];
				$(obj).html("");
			});
			refrescarBloquesDeAnexos();
		}


		function appendAnexos(data, parentID){
			var numelements = 0;
			if (data.viewentry){
				var bloque=$('.bloque-anexos[data-parent-id="'+parentID+'"] .an-card-content');
				var template=$('.bloque-anexos[data-parent-id="'+parentID+'"]').attr("data-template");

				var _$AnexoItemTemplate=_AnexoItemTemplate;
				var _$DeleteAnexoTemplate=_DeleteAnexoTemplate;
				if (template && _AnexoBDTemplates){
					if (_AnexoBDTemplates[template]){
						_$AnexoItemTemplate=_AnexoBDTemplates[template]["anexoItemTemplate"]
						_$DeleteAnexoTemplate=_AnexoBDTemplates[template]["deleteAnexoTemplate"]
					}
				}

				var anexosEditables = $('.bloque-anexos[data-parent-id="'+parentID+'"]').attr("data-editable")=="1";
				numelements = data.viewentry.length;

				for (var i=0; i<data.viewentry.length;i++){
					var anexoUNID = data.viewentry[i]["@unid"];
					if (_loadedAnexos[parentID].indexOf(anexoUNID)==-1){
						var nomFichero=data.viewentry[i].entrydata[1].text[0];
						var enlace=data.viewentry[i].entrydata[2].text[0];
						var creador=data.viewentry[i].entrydata[4].text[0];
						var fecha=data.viewentry[i].entrydata[0].text[0];
						var comentario=data.viewentry[i].entrydata[5].text[0];
						if (comentario=="0") comentario="";


						//Condiciones para mostrar o no el acceso a editar el anexo
						if (anexosEditables) {
							var deleteLink=_$DeleteAnexoTemplate;
							deleteLink=deleteLink.replace(/#ANEXO_UNID#/g, anexoUNID);
							deleteLink=deleteLink.replace(/#ANEXO_PADRE_UNID#/g, parentID);
						}
						else var deleteLink="";

						var anexo=_$AnexoItemTemplate;
						anexo=anexo.replace(/#ANEXO_UNID#/g, anexoUNID);
						anexo=anexo.replace(/#ENLACE#/g, enlace);
						anexo=anexo.replace(/#NOMBRE_FICHERO#/g, nomFichero);
						anexo=anexo.replace(/#CREADOR#/g, creador);
						anexo=anexo.replace(/#FECHA_UPLOAD#/g, fecha);
						anexo=anexo.replace(/#ANEXO_COMENTARIO#/g, comentario);
						anexo=anexo.replace(/#DELETE_LINK#/g, deleteLink);
						anexo=anexo.replace(/#TOOLTIP-ID#/g, i);

						// Fork para permitir previsualización de anexos
						if (visualizableFile(nomFichero)) {
							anexo=anexo.replace(/#HIDDENSHOWLINK#/g, '');
							anexo=anexo.replace(/#SHOWLINK#/g, enlace);
						} else {
							anexo=anexo.replace(/#HIDDENSHOWLINK#/g, 'hiddenElement');
						}

						bloque.append(anexo);
						$('.item-anexo[data-an-unid="'+anexoUNID+'"]').show(400);
						_loadedAnexos[parentID].push(anexoUNID)
						$('.an-comment-edit').attr("data-original-title", translate("litComentarioAnexo"));
					}
				}
				$('.bloque-anexos[data-parent-id="'+parentID+'"]').find(".numAnexos").html("("+i+")").attr("data-qty",i);
				var qtyValidator=$('.bloque-anexos[data-parent-id="'+parentID+'"]').find(".numAnexosValidator");
				if (qtyValidator){
					$(qtyValidator).val(i);
				}
				reloadAnTooltips();
				applyAnexosListeners();
			}

			controlNumAnexos(numelements,parentID);
			var anexosReady = new CustomEvent("anexosReady", {detail:{"bloqueAnexosID" : parentID}});
      document.dispatchEvent(anexosReady);
		}

		function controlNumAnexos(numelements,parentID) {
			// Control de número de anexos para esconder el bloque de subida
			var numAnexos = $('.bloque-anexos[data-parent-id="'+parentID+'"]').attr("data-nummax");
			if (numAnexos != undefined && numAnexos != "" && numAnexos != null) {
				var bloqueUpload = $('.bloque-upload[data-parent-id="'+parentID+'"]');

				if (numelements == 0) {
					bloqueUpload.show();
				} else if (numelements >= numAnexos) {
					bloqueUpload.hide();
				} else {
					bloqueUpload.show();
				}
			}
		}

		function visualizableFile(filePath) {
			var allowedExtensions = /(.jpg|.jpeg|.png|.gif|.pdf|.txt|.html)$/i;
			return allowedExtensions.exec(filePath);
		}

		applyAnexosListeners = function(){
			$('.delete-an').unbind("click").click(function(){
				var anexoUNID = $(this).attr("data-an-unid");
				var parentUNID = $(this).attr("data-parent-unid");

				$('[data-toggle="tooltip-an"]').each(function(obj){
						$(this).tooltip('hide');
				});

				$(this).parent().hide("500", function(){$(this).remove();});
				deleteAnexo(anexoUNID, parentUNID);
			});


			$('.an-comment-edit').editable({
					toggle: 'manual',
					type: 'text',
					title: 'Comentario',
					emptytext : "",
					display: function(value){
						return "";
					},
					success: function(response, newValue) {
							updateAnexo($(this).attr("data-an-unid"), "comment", newValue);
					}
			});

			$('.an-filename-edit').editable({
				toggle: 'manual',
				type: 'text',
				title: 'Nombre de fichero',
				emptytext : "",
				display: function(value){
					return "";
				},
				success: function(response, newValue) {
					updateAnexo($(this).attr("data-an-unid"), "filename", newValue);
				}
			});

			$('.anEditToggle').unbind("click").click(function(e){
				e.stopPropagation();
				var id = $(this).attr("data-an-unid");
				$('.an-comment-edit[data-an-unid="'+id+'"]').editable('toggle');
				$('html, body').animate({
 					scrollTop: $(e.target).offset().top
 				}, 0);
			});

			$('.anEditFilenameToggle').unbind("click").click(function(e){
				e.preventDefault();
				e.stopPropagation();

				var id = $(this).attr("data-an-unid");
				$('.an-filename-edit[data-an-unid="'+id+'"]').editable('toggle');
				$('html, body').animate({
 					scrollTop: $(e.target).offset().top
 				}, 0);
			});

			$('.an-view-info').unbind("click").click(function(){
				self=$(this).attr("data-tooltip-toggle");
				$('[data-toggle="tooltip-an"]').each(function(){
					id=$(this).attr("data-tooltip-id");
					if (id!=self) {
						$(this).tooltip('hide');
					}
					else {
						$(this).tooltip('toggle');
					}
				});
			});

		}


		reloadAnTooltips = function(){
			$('[data-toggle="tooltip-an"]').tooltip('update');
		}


		//Función para eliminar anexos llamado al agente de eliminación
		deleteAnexo = function(anexoUNID,parentUNID){
			$.ajax({
				url: _db + "/API_Anexo_Delete?OpenAgent&idAnexo="+anexoUNID,
				type: "GET",
			    cache:false,
			    dataType: "JSON",

				success: function (data) {
					if (data.result=="ok"){
						_loadedAnexos[parentUNID].slice(_loadedAnexos[parentUNID].indexOf(anexoUNID),1);
						var oldQty=$('.bloque-anexos[data-parent-id="'+parentUNID+'"] .numAnexos').attr("data-qty");
						if (oldQty>0){
							var qty = oldQty - 1;
						}
						else { var qty = 0;}
						$('.bloque-anexos[data-parent-id="'+parentUNID+'"]').find(".numAnexos").html("("+qty+")").attr("data-qty",qty);
						var qtyValidator=$('.bloque-anexos[data-parent-id="'+parentUNID+'"]').find(".numAnexosValidator");
						if (qtyValidator){
							$(qtyValidator).val(qty>0?qty:"");
						}else {
							//console.log("NOOOO!!! " + qty);
						}
						controlNumAnexos(qty,parentUNID);
						showNotification(translate("AnexoEliminado"), "Success");
						$(document).trigger("AnexoDeleted",[anexoUNID]);
					}
					else {
						refrescarBloquesDeAnexos();
						showNotification(translate("ErrorEliminarAnexo") + " " + data.errorCode, "Error");
					}

			},
			error: function (xhr, status) {
				showNotification(translate("ConnectionError"), "Error");
				console.log(status);
				console.log(xhr);
			}
			});
		}

		updateAnexo = function(anexoUNID, campo, nuevoValor){
			$.ajax({
				url: _db + "/API_Anexo_Update?OpenAgent&idAnexo="+anexoUNID+"&campo="+campo+"&valor="+nuevoValor,
				type: "GET",
			    cache:false,
			    dataType: "JSON",

				success: function (data) {
					if (data.result=="ok"){
						$('.an-comment-edit[data-an-unid="'+anexoUNID+'"]').attr("data-value",nuevoValor);
						$('[data-an-unid="'+anexoUNID+'"] a.an-name span').attr("data-original-title",nuevoValor);
						reloadAnTooltips();
						showNotification(translate("AnexoActualizado"), "Success");
						if (campo == "filename") {
							resetearBloquesDeAnexos();
						}
					}
					else {
						refrescarBloquesDeAnexos();
						showNotification(translate("ErrorActualizarAnexo")+ " " + data.errorCode, "Error");
					}

			},
			error: function (xhr, status) {
				showNotification(translate("ConnectionError"), "Error");
				console.log(status);
				console.log(xhr);
			}
			});
		}



document.addEventListener("LanguageReady", function(){
			refrescarBloquesDeAnexos();
});
