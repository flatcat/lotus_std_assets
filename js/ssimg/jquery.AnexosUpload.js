
	var flag="";
	var flagInterval=null;

	applyUploaderListeners = function() {
		$("iframe").load(function(){
			var contents = $(this).contents();
			var validExtensions=contents.find("#validExtensions").val() != undefined ? contents.find("#validExtensions").val() : "";

			if (validExtensions!=""){
				validExtensions=validExtensions.toUpperCase();
				validExtensions=validExtensions.replace(/ /g, "");
				contents.find("#docFileUpload").prop("accept", validExtensions);
			}

			var fileFromCamera = contents.find("#isCamera").val();
			var isCamera = fileFromCamera != undefined ? fileFromCamera : 0;
			if (isCamera == "1") {
				contents.find("#docFileUpload").attr("capture", "camera");
			}

			var fieldMultiple=contents.find("#Multiple").val();
			var multiple=fieldMultiple != undefined ? fieldMultiple : 1;
			if (multiple==0){
				contents.find("#docFileUpload").removeAttr("multiple");
			}

			contents.find("#docFileUpload").on("change", function(){
					var flag = contents.find("#updateFlag").val();
					if (flagInterval){
						clearInterval(flagInterval);
					}
					flagInterval=setInterval(function(){
						newVal = contents.find("#updateFlag").val();
						if (newVal != flag){
							$(document).trigger("AnexoUploaded");
							refrescarBloquesDeAnexos();
							clearInterval(flagInterval);
						}
					},300);

			});
		});
	}

$(document).ready(function(){
	/**
	 *
	 * Zona de control de uploads
	 *
	 */
	$('#docFileUpload').change(function(){

		var validExtensions=[];
		if ($(this).prop("accept")!=""){
			validExtensions=$(this).prop("accept").split(",");
		}


		var options={};
		options.clearForm=true;
		options.resetForm=true;
		options.success=successFunction;

		var fileNames="";
		var files = document.getElementById("docFileUpload").files;
		for (var i = 0; i < files.length; i++){
			if (validExtensions.length>0){
				if (validateFileExtensions(validExtensions, files[i].name)){
					fileNames+=files[i].name + ", ";
				}
				else {
					$(this).val("");
					showNotification(translate("ErrorExtensionNoValida"), "error")
				}
			}

		}
		fileNames=fileNames.substring(0, fileNames.length - 2);

		if (fileNames.length>30) fileNames=fileNames.substring(0, 30) + "...";

		$('#file-name').html(fileNames);
		$('#file-loading').show();
		$('form').ajaxSubmit(options);
		$('#docFileUpload').attr("disabled","true");
	});


	successFunction = function (response){
		$('#file-loading').hide();
		$('#file-loaded').show(200);
		setTimeout(function(){$('#file-loaded').hide(500);},1500);
		$('#docFileUpload').removeAttr("disabled");

		$('#updateFlag').val((new Date()).toString()).change();
	}

	validateFileExtensions = function (validFileExtensions, fileName){

		var tempArray=fileName.split(".");
		var fileExtension="."+tempArray[tempArray.length -1].toUpperCase();
	
		if (validFileExtensions.includes(fileExtension)){
			return true;
		}
		else {
			return false;
		}
	}


	applyUploaderListeners ();

});
