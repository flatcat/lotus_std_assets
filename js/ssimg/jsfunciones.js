/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Elimina todos los dobles espacios (Regular expressions for normalizing white space.)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
var whtSpEnds = new RegExp("^\\s*|\\s*$", "g");
var whtSpMult = new RegExp("\\s\\s+", "g");
completeTrim = function(s)
{
s = s + ""; // Para que lo trate como un string.
if (s.length > 0)
{
s = s.replace(whtSpMult, " ");  // Collapse any multiple whites space.
s = s.replace(whtSpEnds, "");  // Remove leading or trailing white space.
}

return s;
};
fullTrim = function(s)
{
s = s + ""; // Para que lo trate como un string.
var arrMultiLine = s.split("\n");
for (var i=0; i <arrMultiLine.length; i++)
{
arrMultiLine[i] = completeTrim(arrMultiLine[i]);
}

s = arrMultiLine.join("\n");

return s;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Quita los puntos "." separadores de miles.
// Parámetros:
//	- num: string del que quitar los puntos "."
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
removeThousandSeparator = function(num)
{
var valor = "";
valor = replaceSubstring(num, '.', '', true);
return valor;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Añade los puntos "." separadores de miles.
// Parámetros:
//	- num: string al que añadir los puntos "."
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
addThousandSeparator = function(num, decimalPoint, thousandSeparator)
{
// check for missing parameters and use defaults if so
if (arguments.length == 2)
{
thousandSeparator = ".";
}
if (arguments.length == 1)
{
thousandSeparator = ".";
decimalPoint = ",";
}
// remove thousandSeparator and replace decimalPoint with javascript native decimal point (.) to ensure there is only 1 decimal point
var tempNum= replaceSubstring(replaceSubstring(num, thousandSeparator, " ", true), decimalPoint, ".", true);
tempNum = tempNum.split(".");
if (tempNum.length > 2) // there are more than 1 decimal point
return num;
// need a string for operations
num = num.toString();
// separate the whole number and the fraction if possible
a = num.split(decimalPoint);
x = a[0]; // decimal
y = a[1]; // fraction
z = "";

// replace posible use of thousandSeparator as decimalPoint
x = replaceSubstring(x, thousandSeparator, "", true);

if (typeof(x) != "undefined")
{
if (!isNaN(parseFloat(x)))
{
x = parseFloat(x).toString();
// reverse the digits. regexp works from left to right.
for (i=x.length-1;i>=0;i--)
z += x.charAt(i);

// add seperators. but undo the trailing one, if there
z = z.replace(/(\d{3})/g, "$1" + thousandSeparator);
if (z.slice(-thousandSeparator.length) == thousandSeparator)
z = z.slice(0, -thousandSeparator.length);
x = "";

// reverse again to get back the number
for (i=z.length-1;i>=0;i--)
x += z.charAt(i);

// add the fraction back in, if it was there
if (typeof(y) != "undefined" && y.length > 0)
x += decimalPoint + y;
}
}
return x;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función @Left
// Devuelve lo que hay a la izquierda del "subString", empezando a mirar de izquierda a derecha.
// Si "subString" no se encuentra en "fullString" devuelve ""
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
leftString = function(fullString, subString)
{
return (fullString.substring(0, fullString.indexOf(subString)));
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función @LeftBack
// Devuelve lo que hay a la izquierda del "subString", pero empezando a mirar de derecha a izquierda.
// Si "subString" no se encuentra en "fullString" devuelve ""
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
leftBackString = function(fullString, subString)
{
var pos = fullString.lastIndexOf(subString);

if (pos < fullString.length)
	return (fullString.substring(0, pos));
else
	return ("");
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función @Right
// Devuelve lo que hay a la derecha del "subString", empezando a mirar de izquierda a derecha.
// Si "subString" no se encuentra en "fullString" devuelve ""
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
rightString = function(fullString, subString)
{
if (subString != "" && fullString !="" && fullString.indexOf(subString) > -1)
{
return (fullString.substring(fullString.indexOf(subString) + subString.length, fullString.length));
}
else
{
return ("");
}
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función @RightBack
// Devuelve lo que hay a la derecha del "subString", pero empezando a mirar de derecha a izquierda.
// Si "subString" no se encuentra en "fullString" devuelve ""
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
rightBackString = function(fullString, subString)
{
if (subString != "" && fullString !="" && fullString.lastIndexOf(subString) > -1)
{
return (fullString.substring(fullString.lastIndexOf(subString) + subString.length, fullString.length));
}
else
{
return ("");
}
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Función @ReplaceSubstring
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
replaceSubstring = function(str_normal, str_find, str_replace, int_case_insensitive)
{
//Nos aseguramos que los parámetros tipo texto sean string's
str_normal += "";
str_find += "";
str_replace += "";
if (arguments.length<3 || str_find=="" || str_normal=="" || typeof("".split)!="function")
return(str_normal);
//no parm means default, "case SENSITIVE"...
if (!(int_case_insensitive))
return(str_normal.split(str_find)).join(str_replace);

str_find=str_find.toLowerCase();
var rv="";
var ix=str_normal.toLowerCase().indexOf(str_find);
while(ix>-1)
{
rv+=str_normal.substring(0,ix)+str_replace;
str_normal=str_normal.substring(ix+str_find.length);
ix=str_normal.toLowerCase().indexOf(str_find);
};
return(rv+str_normal);
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve el primer childNode que es del tipo y tiene el nombre que le indicamos por parámetro.
// Soluciona el problema en el formateo de las vistas que, al coger el div "embedView", el primer childNode no es la tabla sino un texto.
// Parámetros:
//		- nodeParent: nodo padre (parentNode)
//		- typeNode: tipo del nodo que buscamos (nodeType: 9=doc, 1=element, ...)
//		- nameElement: nombre del elemento que buscamos (nodeName)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
getChildNode = function(nodeParent, typeNode, nameElement)
{
var nodeChild;
var nodeFound = null;
for (var i=0; i < nodeParent.childNodes.length; i++)
{
nodeChild = nodeParent.childNodes[i];
if (nodeChild.nodeType == typeNode && nodeChild.nodeName.toLowerCase() == nameElement.toLowerCase())
{
nodeFound = nodeChild;
i = nodeParent.childNodes.length + 1; // Para que salga del For
}
}
return nodeFound;
};


/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve el QueryString.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
getQueryString = function()
{
return window.location.search.substring(1);
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve un array con los parámetros del QueryString.
// Parámetros:
//				- queryString:
//
// El formato del array devuelto es:
// 			array[nombre_parametro_en_minusculas] = valor_completo_del_parametro
//
//				p.e.: 	array["start"] = "Start=12"
//							array["endview"] = "EndView"
//							array["login"] = "Login"
//							array["cache"] = "Cache=0.12345678"
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
var qsParams = getQueryString(); // Variable global con el QueryString

createArrayQS = function(queryString)
{
if (queryString == undefined)
queryString = "";
// Si no se pasa el QueryString, se coge de la URL
if (fullTrim(queryString).length == 0)
queryString = getQueryString();

var params = queryString.split('&');
var key = "";
var val = "";
var pos = 0;

var arrParams = new Array();
for (var i=0; i<params.length; i++)
{
addQSParam(arrParams, params[i]);
}

return arrParams;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Añade al array con el QueryString la entrada que le pasamos por parámetro.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
//				- newParam: nuevo valor a añadir al QueryString (p.e.: "Start=8").
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
addQSParam = function(qsParams, newParam)
{
var key = leftString(newParam.toLowerCase(), "=");
var val = "";

if (key.length == 0)
key = newParam.toLowerCase();

// El parámetro de caché NO lo guardamos
if (key != "cache")
{
val = newParam;
qsParams[key] = val;
}
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Elimina del array con el QueryString la clave que le pasamos por parámetro.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
//				- key: clave del parámetro a buscar.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
deleteQSParam = function(qsParams, key)
{
key = key.toLowerCase();

for (var p in qsParams)
{
if (p == key)
delete qsParams[key];
}
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve el valor del parámetro del QueryString cuya clave le pasamos.
// Si no encuentra la clave en el array, devuelve "".
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
//				- key: clave del parámetro a buscar.
//				- defaultValue: valor que devolverá si no encuentra la clave (esto irá bien para inicializar el parámetro Count a 0)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
getQSValue = function(qsParams, key, defaultValue)
{
if (defaultValue == undefined)
defaultValue = "";
var val = "";

key = key.toLowerCase();
val = qsParams[key];
if (val == undefined)
{
val = defaultValue;
}
else
{
val = rightString(val, "=");
}
return val;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve el nombre (tal y como viene en el QueryString) del parámetro cuya clave le pasamos.
// Si no encuentra la clave en el array, devuelve "".
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
//				- key: clave del parámetro a buscar.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
getQSName = function(qsParams, key)
{
var val = "";
var valTemp = "";
key = key.toLowerCase();
valTemp = qsParams[key];
if (valTemp == undefined)
{
val = "";
}
else
{
val = leftString(valTemp, "=");
if (val.length == 0)
val = valTemp;
}

return val;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Devuelve un string con los parámetros del QueryString que se encuentran en el array.
// Realiza algún filtrado, como por ejemplo, si se encuentra el parámetro "count" sin valor, no lo incluye en el resultado.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
createQS = function(qsParams)
{
var strQS = "";
var nomParam = "";
var valorParam = "";
var strParam = "";
for (var p in qsParams)
{
nomParam = getQSName(qsParams, p);
valorParam = getQSValue(qsParams, p);
if ( !(nomParam.toLowerCase() == "count" && fullTrim(valorParam).length == 0))
{
strParam = nomParam;
if (fullTrim(valorParam).length > 0)
strParam += "=" + valorParam;
strQS += strParam + "&"
}
}

if (strQS.length > 0)
	strQS = strQS.substring(0, strQS.length - 1);

return strQS;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Cambia el QueryString de la ventana actual por el que le pasamos por parámetro y vuelve a cargar la página.
// Parámetro:
//				- newQS: nuevoQueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
reloadURLQueryString = function(newQS)
{
	var currentLocation = window.location.toString();
	var newLocation = currentLocation.replace(window.location.search, "?" + newQS);

	window.location = newLocation;
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Deshabilita los botones "no operativos" de la barra de acciones de la vista.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
deshabilitarAccionesVista = function(qsParams)
{
var img;
var id_ico;
var src;
// Desactivar iconos páginas siguientes ...
if (getQSValue(qsParams, "EndView", "0") != "0")
{
id_ico = "icoNextPage";

$("i[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");

id_ico = "icoLastPage";

$("i[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");

}
else
{
var numFilasEncontradas = $("#embedView table tr").length - 1; /* -1 porque cuenta los títulos de columna */
var numFilasPorPagina = getQSValue(qsParams, "count", numFilasEncontradas);
if (numFilasEncontradas < numFilasPorPagina || numFilasPorPagina == "-1")
{
id_ico = "icoNextPage";

$("i[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");

id_ico = "icoLastPage";

$("i[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");
}
}
// ... Desactivar iconos páginas anteriores
if (getQSValue(qsParams, "start") == "1")
{
id_ico = "icoFirstPage";

$("img[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");

id_ico = "icoPrevPage";

$("img[id="+id_ico+"]").attr("title", "").attr("alt", "").removeClass("icoViewAction").addClass("icoViewActionDisabled");
}

};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Formatea el mensaje "No documents found" de las vistas sin documentos y oculta la barra de acciones de la vista
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
formatNoDocumentsFound = function()
{
var oH2s = document.getElementsByTagName('h2');
var oTmp; var oH2;

if (oH2s)
{
for (var i=0; i<oH2s.length; i++)
{
var oTmp = oH2s[i];
if (oTmp.firstChild.data == 'No documents found')
oH2 = oTmp;
}

if (oH2)
{
var accionesVista = document.getElementById("vistaAcciones");
if (accionesVista != null)
document.getElementById("vistaAcciones").style.display = "none";
oH2.className = "noDocumentsFound";
oH2.firstChild.data = jsLit_NoDocumentsFound;
}
}
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Se posiciona en la primera página de la vista.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
gotoStartPage = function(qsParams)
{
addQSParam(qsParams, "Start=1");
deleteQSParam(qsParams, "EndView");
var newQS = createQS(qsParams);

reloadURLQueryString(newQS);
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Se posiciona en la última página de la vista.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
gotoEndPage = function(qsParams)
{
addQSParam(qsParams, "Start=0");
addQSParam(qsParams, "EndView");
var newQS = createQS(qsParams);

reloadURLQueryString(newQS);
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Actualiza la vista quedándose en la página en la que se encuentra, pero añadiéndole a al URL un caché.
// Parámetros:
//				- qsParams: array con los parámetros del QueryString
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
actualizarVista = function(qsParams)
{
var newQS = createQS(qsParams);
newQS += "&cache=" + String(Math.random());
reloadURLQueryString(newQS);
};

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Filtra la URL para las vistas (para corregir los problemas de plegar/desplegar/última página/...)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
filtrarURL = function()
{
var urlNew = replaceSubstring(window.location, "&Start=0", "&Start=1", true);
var urlActual = window.location.href;

if (urlActual.toLowerCase().lastIndexOf("&start=0") != -1 && (urlActual.toLowerCase().lastIndexOf("collapse") != -1 || urlActual.toLowerCase().lastIndexOf("expand") != -1))
{
if (urlNew.toLowerCase() != urlActual.toLowerCase())
{
window.location = urlNew;
}
}
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Ordena el array que le pasamos por parámetro
// Parámetro:
//			- arrayIn: array a ordenar
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
arraySort = function(arrayIn)
{
for (i = 0; i <= arrayIn.length; i++)
{
j = i;
for (k = i; k < arrayIn.length; k++)
{
if (arrayIn[k].text.toLowerCase() < arrayIn[j].text.toLowerCase())
{
j = k;
}
}
if (j > i)
{
l = arrayIn[i].text;
arrayIn[i].text = arrayIn[j].text;
arrayIn[j].text = l;
}
}
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// replaceComillas: Cambia comilla doble (") por doble acento (´´) y comilla simple (') por acento (´)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
replaceComillas = function(s)
{
if (s.length > 0)
{
s = s.replace(/"/g,'´´').replace(/'/g,'´');
}
return s;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// replaceTagSimulator: Cambia el caracter < y > por " < " o " > " para que los richtext se lean correctamente.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
replaceTagSimulator = function(s)
{
if (s.length > 0)
{
s = s.replace(/</g,' < ').replace(/>/g,' > ');
}
return s;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Nombre: string2Unicode
// Entrada: entrada --> String
// Salida: String
// Descripción: dado el string de entrada, lo convierte códigos Unicode separador por puntos
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
string2Unicode = function(entrada)
{
	var salida="";
	if (entrada.length > 0)
	{
		for( i=0 ; i<entrada.length ; i++ )
		{
			// la función charCodeAt(x) transforma x en caracter.
			salida = salida + "." + entrada.charCodeAt(i);
		}
		salida += ".";
	}
	return salida;
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Nombre: CambiarBlancos
// Entrada: spacey --> String al que hay que eliminar los blancos
// Salida: String
// Descripción: dado el string de entrada, cambia cada blanco por un "+"
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CambiarBlancos = function(spacey)
{
	// Devuelve la primera posición de lo que estamos buscando, en este caso un blanco.
	i = spacey.indexOf(" ");
	if (i>=0)
	{
		return spacey.substring(0,i)  + "+" + CambiarBlancos(spacey.substring(i+1,spacey.length));
	}
	else
	{
		return spacey;
	}
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Nombre: snifferStart
// Entrada: url --> String que corresponde a la parte Search de la URL
// Salida: String
// Descripción: dado el string de entrada, mira si contiene un "Start=" o "StartKey=" y lo retorna
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
snifferStart = function(url)
{
	var URL = url.toUpperCase();
	// devuelve la posición de la "S"
	var start = URL.indexOf("START");
	var final;
	var res;

	// Si que hay una entrada que empieza por Start
	if (start>=0)
	{
		//Busco un & a partir del primer Start que encuentra , el 5 es orientativo.
		final = URL.indexOf("&", start + 5);
		if (final>=0)
		{
			res ="&" + URL.substr(start, (final - start));
		}
		else
		{
			res = "&" + URL.substr(start);
		}
	}
	// No hay ningun Start previo, por tanto el retorno sera la posición 1 de la vista
	else
	{
		return "&Start=1";
	}
	return res;
}
