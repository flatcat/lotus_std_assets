_vistaEntradasHistorial = "voEntradasHistPorPadre";

refrescarBloquesHistorial = function () {
  $(".bloque-historial").each(function (index, obj) {
    if ($(obj).find(".accordion").length == 0) {
      bloqueHistorial = _BloqueHistorialTemplate;
      bloqueHistorial = bloqueHistorial.replace(/#INDEX#/g, index);
      $(obj).html(bloqueHistorial);
    }

    parentId = $(obj).attr("data-parent-id");
    loadViewContentByCategory(jsDbURL,_vistaEntradasHistorial,parentId,appendEntradasHistorial,false, parentId);
  });
};

function appendEntradasHistorial(data, parentId) {
  if (data.viewentry) {
    var parentElement = parentId;
    bloque = $('.bloque-historial[data-parent-id="' +parentElement +'"] .hist-card-content');
    bloque.html("");

    for (var i = 0; i < data.viewentry.length; i++) {
      entrada = _EntradaHistorialTemplate;

      var accion = data.viewentry[i].entrydata[1].text[0];
      var accionMultiidioma = data.viewentry[i].entrydata[4].text[0];
      var usuario = data.viewentry[i].entrydata[2].text[0];
      var fechaHora = data.viewentry[i].entrydata[0].text[0];
      var comentario = data.viewentry[i].entrydata[5].text[0];

      if (accionMultiidioma != "") {
        entrada = entrada.replace(/#ACCION#/g, translate(accionMultiidioma));
      } else {
        entrada = entrada.replace(/#ACCION#/g, accion);
      }

      if (comentario != "") {
        entrada = entrada.replace(/#COMENTARIO#/g, _EntradaHistorialComentario);
        entrada = entrada.replace(/#ACCION_COMENTARIO#/g, comentario);
      } else {
        entrada = entrada.replace(/#COMENTARIO#/g, "");
      }

      entrada = entrada.replace(/#USUARIO#/g, usuario);
      entrada = entrada.replace(/#FECHAHORA#/g, fechaHora);

      bloque.append(entrada);
    }
  }
  translateHTMLBlock(".bloque-historial");
}

addEntradaHistorial = function (accion, usuario, parentId) {
  $.ajax({
    url:
      jsDbURL +
      "/API_Historial_Add?OpenAgent&accion=" +
      accion +
      "&usuario=" +
      usuario +
      "&idPadre=" +
      parentId,
    type: "GET",
    cache: false,
    dataType: "JSON",

    success: function (data) {
      if (data) {
        if (data.result == "ok") {
          //refrescar  bloques de historial
          refrescarBloquesHistorial();
        } else {
          //Ocurrió algún error al generar la línea de historial
          console.log(
            "Error al generar entrada en el historial:" + data.errorCode
          );
        }
      } else {
        //Ocurrió algún error al generar la línea de historial
        console.log(
          "Error al generar entrada en el historial (2): " + data.errorCode
        );
      }
    },
    error: function (xhr, status) {
      console.log("error en la API de crear entrada de historial ");
      console.log(xhr);
      console.log(status);
    },
  });
};

document.addEventListener("LanguageReady", function () {
  refrescarBloquesHistorial();
});
