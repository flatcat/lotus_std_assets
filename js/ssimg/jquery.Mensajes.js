var _vistaMensajes="voMensajes";
	var _vistaCountMensajes="voCountMensajesPorDocRelacionado";
	var _mensajesMostrados={};


	refrescarBloquesMensajes = function(){
		$('.bloque-mensajes').each(function(index, obj){
			var parentId=$(obj).attr("data-msg-parent-id");
			var editable=$(obj).attr("data-msg-editable")=="1";
			if($(obj).find('.accordion').length==0){
				_mensajesMostrados[parentId]=[];
				var bloqueMensajes=_BloqueMensajesTemplate;
				bloqueMensajes=bloqueMensajes.replace(/#INDEX#/g,index);
				bloqueMensajes=bloqueMensajes.replace(/#PARENT_ID#/g,parentId);
				if ($(obj).attr("data-msg-editable")=="0"){
					bloqueMensajes=bloqueMensajes.replace(/#HIDDEN_ELEMENT#/g,"hiddenElement");
				} else {
					bloqueMensajes=bloqueMensajes.replace(/#HIDDEN_ELEMENT#/g,"");
				}
				$(obj).html(bloqueMensajes);
				if (!jsIsEditMode){
					$('#nuevoMensaje'+index).remove();
				}
				translateHTMLBlock(obj);
			}

			var params= {"parentId" : parentId, "editable" : editable}
			loadViewContentByCategory(jsDbURL, _vistaMensajes, parentId, appendMensajes, true, params);
		});
		actualizarContadores();
	}

	actualizarContadores= function (){
		$('.bloque-mensajes').each(function(index, obj){
			parentId=$(obj).attr("data-msg-parent-id");
			loadViewContentByCategory(jsDbURL, _vistaCountMensajes, parentId, mostrarContadorMensajes, true, parentId);
		});
	}



	mostrarContadorMensajes = function(data, docRelacionado){
		if (data.viewentry) {
			$('.bloque-mensajes[data-msg-parent-id="'+docRelacionado+'"]').find(".msg-qty").html(data.viewentry.length);
		}
	}

	function applyMensajesListeners() {
		//Botón Responder
		$('.btn-responder').unbind("click").click(function(){
			var parentID = $(this).attr("data-msg-id");
			$(this).hide();
			if ($('.nuevo-mensaje').length>0){
				$('.nuevo-mensaje').remove();
			}
			var mensaje=_NuevoMensajeTemplate;
			mensaje=mensaje.replace(/#USUARIO_ACTUAL#/g,$('#usuarioActualCN').val());
			var date = new Date();
			var tempID=date.getTime();
			mensaje=mensaje.replace(/#MSG_TEMP_ID#/g,tempID);
			mensaje=mensaje.replace(/#PARENT_ID#/g,parentID);
			mensaje=mensaje.replace(/#REFERENCE_DOC_ID#/g,$(this).attr("data-reference-doc"));
			mensaje=mensaje.replace(/#NUEVO_MSG_TITULO#/g,"litMensajeRespuesta");
			$('.mensaje[data-msg-id="'+parentID+'"] .bloque-nueva-respuesta').html(mensaje);
			$('.mensaje[data-msg-id="'+parentID+'"] .bloque-nueva-respuesta').show(200);
			$('#mensajePara').val($(this).attr("data-msg-sendto"));
			$('#mensajeAsunto').val("RE: " + $(this).attr("data-msg-subject"));

			applyMensajesListeners();
			processSelectizeField($('.nuevo-mensaje[data-msg-id='+tempID+'] .selectize'));
			translateHTMLBlock('.nuevo-mensaje[data-msg-id='+tempID+']');
			refrescarBloquesDeAnexos();
		});

		//Botón nuevo mensaje/hilo
		$('.btn-nuevoMensaje').unbind("click").click(function(){
			if ($('.nuevo-mensaje').length>0){
				$('.nuevo-mensaje').remove();
			}
			var bloque=$('.bloque-Mensajes .mens-card-content[data-msg-parent-id="'+$(this).attr("data-msg-parent-id")+'"]');
			var mensaje=_NuevoMensajeTemplate;
			mensaje=mensaje.replace(/#USUARIO_ACTUAL#/g,$('#usuarioActualCN').val());
			var date = new Date();
			var tempID=date.getTime();
			mensaje=mensaje.replace(/#MSG_TEMP_ID#/g,tempID);
			mensaje=mensaje.replace(/#PARENT_ID#/g,$(this).attr("data-msg-parent-id"));
			mensaje=mensaje.replace(/#REFERENCE_DOC_ID#/g,$(this).attr("data-msg-parent-id"));
			var referenceDOCShort=$(this).attr("data-msg-parent-id");
			var tempParent=referenceDOCShort.split("_");
			referenceDOCShort=tempParent[0];
			mensaje=mensaje.replace(/#REFERENCE_DOC_ID_SHORT#/g,referenceDOCShort);
			mensaje=mensaje.replace(/#NUEVO_MSG_TITULO#/g,"litNuevoMensaje");
			bloque.append(mensaje);

			$([document.documentElement, document.body]).animate({
        scrollTop: $(".nuevo-mensaje[data-msg-id="+tempID+"]").offset().top
    	}, 300);

			applyMensajesListeners();
			processSelectizeField($('.nuevo-mensaje[data-msg-id='+tempID+'] .selectize'));
			translateHTMLBlock('.nuevo-mensaje[data-msg-id='+tempID+']');
			refrescarBloquesDeAnexos();

		});

		//Botón enviar mensaje
		$('.btn-enviarMensaje').unbind("click").click(function(){
			var isValid=true;
			$('.required').each(function(index,obj){
				if ($(obj).val()==""){
					isValid=false;
					$(obj).addClass("error-field");
					$('#sz-'+$(obj).attr("id")).next(".selectize-control").addClass("error-field");
					$('#msg_'+$(obj).attr("id")).removeClass("hiddenElement");
				}
				$(obj).unbind("change").change(function(){
					if ($(this).val()!=""){
						$(obj).removeClass("error-field");
						$('#msg_'+$(obj).attr("id")).addClass("hiddenElement");
					}
				});
				if ($(obj).hasClass("selectize")){//campos selectize
						$('#sz-'+$(obj).attr("id")).change(function(){
							$('#sz-'+$(obj).attr("id")).next(".selectize-control").removeClass("error-field");
							$('#msg_'+$(obj).attr("id")).addClass("hiddenElement").html("");
						})
				}
			});

			if (!isValid){
				showNotification(translate("jsLit_msgErrorEnFormulario"), "Error");
	      return false;
			}
			else {
				var tempID = $(this).attr('data-msg-id');
				var parentID = $(this).attr('data-parent-id');
				var docReference = $(this).attr('data-reference-doc');
				var from = $('#usuarioActualCN').val();
				var sendTo = $('#mensajePara').val().replace(/,/g,';');
				var subject = $('#mensajeAsunto').val().replace(/"/g,"'");
				var body = $('#mensajeBody').val().replace(/\n/g, '\\n').replace(/"/g,"'");
				body = body.replace(/"/g, '\\"');
				body = body.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
				var params= '{"tempID" : "'+tempID+'", "idPadre" : "'+parentID+'", "docRelacionado" : "'+docReference+'" ,"from" : "'+from+'", "sendTo" : "'+sendTo+'", "subject":"'+subject+'", "body" : "'+body+'"}';
				addMensaje(params);
			}


		});


		//Botón cancelar mensaje
		$('.btn-cancelarMensaje').unbind("click").click(function(){
			var msgID = $(this).attr("data-msg-id");
			var parentID= $(this).attr("data-parent-id");
			cerrarNuevoMensaje(parentID);

		});

	}


	cerrarNuevoMensaje = function(parentID){
		$('.nuevo-mensaje select[id^="sz-"]').each(function(index, obj) {
			if (this.selectize) {
				this.selectize.destroy();
			}
		});

		$(".mensaje[data-msg-id="+parentID+"]").find(".btn-responder").show();
		$('.nuevo-mensaje').hide("0", function(){$(this).remove()});
	}



	function appendMensajes(data, extraParams){
		var parentID = extraParams.parentId;
		var isEditable =extraParams.editable;
		if (data.viewentry){
			var bloque=$('.lista-mensajes[data-msg-parent-id='+parentID+']');
			for (var i=0; i<data.viewentry.length;i++){
				var mensaje=_MensajeTemplate
				var msgID=data.viewentry[i]["@unid"];
				if (_mensajesMostrados[parentID]==undefined){
					_mensajesMostrados[parentID]=[];
				}
				if (_mensajesMostrados[parentID].indexOf(msgID)==-1){
					_mensajesMostrados[parentID].push(msgID);
					var parentID=data.viewentry[i].entrydata[6].text[0];
					var fechaHora=data.viewentry[i].entrydata[0].text[0];
					var remitente=data.viewentry[i].entrydata[1].text[0];
					var destinatarios=data.viewentry[i].entrydata[2].text[0];
					var asunto=data.viewentry[i].entrydata[3].text[0];
					var body=data.viewentry[i].entrydata[4].text[0];
					var tieneAnexos=data.viewentry[i].entrydata[5].text[0];
					var shortID = msgID//.substring(1,5);
					var docRelacionado=data.viewentry[i].entrydata[7].text[0];
					var docRelacionadoShort=docRelacionado;
					var temp=docRelacionado.split("_");
					docRelacionadoShort=temp[0];
					body=getCuttedBody(body);
					var preview=body[0];
					var full=body[1];

					mensaje=mensaje.replace(/#MSG_ID#/g,msgID);
					mensaje=mensaje.replace(/#MSG_REMITENTE#/g,remitente);
					mensaje=mensaje.replace(/#MSG_DESTINATARIOS#/g,destinatarios);
					mensaje=mensaje.replace(/#MSG_FECHA_HORA#/g,fechaHora);
					mensaje=mensaje.replace(/#MSG_SUBJECT#/g,asunto);
					mensaje=mensaje.replace(/#MSG_SHORT_ID#/g,shortID);
					mensaje=mensaje.replace(/#INDEX#/g,shortID);
					mensaje=mensaje.replace(/#MSG_PREVIEW#/g,preview);
					mensaje=mensaje.replace(/#MSG_FULL#/g,full);
					mensaje=mensaje.replace(/#REFERENCE_DOC_ID#/g,docRelacionado);
					mensaje=mensaje.replace(/#REFERENCE_DOC_ID_SHORT#/g,docRelacionadoShort);
					if (tieneAnexos){
						mensaje=mensaje.replace(/#HAS_ATTACHMENTS#/g,_AttachmentIndicatorTemplate);
					}
					else {
							mensaje=mensaje.replace(/#HAS_ATTACHMENTS#/g,"");
					}

					if (tieneAnexos=="S"){
							loadViewContentByCategory(jsDbURL, "voAnexos", msgID, appendAnexosMensaje, false, msgID)
					}

					bloque.append(mensaje);
					if (full==""){
						$('[data-target="#details'+shortID+'"]').hide();
					}
					if (!isEditable){
						//$('.btn-responder').remove();
					}

					//Obtener respuestas al mensaje
					var params= {"parentId" : msgID, "editable" : isEditable};
					_mensajesMostrados[msgID]=[];
					loadViewContentByCategory(jsDbURL, _vistaMensajes, msgID, appendMensajes, true, params);
					//Si se ha solicitado por QueryString abrir el mensaje, posicionar la pantalla en este mensaje
					var _targetMsg = $('#VerMail').val();
					if (msgID==_targetMsg){
						abrirMensaje(docRelacionado, msgID);
					}
				}

			}
			translateHTMLBlock('.lista-Mensajes[data-msg-parent-id='+parentID+']');
		}
		applyMensajesListeners();

	}


	appendAnexosMensaje = function (data, msgID){
		if (data.viewentry){
			var listaAnexosMsg=$('.listado-anexos-msg[data-msg-id="'+msgID+'"]');

			for (var i=0; i<data.viewentry.length;i++){
					var anexoUNID = data.viewentry[i]["@unid"];
					var nomFichero=data.viewentry[i].entrydata[1].text[0];
					var enlace=data.viewentry[i].entrydata[2].text[0];
					var creador=data.viewentry[i].entrydata[4].text[0];
					var fecha=data.viewentry[i].entrydata[0].text[0];
					var comentario=data.viewentry[i].entrydata[5].text[0];
					if (comentario=="0") {
						comentario=""
					};
					var anexo=i>0 ? (", " + _ItemAnexoMensaje) : _ItemAnexoMensaje;;
					anexo=anexo.replace(/#ANEXO_UNID#/g, anexoUNID);
					anexo=anexo.replace(/#ENLACE#/g, enlace);
					anexo=anexo.replace(/#NOMBRE_FICHERO#/g, nomFichero);
					anexo=anexo.replace(/#CREADOR#/g, creador);
					anexo=anexo.replace(/#FECHA_UPLOAD#/g, fecha);
					anexo=anexo.replace(/#ANEXO_COMENTARIO#/g, comentario);
					anexo=anexo.replace(/#TOOLTIP-ID#/g, i);

					listaAnexosMsg.append(anexo);
					$('.an-comment-edit').attr("data-original-title", translate("litComentarioAnexo"));

			}
			reloadAnTooltips();
			applyAnexosListeners();
		}
	}



	addMensaje= function(params){
		params = JSON.parse(params);
		startLoad();
		var db=$('#pathBDWeb').val();
		$.ajax({
			url: db + "/API_Mensaje_Add?openAgent",
			type: "POST",
			cache:false,
			data : params,
			dataType: "text",
			success: function (data) {
				stopLoad();
				var response = JSON.parse(data);
				if (response.result=="ok"){
					cerrarNuevoMensaje(params.idPadre);
					_mensajesMostrados[params.idPadre]=[];
					$('.lista-Mensajes[data-msg-parent-id='+params.idPadre+']').html("");
					var parameters={"parentId" : params.idPadre, "editable" : true};
					loadViewContentByCategory(jsDbURL, _vistaMensajes, params.idPadre, appendMensajes, true, parameters);
					$('[id^=collapseMsg]').collapse('hide');
					actualizarContadores();
					showNotification(translate("litMensajeEnviado"), "Success");
				}
				else {
					showNotification(translate("QueryError"), "Error");
				}
			},
			error: function (xhr, status) {
				stopLoad();
				showNotification(translate("ConnectionError"), "Error");
			}
		});
	}

// Devuelve un objeto con el texto cortado en dos partes.
// La primera parte tendrá una longitud máxima, pero se cortará
// en la primera palabra completa antes del límite o en el primer salto de línea.
	getCuttedBody = function(body){
		var maxLength=120;
		var cuttedBody=[];

		var firstStopPosition=body.indexOf(".") || 0;
		if (firstStopPosition>0 && firstStopPosition < maxLength){
			cuttedBody[0]=body.substring(0,firstStopPosition);
			if (firstStopPosition<body.length){
					cuttedBody[1]=body.substring(firstStopPosition+1);
			}
			else {
				cuttedBody[1]="";
			}
			return cuttedBody;
		}

		var firstLineBreak=body.indexOf("<br>") || 0
		if (firstLineBreak>0 && firstLineBreak < maxLength){
			cuttedBody[0]=body.substring(0,firstLineBreak);
			if (firstStopPosition<body.length){
					cuttedBody[1]=body.substring(firstLineBreak+4);
			}
			else {
				cuttedBody[1]="";
			}
			return cuttedBody;
		}

		if (body.length<=maxLength){
			cuttedBody[0]=body;
			cuttedBody[1]="";
			return cuttedBody;
		}
		else {
			var positionToCut=maxLength;
			var canCut=false;
			while (!canCut) {
				var char= body.substring(positionToCut,1);
				if (char=" "){
					canCut=true;
				}
				else {
					positionToCut--;
				}
			}
			cuttedBody[0]=body.substring(0,positionToCut);
			cuttedBody[1]=body.substring(positionToCut+1);
			return cuttedBody;
		}
	}


	abrirMensaje = function (docRelacionado, msgUNID) {
		var tabs=false;
		if ($('.tab-pane-msg')) {
			tabs=true;
		}
		if (tabs){
			$('.tab-pane').removeClass("in").removeClass("active").removeClass("show");
			$('.tab-pane-msg').addClass("in").addClass("active").addClass("show");

			$('.nav-link').prop("aria-selected","false").removeClass("active").removeClass("show");
			$('.nav-link-msg').prop("aria-selected","true").addClass("active").addClass("show");
		}
		//Desplegar sección mensajes con data-msg-parent-id=data-msg-parent-id
		$('.bloque-mensajes[data-msg-parent-id='+docRelacionado+'] .collapse').first().collapse(false);
		//Desplegar mensaje  data-msg-id=msgUNID
		$('.mensaje[data-msg-id='+msgUNID+'] [id^=collapseMsg]').collapse(false);
		//Posicionar scroll de pantalla
		$([document.documentElement, document.body]).animate({
			scrollTop: $('.mensaje[data-msg-id='+msgUNID+']').offset().top
		}, 300);
	}



document.addEventListener("LanguageReady", function(){
  	refrescarBloquesMensajes();
});
